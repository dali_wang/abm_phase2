
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kison
 */

public class ABMSimulationGUI extends javax.swing.JFrame {

    /**
     * Creates new form ABMSimulationGUI
     */
    public ABMSimulationGUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        ModelTextField = new javax.swing.JTextField();
        DivTextField = new javax.swing.JTextField();
        ModelLabel = new javax.swing.JLabel();
        DivLabel = new javax.swing.JLabel();
        CellNameTextField = new javax.swing.JTextField();
        VisualizeButton = new javax.swing.JButton();
        CellNameLabel = new javax.swing.JLabel();
        OutputLabel = new javax.swing.JLabel();
        CellTrackCheckBox = new javax.swing.JCheckBox();
        OutputTextField = new javax.swing.JTextField();
        ModelFileButton = new javax.swing.JButton();
        DivFileButton = new javax.swing.JButton();
        OutputFileButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        ModelLabel.setText("Model.props File Location");

        DivLabel.setText("Division Table File Location");

        VisualizeButton.setText("Visualize");
        VisualizeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VisualizeButtonActionPerformed(evt);
            }
        });

        CellNameLabel.setText("Cell Name (if tracking)");

        OutputLabel.setText("Output File Location");

        CellTrackCheckBox.setText("Cell Tracking");

        ModelFileButton.setText("Browse");
        ModelFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModelFileButtonActionPerformed(evt);
            }
        });

        DivFileButton.setText("Browse");
        DivFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DivFileButtonActionPerformed(evt);
            }
        });

        OutputFileButton.setText("Browse");
        OutputFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OutputFileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(VisualizeButton)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addComponent(CellNameTextField))
                                    .addComponent(CellTrackCheckBox, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CellNameLabel)))
                        .addGap(31, 31, 31))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(OutputTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(DivTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ModelTextField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(OutputFileButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(OutputLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(DivFileButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                .addComponent(DivLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ModelFileButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ModelLabel)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ModelTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ModelLabel)
                    .addComponent(ModelFileButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DivTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DivLabel)
                    .addComponent(DivFileButton))
                .addGap(18, 18, 18)
                .addComponent(CellTrackCheckBox)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CellNameLabel)
                    .addComponent(CellNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(OutputLabel)
                    .addComponent(OutputTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(OutputFileButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(VisualizeButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void VisualizeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VisualizeButtonActionPerformed
        // TODO add your handling code here:
        String propsloc = ModelTextField.getText();
        String outputloc = OutputTextField.getText();
        String divloc = DivTextField.getText();
            
        if (CellTrackCheckBox.isSelected()==true)
        {
            String cellname = CellNameTextField.getText();
            String[] commands = new String[]{"C:/Python27/python","CellTrackingJava.py", divloc, outputloc, cellname};
            try { 
                Process p = Runtime.getRuntime().exec(commands);
            } catch (IOException e) {
            }
            try {  
            Runtime.getRuntime().exec("visit -cli -s visitsettingstracking.py");
            } catch (IOException ex) {
                Logger.getLogger(ABMSimulationGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            String[] commands = new String[]{"C:/Python27/python","4colorsjava.py", divloc, outputloc};
            try { 
                Process p = Runtime.getRuntime().exec(commands);
            } catch (IOException e) {
            }
            
            try {  
            Runtime.getRuntime().exec("visit -cli -s visitsettings.py");
            } catch (IOException ex) {
                Logger.getLogger(ABMSimulationGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       
        
    }//GEN-LAST:event_VisualizeButtonActionPerformed

    private void DivFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DivFileButtonActionPerformed
        // TODO add your handling code here:
       
        int returnVal = jFileChooser1.showOpenDialog(this);
        if (returnVal == jFileChooser1.APPROVE_OPTION) {
            File file = jFileChooser1.getSelectedFile();
            
              // What to do with the file, e.g. display it in a TextArea
              String path = file.getAbsolutePath();
              DivTextField.setText(path);
           
              System.out.println("problem accessing file"+file.getAbsolutePath());
            
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_DivFileButtonActionPerformed

    private void OutputFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OutputFileButtonActionPerformed
        // TODO add your handling code here:
        int returnVal = jFileChooser1.showOpenDialog(this);
        if (returnVal == jFileChooser1.APPROVE_OPTION) {
            File file = jFileChooser1.getCurrentDirectory();

            // What to do with the file, e.g. display it in a TextArea
            String path = file.getAbsolutePath() +"/";
            OutputTextField.setText(path);

            System.out.println("problem accessing file"+file.getAbsolutePath());

        } else {
            System.out.println("File access cancelled by user.");
        }
        
    }//GEN-LAST:event_OutputFileButtonActionPerformed

    private void ModelFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModelFileButtonActionPerformed
        // TODO add your handling code here:
        int returnVal = jFileChooser1.showOpenDialog(this);
        if (returnVal == jFileChooser1.APPROVE_OPTION) {
            File file = jFileChooser1.getSelectedFile();
            
              // What to do with the file, e.g. display it in a TextArea
              String path = file.getAbsolutePath();
              DivTextField.setText(path);
           
              System.out.println("problem accessing file"+file.getAbsolutePath());
            
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_ModelFileButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ABMSimulationGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ABMSimulationGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ABMSimulationGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ABMSimulationGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ABMSimulationGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CellNameLabel;
    private javax.swing.JTextField CellNameTextField;
    private javax.swing.JCheckBox CellTrackCheckBox;
    private javax.swing.JButton DivFileButton;
    private javax.swing.JLabel DivLabel;
    private javax.swing.JTextField DivTextField;
    private javax.swing.JButton ModelFileButton;
    private javax.swing.JLabel ModelLabel;
    private javax.swing.JTextField ModelTextField;
    private javax.swing.JButton OutputFileButton;
    private javax.swing.JLabel OutputLabel;
    private javax.swing.JTextField OutputTextField;
    private javax.swing.JButton VisualizeButton;
    private javax.swing.JFileChooser jFileChooser1;
    // End of variables declaration//GEN-END:variables
}
