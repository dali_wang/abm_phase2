Cell name  Mean cell cycle length  Stdev of cell cycle length 

ABal  14.734   0.956
ABar  14.849   0.970
ABpl  14.790   2.118
ABpr  14.742   0.918
ABala  17.289   0.911
ABalp  17.033   0.873
ABara  16.910   1.002
ABarp  16.714   0.961
ABpla  16.761   1.329
ABplp  17.293   1.456
ABpra  16.933   0.947
ABprp  17.460   0.935
ABalaa  24.984   1.020
ABalap  24.574   1.100
ABalpa  24.164   1.293
ABalpp  24.432   1.355
ABaraa  24.210   1.162
ABarap  24.168   1.196
ABarpa  25.292   1.097
ABarpp  26.173   1.271
ABplaa  24.139   1.121
ABplap  24.043   1.185
ABplpa  24.633   1.299
ABplpp  25.228   1.320
ABpraa  24.004   1.344
ABprap  23.935   1.190
ABprpa  24.509   1.138
ABprpp  24.907   1.293
ABalaaa  29.751   1.049
ABalaap  30.545   1.010
ABalapa  28.896   1.185
ABalapp  28.940   1.187
ABalpaa  28.390   1.111
ABalpap  27.621   1.114
ABalppa  29.374   0.963
ABalppp  28.135   0.916
ABaraaa  29.290   0.853
ABaraap  28.233   1.083
ABarapa  28.676   0.818
ABarapp  28.838   0.927
ABarpaa  26.248   0.876
ABarpap  27.523   0.968
ABarppa  27.299   0.816
ABarppp  27.882   1.013
ABplaaa  28.361   1.096
ABplaap  26.754   0.988
ABplapa  27.490   1.138
ABplapp  27.803   1.228
ABplpaa  28.714   1.321
ABplpap  27.730   1.238
ABplppa  29.999   1.054
ABplppp  28.344   1.186
ABpraaa  29.357   0.999
ABpraap  27.637   1.171
ABprapa  27.894   0.929
ABprapp  28.398   0.894
ABprpaa  28.794   0.941
ABprpap  28.000   0.940
ABprppa  30.019   1.001
ABprppp  28.559   1.148
ABalaaaa  40.186   1.757
ABalaaap  35.305   1.153
ABalaapa  40.293   2.227
ABalaapp  35.787   1.531
ABalapaa  37.117   1.262
ABalapap  34.112   1.026
ABalappa  39.763   1.319
ABalappp  33.548   1.111
ABalpaaa  39.446   1.619
ABalpaap  32.337   0.970
ABalpapa  34.827   1.145
ABalpapp  31.613   1.029
ABalppaa  41.356   1.568
ABalppap  34.410   0.945
ABalpppa  35.643   1.105
ABalpppp  32.402   1.049
ABaraaaa  41.425   1.487
ABaraaap  33.623   0.962
ABaraapa  35.347   1.430
ABaraapp  33.518   1.316
ABarapaa  36.237   1.007
ABarapap  32.639   0.856
ABarappa  36.755   1.349
ABarappp  33.730   0.882
ABarpaaa  42.654   2.010
ABarpaap  33.353   1.175
ABarpapa  34.727   1.173
ABarpapp  35.123   1.266
ABarppaa  38.870   1.559
ABarppap  39.338   1.572
ABarpppa  37.035   1.231
ABarpppp  38.884   1.401
ABplaaaa  35.688   1.712
ABplaaap  35.821   1.712
ABplaapa  35.348   1.714
ABplaapp  32.386   1.546
ABplapaa  37.745   2.078
ABplapap  33.698   1.630
ABplappa  36.433   1.414
ABplappp  38.642   1.190
ABplpaaa  34.933   2.024
ABplpaap  33.808   1.759
ABplpapa  35.068   1.445
ABplpapp  32.626   2.132
ABplppaa  39.793   1.653
ABplppap  36.600   1.778
ABplpppa  35.062   1.255
ABplpppp  34.186   1.500
ABpraaaa  38.942   1.523
ABpraaap  34.403   1.016
ABpraapa  36.446   1.194
ABpraapp  33.298   1.037
ABprapaa  38.197   1.677
ABprapap  33.714   1.045
ABprappa  37.349   1.341
ABprappp  39.450   1.161
ABprpaaa  34.855   0.867
ABprpaap  33.849   1.240
ABprpapa  34.454   1.008
ABprpapp  32.736   1.123
ABprppaa  39.124   1.233
ABprppap  36.331   0.953
ABprpppa  34.718   0.946
ABprpppp  33.982   1.055
ABalaaaal  45.181   1.990
ABalaaaar  47.732   1.842
ABalaaapa  45.331   1.919
ABalaaapp  45.558   2.209
ABalaapaa  51.185   2.260
ABalaapap  48.970   1.904
ABalaappa  50.680   1.991
ABalaappp  43.798   2.460
ABalapaaa  49.437   2.164
ABalapaap  46.173   2.018
ABalapapa  50.233   2.738
ABalapapp  43.150   2.110
ABalappaa  54.886   2.262
ABalappap  45.238   1.651
ABalapppa  44.428   1.949
ABalapppp  43.697   1.510
ABalpaaaa  50.018   1.907
ABalpaaap  49.288   2.411
ABalpaapa  39.012   1.642
ABalpaapp  46.934   2.327
ABalpapaa  46.596   1.617
ABalpapap  45.317   1.681
ABalpappa  40.561   1.510
ABalpappp  39.744   1.708
ABalppaaa  50.618   2.034
ABalppaap  48.002   2.645
ABalppapa  43.861   1.752
ABalppapp  41.437   1.545
ABalpppaa  50.052   2.103
ABalpppap  42.940   1.548
ABalppppa  45.170   2.150
ABalppppp  38.931   1.869
ABaraaaaa  49.895   2.258
ABaraaaap  57.791   3.740
ABaraaapa  40.217   1.948
ABaraaapp  48.439   2.650
ABaraapaa  44.686   2.343
ABaraapap  42.454   2.429
ABaraappa  44.054   1.922
ABaraappp  43.229   2.518
ABarapaaa  49.980   2.252
ABarapaap  42.526   1.543
ABarapapa  42.969   1.483
ABarapapp  39.533   1.526
ABarappaa  48.724   2.334
ABarappap  44.453   2.395
ABarapppa  45.704   2.330
ABarapppp  43.073   1.727
ABarpaaaa  50.040   1.546
ABarpaaap  53.130   2.953
ABarpaapa  44.471   2.193
ABarpaapp  44.425   2.243
ABarpapaa  48.660   2.151
ABarpapap  47.254   1.928
ABarpappa  53.144   2.767
ABarpappp  46.594   2.109
ABarppaaa  54.077   2.863
ABarppaap  47.543   2.127
ABarppapa  53.632   3.413
ABarppapp  53.716   2.616
ABarpppaa  52.323   1.706
ABarpppap  46.764   2.269
ABarppppa  51.433   2.717
ABarppppp  52.168   3.165
ABplaaaaa  49.693   2.392
ABplaaaap  47.211   2.994
ABplaaapa  52.548   3.938
ABplaaapp  45.978   3.297
ABplaapaa  46.942   2.321
ABplaapap  45.430   2.412
ABplaappa  49.805   2.885
ABplaappp  43.131   1.981
ABplapaaa  46.644   1.750
ABplapaap  50.467   2.671
ABplapapa  47.258   2.083
ABplapapp  44.703   1.549
ABplappaa  48.002   2.590
ABplappap  48.249   2.731
ABplapppa  51.833   2.776
ABplapppp  51.408   2.415
ABplpaaaa  43.101   1.518
ABplpaaap  44.512   1.417
ABplpaapa  46.383   2.343
ABplpaapp  43.291   2.050
ABplpapaa  48.826   2.037
ABplpapap  45.260   1.813
ABplpappa  41.232   1.976
ABplpappp  48.034   2.472
ABplppaaa  51.404   2.311
ABplppaap  47.742   1.928
ABplppapa  46.928   2.406
ABplppapp  49.753   2.675
ABplpppaa  46.343   2.101
ABplpppap  46.956   2.067
ABplppppa  43.506   1.420
ABplppppp  46.332   1.843
ABpraaaaa  50.593   2.954
ABpraaaap  44.117   2.671
ABpraaapa  47.885   2.228
ABpraaapp  39.486   2.046
ABpraapaa  46.858   2.359
ABpraapap  45.928   2.509
ABpraappa  51.061   2.497
ABpraappp  44.548   2.065
ABprapaaa  47.116   1.803
ABprapaap  51.439   2.662
ABprapapa  47.797   2.529
ABprapapp  45.297   1.952
ABprappaa  48.739   2.618
ABprappap  47.855   1.988
ABprapppa  52.816   2.516
ABprapppp  52.619   2.661
ABprpaaaa  43.278   1.714
ABprpaaap  44.689   2.066
ABprpaapa  47.114   2.703
ABprpaapp  43.822   2.271
ABprpapaa  48.030   2.453
ABprpapap  44.964   2.222
ABprpappa  43.524   2.481
ABprpappp  44.998   2.705
ABprppaaa  51.636   2.051
ABprppaap  47.194   2.549
ABprppapa  47.062   2.478
ABprppapp  51.059   3.486
ABprpppaa  46.732   2.343
ABprpppap  47.063   2.439
ABprppppa  43.496   1.668
ABprppppp  46.313   2.326
ABalaaaala     
ABalaaaalp     
ABalaaaarl     
ABalaaaarr     
ABalaaapal     
ABalaaapar     
ABalaaappl     
ABalaaappr     
ABalaapaaa     
ABalaapaap     
ABalaapapa     
ABalaapapp     
ABalaappaa     
ABalaappap     
ABalaapppa     
ABalaapppp     
ABalapaaaa     
ABalapaaap     
ABalapaapa     
ABalapaapp     
ABalapapaa     
ABalapapap     
ABalapappa     
ABalapappp     
ABalappaaa     
ABalappaap     
ABalappapa     
ABalappapp     
ABalapppaa     
ABalapppap     
ABalappppa     
ABalappppp     
ABalpaaaaa     
ABalpaaaap     
ABalpaaapa     
ABalpaaapp     
ABalpaapaa     
ABalpaapap     
ABalpaappa     
ABalpaappp     
ABalpapaaa     
ABalpapaap     
ABalpapapa     
ABalpapapp     
ABalpappaa     
ABalpappap     
ABalpapppa     
ABalpapppp     
ABalppaaaa     
ABalppaaap     
ABalppaapa     
ABalppaapp     
ABalppapaa     
ABalppapap     
ABalppappa     
ABalppappp     
ABalpppaaa     
ABalpppaap     
ABalpppapa     
ABalpppapp     
ABalppppaa     
ABalppppap     
ABalpppppa     
ABalpppppp     
ABaraaaaaa     
ABaraaaaap     
ABaraaaapa     
ABaraaaapp     
ABaraaapaa     
ABaraaapap     
ABaraaappa     
ABaraaappp     
ABaraapaaa     
ABaraapaap     
ABaraapapa     
ABaraapapp     
ABaraappaa     
ABaraappap     
ABaraapppa     
ABaraapppp     
ABarapaaaa     
ABarapaaap     
ABarapaapa     
ABarapaapp     
ABarapapaa     
ABarapapap     
ABarapappa     
ABarapappp     
ABarappaaa     
ABarappaap     
ABarappapa     
ABarappapp     
ABarapppaa     
ABarapppap     
ABarappppa     
ABarappppp     
ABarpaaaaa     
ABarpaaaap     
ABarpaaapa     
ABarpaaapp     
ABarpaapaa     
ABarpaapap     
ABarpaappa     
ABarpaappp     
ABarpapaaa     
ABarpapaap     
ABarpapapa     
ABarpapapp     
ABarpappaa     
ABarpappap     
ABarpapppa     
ABarpapppp     
ABarppaaaa     
ABarppaaap     
ABarppaapa     
ABarppaapp     
ABarppapaa     
ABarppapap     
ABarppappa     
ABarppappp     
ABarpppaaa     
ABarpppaap     
ABarpppapa     
ABarpppapp     
ABarppppaa     
ABarppppap     
ABarpppppa     
ABarpppppp     
ABplaaaaaa     
ABplaaaaap     
ABplaaaapa     
ABplaaaapp     
ABplaaapaa     
ABplaaapap     
ABplaaappa     
ABplaaappp     
ABplaapaaa     
ABplaapaap     
ABplaapapa     
ABplaapapp     
ABplaappaa     
ABplaappap     
ABplaapppa     
ABplaapppp     
ABplapaaaa     
ABplapaaap     
ABplapaapa     
ABplapaapp     
ABplapapaa     
ABplapapap     
ABplapappa     
ABplapappp     
ABplappaaa     
ABplappaap     
ABplappapa     
ABplappapp     
ABplapppaa     
ABplapppap     
ABplappppa     
ABplappppp     
ABplpaaaaa     
ABplpaaaap     
ABplpaaapa     
ABplpaaapp     
ABplpaapaa     
ABplpaapap     
ABplpaappa     
ABplpaappp     
ABplpapaaa     
ABplpapaap     
ABplpapapa     
ABplpapapp     
ABplpappaa     
ABplpappap     
ABplpapppa     
ABplpapppp     
ABplppaaaa     
ABplppaaap     
ABplppaapa     
ABplppaapp     
ABplppapaa     
ABplppapap     
ABplppappa     
ABplppappp     
ABplpppaaa     
ABplpppaap     
ABplpppapa     
ABplpppapp     
ABplppppaa     
ABplppppap     
ABplpppppa     
ABplpppppp     
ABpraaaaaa     
ABpraaaaap     
ABpraaaapa     
ABpraaaapp     
ABpraaapaa     
ABpraaapap     
ABpraaappa     
ABpraaappp     
ABpraapaaa     
ABpraapaap     
ABpraapapa     
ABpraapapp     
ABpraappaa     
ABpraappap     
ABpraapppa     
ABpraapppp     
ABprapaaaa     
ABprapaaap     
ABprapaapa     
ABprapaapp     
ABprapapaa     
ABprapapap     
ABprapappa     
ABprapappp     
ABprappaaa     
ABprappaap     
ABprappapa     
ABprappapp     
ABprapppaa     
ABprapppap     
ABprappppa     
ABprappppp     
ABprpaaaaa     
ABprpaaaap     
ABprpaaapa     
ABprpaaapp     
ABprpaapaa     
ABprpaapap     
ABprpaappa     
ABprpaappp     
ABprpapaaa     
ABprpapaap     
ABprpapapa     
ABprpapapp     
ABprpappaa     
ABprpappap     
ABprpapppa     
ABprpapppp     
ABprppaaaa     
ABprppaaap     
ABprppaapa     
ABprppaapp     
ABprppapaa     
ABprppapap     
ABprppappa     
ABprppappp     
ABprpppaaa     
ABprpppaap     
ABprpppapa     
ABprpppapp     
ABprppppaa     
ABprppppap     
ABprpppppa     
ABprpppppp     
MS     
MSa  20.492   1.074
MSp  20.480   1.217
MSaa  24.789   1.131
MSap  27.412   1.129
MSpa  24.999   0.993
MSpp  27.325   1.211
MSaaa  28.903   0.755
MSaap  29.258   0.869
MSapa  30.627   1.046
MSapp  31.347   0.932
MSpaa  29.797   0.995
MSpap  29.077   0.901
MSppa  30.907   0.932
MSppp  31.183   1.421
MSaaaa  31.944   1.021
MSaaap  35.739   1.786
MSaapa  31.924   0.931
MSaapp  40.789   2.070
MSapaa  37.902   1.409
MSapap  37.615   1.734
MSappa  44.197   2.582
MSappp  47.035   3.176
MSpaaa  31.895   0.983
MSpaap  38.530   1.660
MSpapa  32.287   1.075
MSpapp  41.349   2.405
MSppaa  38.173   1.218
MSppap  36.802   1.353
MSpppa  40.968   1.976
MSpppp  44.988   2.696
MSaaaaa  38.759   1.681
MSaaaap  43.120   2.354
MSaaapa  37.638   2.003
MSaaapp  51.350   9.675
MSaapaa  39.099   1.930
MSaapap  47.498   2.661
MSaappa  43.100   10.392
MSaappp  47.400   10.338
MSapaaa  45.876   2.046
MSapaap  47.000   8.627
MSapapa  45.738   2.166
MSapapp  53.935   3.566
MSappaa  42.400   8.438
MSappap  40.850   9.161
MSapppa  39.600   9.150
MSapppp  38.950   9.219
MSpaaaa  38.266   1.862
MSpaaap  41.433   1.962
MSpaapa  41.701   2.092
MSpaapp  46.650   9.121
MSpapaa  38.610   1.412
MSpapap  46.238   1.613
MSpappa  49.250   8.385
MSpappp  47.600   8.941
MSppaaa  44.768   2.274
MSppaap  47.100   8.608
MSppapa  46.159   2.472
MSppapp  46.650   7.597
MSpppaa  45.000   9.188
MSpppap  43.800   9.855
MSppppa  42.450   8.617
MSppppp  41.600   9.730
MSaaaaaa  NaN   NaN
MSaaaaap  NaN   NaN
MSaaaapa  NaN   NaN
MSaaaapp  NaN   NaN
MSaaapaa  NaN   NaN
MSaaapap  NaN   NaN
MSaaappa  NaN   NaN
MSaaappp  NaN   NaN
MSaapaaa  NaN   NaN
MSaapaap  NaN   NaN
MSaapapa  NaN   NaN
MSaapapp  NaN   NaN
MSaappaa  NaN   NaN
MSaappap  NaN   NaN
MSaapppa  NaN   NaN
MSaapppp  NaN   NaN
MSapaaaa  NaN   NaN
MSapaaap  NaN   NaN
MSapaapa  NaN   NaN
MSapaapp  NaN   NaN
MSapapaa  NaN   NaN
MSapapap  NaN   NaN
MSapappa  NaN   NaN
MSapappp  NaN   NaN
MSappaaa  NaN   NaN
MSappaap  NaN   NaN
MSappapa  NaN   NaN
MSappapp  NaN   NaN
MSapppaa  NaN   NaN
MSapppap  NaN   NaN
MSappppa  NaN   NaN
MSappppp  NaN   NaN
MSpaaaaa  NaN   NaN
MSpaaaap  NaN   NaN
MSpaaapa  NaN   NaN
MSpaaapp  NaN   NaN
MSpaapaa  NaN   NaN
MSpaapap  NaN   NaN
MSpaappa  NaN   NaN
MSpaappp  NaN   NaN
MSpapaaa  NaN   NaN
MSpapaap  NaN   NaN
MSpapapa  NaN   NaN
MSpapapp  NaN   NaN
MSpappaa  NaN   NaN
MSpappap  NaN   NaN
MSpapppa  NaN   NaN
MSpapppp  NaN   NaN
MSppaaaa  NaN   NaN
MSppaaap  NaN   NaN
MSppaapa  NaN   NaN
MSppaapp  NaN   NaN
MSppapaa  NaN   NaN
MSppapap  NaN   NaN
MSppappa  NaN   NaN
MSppappp  NaN   NaN
MSpppaaa  NaN   NaN
MSpppaap  NaN   NaN
MSpppapa  NaN   NaN
MSpppapp  NaN   NaN
MSppppaa  NaN   NaN
MSppppap  NaN   NaN
MSpppppa  NaN   NaN
MSpppppp  NaN   NaN
E  18.101   1.308
Ea  39.770   2.048
Ep  41.965   1.917
Eal  44.693   2.017
Ear  45.445   1.915
Epl  46.885   2.076
Epr  46.616   2.033
Eala  67.327   2.591
Ealp  69.760   3.227
Eara  72.158   3.594
Earp  73.039   3.118
Epla  68.361   4.909
Eplp  73.436   4.714
Epra  72.975   4.419
Eprp  79.298   3.717
C  18.178   1.550
Ca  23.441   1.409
Cp  23.623   1.339
Caa  30.141   1.121
Cap  35.225   1.759
Cpa  29.513   1.095
Cpp  33.655   1.535
Caaa  35.904   1.781
Caap  43.769   1.965
Capa  52.778   1.978
Capp  45.482   1.927
Cpaa  34.239   1.325
Cpap  35.861   1.495
Cppa  52.010   2.534
Cppp  43.839   1.973
Caaaa  48.900   2.020
Caaap  46.120   1.835
Caapa  60.800   10.035
Caapp  53.717   2.035
Capaa  52.396   3.235
Capap  53.838   3.918
Cappa  51.947   4.314
Cappp  56.335   3.407
Cpaaa  51.275   2.481
Cpaap  48.636   2.214
Cpapa  54.735   3.192
Cpapp  58.705   3.140
Cppaa  52.650   3.979
Cppap  54.011   2.597
Cpppa  51.261   2.775
Cpppp  55.584   3.279
Caaaaa  21.158   12.584
Caaaap  22.842   11.950
Caaapa  28.053   10.373
Caaapp  26.842   10.489
Caapaa  13.500   9.192
Caapap  20.500   0.707
Caappa     
Caappp     
Capaaa  9.923   4.734
Capaap  7.538   4.075
Capapa  7.800   4.104
Capapp  7.400   4.526
Cappaa  11.118   6.698
Cappap  10.824   6.748
Capppa  11.667   6.513
Capppp  11.667   6.513
Cpaaaa  20.900   10.457
Cpaaap  20.350   10.781
Cpaapa  25.450   10.288
Cpaapp  26.850   10.510
Cpapaa  18.300   9.647
Cpapap  20.450   10.440
Cpappa     
Cpappp     
Cppaaa  9.000   4.226
Cppaap  10.067   5.470
Cppapa  8.462   4.754
Cppapp  8.538   4.612
Cpppaa  13.706   7.321
Cpppap  12.941   8.242
Cppppa  12.688   7.947
Cppppp  14.063   7.690
D  34.151   2.450
Da  42.982   2.599
Dp  43.565   2.315
Daa  48.801   2.779
Dap  43.548   2.368
Dpa  48.297   2.709
Dpp  43.810   2.746
Daaa  27.400   10.806
Daap  31.350   9.959
Dapa  34.000   11.425
Dapp  33.550   12.224
Dpaa  23.850   12.567
Dpap  31.450   9.567
Dppa  34.750   11.452
Dppp  32.800   11.660
P3  25.323   1.853
P4  64.685   6.636
Z3  74.833   16.838
Z2  78.171   14.435
