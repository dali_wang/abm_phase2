%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%     Please direct any questions or feedback to baoz@mskcc.org or
%     juliacompbio@gmail.com

guiPC
%load set of cells to be analyzed
if ~exist('zipFileName','var')
    error('No zip file entered, cannot perform analysis');
end
if isempty('outputFolder')
    error('No output folder entered, cannot perform analysis');
end
if exist('endCells','var')
    if endCells == 100
        load('cellsto100')
        cells = cellsTo100;
    elseif endCells == 200
        load('cellsto200')
        cells = cellsTo200;
    elseif endCells == 350
        load('cellsTo350')
        cells = cellsTo350;
    else
        load('cellsTo350')
        cells = cellsTo350;
    end
else
    disp('No ending number of cells chosen, using default 350');
    load('cellsTo350')
    cells = cellsTo350;
end


%Examine entered measurements
measures = {};
if exist('cellCycleFlag','var')
    if cellCycleFlag == 1
        measures = [measures 'Cell cycle'];
    end
end
if exist('generationAsychronyFlag','var')
    if generationAsychronyFlag == 1
        measures = [measures 'Generation asynchrony'];
    end
end
if exist('divisionTimeFlag','var')
    if divisionTimeFlag == 1
        measures = [measures 'Division time'];
    end
end
if exist('expressionFlag','var')
    if expressionFlag == 1
        measures = [measures 'Expression'];
    end
end
if exist('alignedPositionFlag','var')
    if alignedPositionFlag == 1
        measures = [measures 'Raw positions'];
    end
end
if exist('alignedNormalizedFlag','var')
    if alignedNormalizedFlag == 1
        measures = [measures 'Normalized positions'];
    end
end

%Examine entered movies
movies = {};
if exist('ab32movieFlag','var')
    if ab32movieFlag == 1
        movies = [movies 'AB32'];
    end
end
if exist('ab64movieFlag','var')
    if ab64movieFlag == 1
        movies = [movies 'AB64'];
    end
end
if exist('ab128movieFlag','var')
    if ab128movieFlag == 1
        movies = [movies 'AB128'];
    end
end
if exist('ab32to128Flag','var')
    if ab32to128Flag == 1
        movies = [movies 'AB32toAB128'];
    end
end
if exist('slidingWindowFlag','var')
    if slidingWindowFlag == 1
        movies = [movies 'window'];
    end
end
if exist('covarEllipseFlag','var')
    if covarEllipseFlag == 1
        movies = [movies 'ellipses'];
    end
end

%Examine entered end times
tEnd = zeros(1,6);
if exist('ABendTime','var')
    tEnd(1) = ABendTime;
    tEnd(2) = ABendTime;
end
if exist('MSendTime','var')
    tEnd(3) = MSendTime;
end
if exist('EendTime','var')
    tEnd(4) = EendTime;
end
if exist('CendTime','var')
    tEnd(5) = CendTime;
end
if exist('DPendTime','var')
    tEnd(6) = DPendTime;
end
zeroVal = find(tEnd == 0);
if length(zeroVal) == 6
    error('No end times given, cannot run analysis');
else
    tEnd(zeroVal) = min(tEnd(logical(tEnd)));
end

% make sure other parameters were entered, if not use defaults
if ~exist('timeResolution','var');
    disp('No seconds between images entered, using default 75');
    timeResolution = 75;
end
if ~exist('aspectRatio','var');
    disp('No aspect ratio entered, using default 0.25');
    aspectRatio = 0.25;
end
if ~exist('strain','var')
    strain = '';
end
if ~exist('adjustPval','var')
    disp('No p-value cutoff entered, using devfault 0.01');
    adjustPval = 0.01;
end

%Now that all input variables are organized, run the main analysis file
analyzeOneEmbryoExprNormPosAndMultipleTesting( ...
    zipFileName, outputFolder, cells, measures, movies, tEnd, ...
    timeResolution, aspectRatio, strain, adjustPval);

len = length(zipFileName);
lastSlash=0;
for c=1:len
    if strcmp(zipFileName(c),'/') || strcmp(zipFileName(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    outputDir = [outputFolder zipFileName(lastSlash:len-4)];
else
    outputDir = outputFolder;
end

%Display output browers for users to examine the resultsin their output directory
outputBrowserPC_BrowseOrEnterDir(outputDir);

clear('zipFileName','outputFolder','endCells','cellCycleFlag',...
    'generationAsychronyFlag','divisionTimeFlag','expressionFlag',...
    'alignedPositionFlag','alignedNormalizedFlag','measures','movies',...
    'ab32movieFlag','ab64movieFlag','ab128movieFlag','ab32to128Flag',...
    'slidingWindowFlag','tEnd','ABendTime','MSendTime',...
    'EendTime','CendTime','DPendTime','zeroVal','timeResolution',...
    'aspectRatio','strain','adjustPval');






