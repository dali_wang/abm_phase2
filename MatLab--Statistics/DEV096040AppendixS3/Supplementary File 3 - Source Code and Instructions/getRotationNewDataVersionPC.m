function [tx ty tz] = getRotationNewDataVersionPC(M,tEnd)
%Get the orientation of the embryo in the x and y direction.
%The cells on the long axis (x) are ABa and P2; these define the anterior 
%and posterior ends of the embryo, respectively. 
%The cells on the short axis (y) are ABp and EMS; these define the dorsal 
%and ventral poles. (from : "Automated cell lineage tracing in 
%Caenorhabditis elegans", Bao Et. Al. 2006)
%Input : M - coordinate data , must contain cells: ABa, P2, ABp, EMS
%Output :   xori - x orientation of the embryo +1 is for 
%           correct orientation as described above, -1 for opposite 
%           direction.
%           yori - in a similar fashion to xori, but along y axis 
%           orientation     

tx=0;ty=0;tz=0;

iABal= getIds(M, 'ABal');
iABar= getIds(M, 'ABar');
iABpl= getIds(M, 'ABpl');
iABpr= getIds(M, 'ABpr');
iP3  = getIds(M,'P3');

first=1;%length(c);

%Find positions of early cells to determine direction of left-right axis
ABal = [ [M(iABal).t]; [M(iABal).x]; [M(iABal).y];  [M(iABal).z];]';
ABar = [ [M(iABar).t]; [M(iABar).x]; [M(iABar).y];  [M(iABar).z];]';
ABpl = [ [M(iABpl).t]; [M(iABpl).x]; [M(iABpl).y];  [M(iABpl).z]]';
ABpr = [ [M(iABpr).t]; [M(iABpr).x]; [M(iABpr).y];  [M(iABpr).z]]';
P3 = [ [M(iP3).t]; [M(iP3).x]; [M(iP3).y];  [M(iP3).z]]';

flip=ones(1,2);
%c = intersect(intersect(intersect(intersect(ABal(:,1), ABar(:,1)),  ABpl(:,1)), ABpr(:,1)), P3(:,1));
c1 = intersect(ABal(:,1), ABar(:,1));
c2 = intersect(ABpl(:,1), ABpr(:,1));
ABal = ABal(min(c1) <= ABal(:,1) & ABal(:,1) <= max(c1), :);
ABar = ABar(min(c1) <= ABar(:,1) & ABar(:,1) <= max(c1), :);
ABpl = ABpl(min(c2) <= ABpl(:,1) & ABpl(:,1) <= max(c2),:);
ABpr = ABpr(min(c2) <= ABpr(:,1) & ABpr(:,1) <= max(c2),:);

% Flip if necessary
if ABal(1,4)<ABar(1,4)
    flip(1)=-1;
end
if ABpl(1,4)<ABpr(1,4)
    flip(2)=-1;
end

if sign(flip(1))~=sign(flip(2))
    if length(c1)>1 && length(c2)>1
        if ABal(2,4)<ABar(2,4)
            flip(1)=-1;
        end
        if ABpl(2,4)<ABpr(2,4)
            flip(2)=-1;
        end
        if sign(flip(1))~=sign(flip(2))
            disp('Cannot be determined whether left or right side is up');
        elseif flip(1)==-1
            tx=pi;
        end
    end
elseif flip(1)==-1
    tx=pi;
end

% Find all cells in last time point to determine orientation of
% anterior-posterior axis (direction with most variation)
lastIdxs = find([M(:).t]' == tEnd);
xLast = [M(lastIdxs).x]';
yLast = [M(lastIdxs).y]';
if isempty(xLast)
    tEnd = max([M(:).t]);
    lastIdxs = find([M(:).t]' == tEnd);
    xLast = [M(lastIdxs).x]';
    yLast = [M(lastIdxs).y]';
end
[coeff, ~] = princomp([xLast yLast]);
angle = abs(atan(coeff(1,2)/coeff(1,1)));
% disp(sprintf('coeff 1  %f  coeff 2  %f  angle %f',coeff(1,1), coeff(1,2),angle))
% disp(sprintf('ABa x: %7.3f, y: %7.3f, z: %7.3f',ABa(last,2),ABa(last,3),ABa(last,4)))
% disp(sprintf('P2 x: %7.3f, y: %7.3f, z: %7.3f',P2(last,2),P2(last,3),P2(last,4)))

% Rotate to align anterior-posterior axis
if ABal(1,2)<P3(1,2)
    if ABal(1,3)<P3(1,3)
        tz=angle;
    else
        tz=-angle;
    end
elseif ABal(1,3)<P3(1,3)
    tz=pi-angle;
else
    tz=-pi+angle;
end

% 
% if abs(P2(last,2)-ABa(last,2)) > abs(P2(last,3)-ABa(last,3))
%     xori = sign(P2(last,2)-ABa(last,2));
%     if xori==-1
%         tz=tz+pi;
%     end
% else
%     xori = sign(P2(last,3)-ABa(last,3));
%     if xori==-1
%         tz=tz+pi;
%     end
% end



% if mean(quadDifs)<0
%     ty=pi;
%     tz=tz-(apDif+dvDif)/2;
% elseif mean(quadDifs)>0
%     tz=tz+(apDif+dvDif)/2;
% else
%     error('Cannot be determined whether left or right side is up');
% end
% yori = unique(-sign(ABp(:,3)-EMS(:,3)));
% if (length (yori) ~= 1)
%     yori=sign(ABp(length(ABp(:,3)),3)-EMS(length(EMS(:,3)),3));
% %     error('y-orientation ambiguous!');
% end
% 
% xori = unique(sign(P2(:,2)-ABa(:,2)));
% if (length (xori) ~= 1)
%     xori=sign(P2(length(P2(:,2)),2)-ABa(length(ABa(:,2)),2));
% %     error('x-orientation ambiguous!');
% end
% if (xori == -1 )
%     if (yori == -1)  
%         tz=pi;
%     else
%         ty=pi;     
%     end
% else
%      if (yori == 1)        
%         tx=pi;
%      end
% end

% 
% if (yori==1 && xori==-1) || (yori==-1 && xori==1)
%   %  ty=pi;    
%     tz=tz-(apDif+dvDif)/2;%rotation about the z axis, not changing the z coordinates
% else
%     tz=tz+(apDif+dvDif)/2;%rotation about the z axis, not changing the z coordinates
% end
% if yori==1
%      tz=tz+(apDif+dvDif)/2;%rotation about the z axis, not changing the z coordinates
% else
%     tz=tz-(apDif+dvDif)/2;
% end
% if abs(tz)<0.1
%     tz=0;
% end

end

function id = getIds(M, cellnm)

id = find(strcmp([M.name]', cellnm));
if( isempty(id))
    error (['cell ' cellnm ' not found!']);
end

end


