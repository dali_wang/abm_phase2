function [M cellNames] = readnucleiWithRed(filename,tLastFlag)
%
%Reads a nuclei file 
%with the following line structure :
%1)file specific index (int)
%2)is valid (int)
%3)predessesor (int)
%4)successor1 or -1 (int)
%5)successor2 or -1 (int)
%6,7,8)x,y,z (int,int,float) 
%9)diameter (int , each unit equals 1 micron))
%10-)cell name 
%
%For example:
%1, 1, 1, 1, -1, 305, 228, 10.2, 74, MS, 1602934, 0, 0, 0, , 
%
%USAGE:  M = readnuclei('t021-nuclei');
%        M = readnuclei('t021-nuclei', [1 3 4 5 6]); 
%OUTPUT : M (nX4) containing nuclei numeric index
%col1 - time point
%col2 - file specific index
%col3 - successor id(0 if the cell dies or divides, index of next timepoint)
%col456 - xyz
%CREATE DATE: apr 21, 2008


%READ FILE
fid = fopen(filename);

if fid < 0
     error(['could not open file: ' filename]);  
end

% C = textscan(fid, '%f%d%d%f%f%f%f%f%f%s%*d%f%*f%*d%*s%*s', 'delimiter',
% ','); this was used before red correction was right
C = textscan(fid, '%f%d%d%f%f%f%f%f%f%s%*d%f%*f%*d%*s%*f%*f%*f%f%*f', 'delimiter', ',');
fclose(fid);

%PREPARE OUTPUT
if nargin == 2
     
%creating time vector from file name    
[pathstr, name] = fileparts(filename);
if (length(name) < 3)
    error('file name too short, could not extract the time point in which the file was taken')
end
t = str2double(name(2:4));
T = t*ones(length(C{1}),1);

%getting direct successors
if tLastFlag==0
    C{4}((C{4} == -1) | (C{5} > 0)) = 0; %0 when the cell dies or divides
else
    C{4}(:) = 0; %flag for being at the last time point
end
%M = [T, C{1},C{4}, C{6},C{7},C{8},C{9},C{11}]; was used before red
%correction was right
M = [T, C{1},C{4}, C{6},C{7},C{8},C{9},C{11}-C{12}];
cellNames = strtrim(C{10});
if  any( C{2} == -1)
    warning(['file : ' filename ' has invalid cells']);
end

valid_id =  find( C{2} == 1);  
M = M(valid_id,:);
cellNames = cellNames(valid_id);
else 
    error('col argument not implemented');
end

    
return;