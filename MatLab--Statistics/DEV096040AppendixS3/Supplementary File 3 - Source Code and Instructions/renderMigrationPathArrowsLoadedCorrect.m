function [first, last]=renderMigrationPathArrowsLoadedCorrect(MT,cells,folderName,fileName,cameraDist)

% MT is the loaded information for each embryo (from loadNucleiNewData).
% cells is a cell array with all the cell names to be examined.
% folderName is where the movie will be saved.
% fileName is the name of the file that will be saved within folderName.
% cameraDist is how far from the nuclei that the camera should be
%   positioned.  This will vary based on the imaging parameters.

% First is where all the cells are born, last is where the cells divide.

% Initialize variables
nCells=length(cells);
first=zeros(nCells,3);
last=zeros(nCells,3);
colors=zeros(nCells,1);

ids=name2id(MT.value,cells);
[scellid,IX] = sortrows([[MT.value.routid]' , [MT.value.t]'],[1 2] );%scellid lists the routids and time points each cell is alive for, IX telss you how they sorted rows correspond to the original M
[~, ind] = unique(scellid(:,1),'first');%gives the first entry in M that corresponds to each id
ind = [ind' ,length(MT.value)+1];%changes id vector to a row vector and ads the final index so each entry can be used as a range for each cell
MT.value = MT.value(IX);%resort M according to the order from sorted rows (could this give us trouble depending which row is changed first?)

for c=1:nCells
    if ids(c)~=-1
        % Identify the coordinates for each cell
        startVal=find([MT.value(:).routid] == ids(c),1,'first');
        endVal=find([MT.value(:).routid] == ids(c),1,'last');
        
        % Record the position of the cell when it is first born.
        first(c,1)=MT.value(startVal).x;
        first(c,2)=MT.value(startVal).z;
        first(c,3)=MT.value(startVal).y;
        
        % Record the position of the cell just before it divides or dies.
        last(c,1)=MT.value(endVal).x;
        last(c,2)=MT.value(endVal).z;
        last(c,3)=MT.value(endVal).y;
        
        % Determine the color of the cell based on its lineage group.
        lenName=length(cells{1,c});
        if lenName>=5
            if strcmp(cells{1,c}(1:5),'ABala')
                colors(c)=3; %orange               
            elseif strcmp(cells{1,c}(1:5),'ABalp')
                colors(c)=6;%2;
            elseif strcmp(cells{1,c}(1:5),'ABara')
                colors(c)=7;%3;
            elseif strcmp(cells{1,c}(1:5),'ABarp')
                colors(c)=4;
            elseif strcmp(cells{1,c}(1:5),'ABpla')
                colors(c)=2;%5;
            elseif strcmp(cells{1,c}(1:5),'ABplp')
                colors(c)=8;%6;
            elseif strcmp(cells{1,c}(1:5),'ABpra')
                colors(c)=12;%7;
            elseif strcmp(cells{1,c}(1:5),'ABprp')
                colors(c)=13;%8;
            end
        end
        if colors(c)==0
            if strcmp(cells{1,c}(1),'M')
                if length(cells{1,c})>2
                    if strcmp(cells{1,c}(3),'a')
                        colors(c)=5;
                    else
                        colors(c)=10;
                    end
                else
                    colors(c)=5;%14;
                end
            elseif strcmp(cells{1,c}(1),'E')
                colors(c)=1;%9; %magenta
            elseif strcmp(cells{1,c}(1),'C')
                colors(c)=14;%10;
            elseif strcmp(cells{1,c}(1),'D')
                colors(c)=9;%12;
            elseif strcmp(cells{1,c}(1),'P') || strcmp(cells{1,c}(1),'Z')
                colors(c)=11;%13;
            end
        end
    end
end

% Create the movie.
center=[0 0 0];
specifySphereSweepNoPerl(folderName,fileName,first,last,colors,center,cameraDist)
