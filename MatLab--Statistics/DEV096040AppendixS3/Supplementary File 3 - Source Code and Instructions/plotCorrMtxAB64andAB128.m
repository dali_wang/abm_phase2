function plotCorrMtxAB64andAB128(M, labels, options, stage)
%Plots a labled matrix, and colors each cell of a numeric matrix with a
%user specified colormap.
%Matrix Labels can be rearranged by changing order of entries in labels
%(just make sure that it corresponds to the matrix)
%
%variable
%Options :
%   coloring scheme: bw/rwb
%   display nans : xnan
%   partial matrix : upper,lower
%Usage:
%plotCorrMtx(rand(104), CELLSAB64,1,  'bw,lower')

if nargin < 3
    options = 'rwb';
end

if size(labels,1) > 1
    labels = labels';
end

% if size(M,1) ~= length(labels)
%     error('matrix and lables are not the same size!')
% end

tmp = M;
M(isnan(M(:))) = 0;
n = length(M);




if  exist_(options,'bw')
    colormap(flipud(gray(10)));
    nullVal = min(M(:));
elseif exist_(options,'rwb')
    colormap(french(100));
    nullVal = mean(M(:));    
else
    error('options not recognized: I only know bw, rwb');
end

if exist_(options,'lower')
    for i=1:n for j=i+1:n M(i,j) = nullVal ;end;end
elseif exist_(options,'upper')
    for i=1:n for j=i:n M(j,i) = nullVal ;end;end
end


imagesc(M)

hold on
if exist_(options,'xnan')
    [row col] = ind2sub(size(tmp), find(isnan(tmp(:))));
    plot(row,col,'xk');
else
    M(isnan(M)) = 0;
end

lineages = {'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa','MSp','E','C','D'};
if ~isempty(labels)
    for i=1:length(lineages)
        res =  ~cellfun(@isempty, regexpi(labels, strcat('^', lineages(i))));
        X = [find(diff(res))' find(diff(res))']+0.5;
        Y = [];
        for j = 1:length(find(diff(res)))

            Y = [Y; 0 800 ];
        end


        line(X', Y','Color','k')
        line(Y', X','Color','k')
    end
end


if strcmp(stage,'AB64')
    set(gca,'XTick',[4.5 12.5 20.5 28.5 36.5 44.5 52.5 60.5 68.5 76.5 82.5 88.5 94.5 97.5])
    set(gca,'XTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa8','MSp8','E','C','D','P'})
    set(gca,'YTick',[4.5 12.5 20.5 28.5 36.5 44.5 52.5 60.5 68.5 76.5 82.5 88.5 94.5 97.5])
    set(gca,'YTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa8','MSp8','E','C','D','P'})
    
elseif strcmp(stage,'AB128')
    %AB128
    set(gca,'XTick',       [8.5     24.5    40.5    56.5   72.5    88.5    104.5   120.5   136.5  152.5   164.5 176.5 188.5 193.5])
    set(gca,'XTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa32','MSp32','E8','C32','D8','P'})
    set(gca,'YTick',       [8.5     24.5    40.5    56.5   72.5    88.5    104.5   120.5   136.5  152.5   164.5 176.5 188.5 193.5])
    set(gca,'YTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa32','MSp32','E8','C32','D8','P'})
    % set(gca,'XTick',[4.5 12.5 20.5 28.5 36.5 44.5 52.5 60.5 68.5 76.5 84.5 90.5 96.5 103.5 107.5])
    % set(gca,'XTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MS8','MSa8','MSp8','E','C','D','P'})
    % set(gca,'YTick',[4.5 12.5 20.5 28.5 36.5 44.5 52.5 60.5 66.5 70.5 76.5 84.5 90.5 96.5 103.5 107.5])
    % set(gca,'YTickLabel',{'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa4','MSp4','MSa8','MSp8','E','C','D','P'})
    % set(gca,'YTick',1:length(labels))
    % set(gca,'YTickLabel',labels)
end


end

function p = exist_(string, pattern)
p = ~isempty( strfind(string, pattern));
end

% for i = 1:length(labels)
%     text(-4,i-0.5 , labels(i));
% end


% text(4, -1, 'ala'); text(4+8, -1, 'alp'); text(4+16, -1, 'ara');
% text(4+24, -1, 'arp');text(4+32, -1, 'pla'); text(4+40, -1, 'plp'); text(4+48, -1, 'pra');
% text(4+56, -1, 'prp');
% text(4+56+8, -1, 'C^8');
% text(1+56+8+8, -1, 'D^2');
% text(2+56+8+8+2, -1,'D^4');
% text(2+56+8+8+2+4, -1, 'E');
% text(4+56+8+8+2+4+4, -1, 'MS^8');
% text(8+56+8+8+2+4+4+8, -1, 'MS^1^6');
