function MT = loadNucleiNewData(EMBS,tEnds, aspectRatio)
%the script by default loads data of 10 embryos into 2 global variables.
%input- 'nuclei' directories.
%output- MT , STATS

n = length(EMBS);
MT = [];

for i = 1:n
    try
        emb = EMBS(i);
        tEnd=tEnds(i,:);
        lastSlash=0;
        len=length(EMBS{i});
        for s=1:len-1
            if strcmp(EMBS{i}(s),'/') || strcmp(EMBS{i}(s),'\')
                lastSlash=s;
            end
        end
        if lastSlash~=0
            if strcmp(EMBS{i}(len-2:len),'csv')
                emb = emb{:};
                disp(sprintf('Loading %s',emb(lastSlash+1:len-4))); %#ok<*DSPS>
                tmpMT = readCsvNuclei(emb,tEnd,aspectRatio(i));
                MT(i).key = emb(lastSlash+1:len-4);
                MT(i).value = tmpMT;
            else
                emb = emb{:};
                disp(sprintf('Loading %s',emb(lastSlash+1:len)));
                [tmpMT] = readnucleiDirWithRed([emb '/nuclei'],tEnd,aspectRatio(i));
                MT(i).key = emb(lastSlash+1:len);
                MT(i).value = tmpMT;
            end
        else
            error('%s is not a valid embryo directory',emb)
        end
    catch ME
        i
        EMBS{i}
        ME.message
    end

end
