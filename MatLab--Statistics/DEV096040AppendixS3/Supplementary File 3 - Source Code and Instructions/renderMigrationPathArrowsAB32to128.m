function [first, last]=renderMigrationPathArrowsAB32to128(paths,cells,folderName,fileName,cameraDist,largeSizeFlag)

% MT is the loaded information for each embryo (from loadNucleiNewData).
% cells is a cell array with all the cell names to be examined.
% folderName is where the movie will be saved.
% fileName is the name of the file that will be saved within folderName.
% cameraDist is how far from the nuclei that the camera should be
%   positioned.  This will vary based on the imaging parameters.

% First is where all the cells are born, last is where the cells divide.

% Initialize variables
nAllCells = length(cells);
load('cellSets32_64_128');
showCells = [CELLSAB32 CELLSAB64 CELLSAB128];
nShowCells = length(showCells);
first=zeros(nShowCells,3);
last=zeros(nShowCells,3);
colors=zeros(nShowCells,1);


for cAll=1:nAllCells
    for cShow = 1:nShowCells
        if strcmp(cells{cAll},showCells{cShow})
            if ~isempty(paths{cAll})
                 % Identify the coordinates for each cell
                len = length(paths{cAll}(:,1));
                
                % Record the position of the cell when it is first born.
                first(cShow,1) = paths{cAll}(1,1);
                first(cShow,2) = paths{cAll}(1,3);
                first(cShow,3) = paths{cAll}(1,2);
                
                % Record the position of the cell just before it divides or dies.
                last(cShow,1) = paths{cAll}(len,1);
                last(cShow,2) = paths{cAll}(len,3);
                last(cShow,3) = paths{cAll}(len,2);
            end
        end
        
        % Determine the color of the cell based on its lineage group.
        lenName = length(showCells{1,cShow});
        if lenName>=5
            if strcmp(showCells{1,cShow}(1:5),'ABala')
                colors(cShow)=3; %orange               
            elseif strcmp(showCells{1,cShow}(1:5),'ABalp')
                colors(cShow)=6;%2;
            elseif strcmp(showCells{1,cShow}(1:5),'ABara')
                colors(cShow)=7;%3;
            elseif strcmp(showCells{1,cShow}(1:5),'ABarp')
                colors(cShow)=4;
            elseif strcmp(showCells{1,cShow}(1:5),'ABpla')
                colors(cShow)=2;%5;
            elseif strcmp(showCells{1,cShow}(1:5),'ABplp')
                colors(cShow)=8;%6;
            elseif strcmp(showCells{1,cShow}(1:5),'ABpra')
                colors(cShow)=12;%7;
            elseif strcmp(showCells{1,cShow}(1:5),'ABprp')
                colors(cShow)=13;%8;
            end
        end
        if colors(cShow)==0
            if strcmp(showCells{1,cShow}(1),'M')
                if length(showCells{1,cShow})>2
                    if strcmp(showCells{1,cShow}(3),'a')
                        colors(cShow)=5;
                    else
                        colors(cShow)=10;
                    end
                else
                    colors(cShow)=5;%14;
                end
            elseif strcmp(showCells{1,cShow}(1),'E')
                colors(cShow)=1;%9; %magenta
            elseif strcmp(showCells{1,cShow}(1),'C')
                colors(cShow)=14;%10;
            elseif strcmp(showCells{1,cShow}(1),'D')
                colors(cShow)=9;%12;
            elseif strcmp(showCells{1,cShow}(1),'P') || strcmp(showCells{1,cShow}(1),'Z')
                colors(cShow)=11;%13;
            end
        end
    end
end

% Shift the AB32 and AB128 sizes to the left and right.
xMax = max( [ max(first(:,1)) max(last(:,1)) ]);
xMin = min( [ min(first(:,1)) min(last(:,1)) ]);
xRange = xMax - xMin;
if xRange > 62
    xShift = xRange;
else
    xShift = 62;
end
first(1:45,1) = first(1:45,1) - xShift;
last(1:45,1) = last(1:45,1) - xShift;
first(144:337,1) = first(144:337,1) + xShift;
last(144:337,1) = last(144:337,1) + xShift;
center=[0 0 0];

% Create the movie.
if exist('largeSizeFlag','var')
    if largeSizeFlag == 1
        specifySphereSweepNoPerl(folderName,fileName,first,last,colors,center,cameraDist,largeSizeFlag)
    end
else
    specifySphereSweepNoPerl(folderName,fileName,first,last,colors,center,cameraDist,0)
end

