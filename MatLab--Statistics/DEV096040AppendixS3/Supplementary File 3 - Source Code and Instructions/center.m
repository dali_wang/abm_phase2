function M = center(M,tEnd)
%Centers embryo coordinates to the origin

%m = [[M.x]; [M.y]; [M.z]]';
tLast = find([M.t] == tEnd);
lenLast = length(tLast);
m = [[M.x]; [M.y]; [M.z]]';
if lenLast > 100
    mMini = [sort([M(tLast).x]); sort([M(tLast).y]); sort([M(tLast).z])]';
    center = (mMini(lenLast - 1,:) - mMini(2,:))/2 + mMini(2,:);
else
    disp('Last time point has less than 100 cells')
    center = mean(m);
end
%  sortedM = zeros(nVals,3);
% for i = 1:3
%     sortedM(:,i) = sort(m(:,i));
% end
% onePerc = 200;%;round(nVals / 10);
% center = (sortedM(nVals - onePerc,:) - sortedM(onePerc,:))/2 + sortedM(onePerc,:);
% center = (max(m,[],1) - min(m,[],1))/2 + min(m,[],1);%mean(m);
% center = mean(m);
% figure;
% for comp = 1:3
%     subplot(1,3,comp)
%     hist(m(:,comp),15)
%     line([centerMean(comp) centerMean(comp)], [0 2000],'Color',[1 0 0])
%     line([center(comp) center(comp)], [0 2000],'Color',[0 1 0])
% end
m = [m(:,1)'-center(1); m(:,2)'-center(2); m(:,3)'-center(3)]';

M = setVecField(M, 'x', m(:,1));
M = setVecField(M, 'y', m(:,2));
M = setVecField(M, 'z', m(:,3));

