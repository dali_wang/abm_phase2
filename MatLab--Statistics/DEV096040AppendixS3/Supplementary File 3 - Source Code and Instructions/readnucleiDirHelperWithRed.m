function M = readnucleiDirHelperWithRed(dirname,tEnd)
%
%Reads a directory of nuclei files
%Nuclei files names should be in the following format txxx*
%for instance: 't021-nuclei'
%USAGE:  M = readnucleiDir('data/nuclei');
%
%OUTPUT : M is an vector of structs each containing the following fields
%t      -time 
%id     -id from file
%successor- id in successive time point (-1 if no successor)
%x,y,z  - cartesian location 
%diameter   - cell diameter
%
%CREATE DATE: apr 21, 2008


if (exist(dirname,'dir') == 0)
    error(['Directory ',dirname,' does not exist!']);
end
D = dir (dirname);

nfiles = length(D);
if nfiles>tEnd+2
    nfiles=tEnd+2;
end
nloaded = 0;
M_ = [];
cellNames = [];
fprintf('    ');
for i=1:nfiles-1
    if D(i).isdir == 0 && length(D(i).name) >3
        dispprecentage(i , nfiles, 10)
        filename = D(i).name;
        [Mtmp ,cntmp] =  readnucleiWithRed([dirname '/' filename],0);
        M_ = [M_ ; Mtmp];
        cellNames = [cellNames; cntmp];
        nloaded = nloaded+1;
    end
end
if D(nfiles).isdir == 0 && length(D(nfiles).name) >3
    dispprecentage(nfiles , nfiles, 10)
    filename = D(nfiles).name;
    [Mtmp ,cntmp] =  readnucleiWithRed([dirname '/' filename],1);
    M_ = [M_ ; Mtmp];
    cellNames = [cellNames; cntmp];
    nloaded = nloaded+1;
end
fprintf('\n\n');
%check data
u = unique(M_(:,1)) ;

missing_data_id = find(diff(sort(u)) ~= 1);
if(missing_data_id)
    
    warning('some time points are missing, are you missing any files?');
    warning(['Hint, look for the following time points: ' int2str(u(missing_data_id)'+1)]);
end
    
%create struct
disp('transferring data to struct');
n = length(M_);
M = repmat(struct('t', 0, 'id', 0, 'successor', 0,'diameter', 0,'x', 0,'y', 0,'z', 0,'name', 0,'rfp',0), 1, n);
for i=1:n
    M(i).t = M_(i,1);
    M(i).id = M_(i,2);
    M(i).successor = M_(i,3);
    M(i).x = M_(i,4);
    M(i).y = M_(i,5);
    M(i).z = M_(i,6);
    M(i).diameter = M_(i,7);
    M(i).name = cellNames(i);
    M(i).rfp = M_(i,8);
    
end
    
    

disp(['t0 : ' int2str(min(u)) ', tn : ' int2str(max(u))])
disp(['total files in directory : ' int2str(nfiles-2)])
disp(['successfully loaded : ' int2str(nloaded)])



return;        
