function [ pValMtx, percentMtx ] = measuresToPvalsAndPercentsWithPosition(measureMtx, cells, type)
%Takes a matrix of measures of a specific type, the cells measured and the
%type of measurement and returns a matrix of pValues

%Initialize p-value matrix
[nMeasures nCells]=size(measureMtx);
measureMtx = measureMtx';
pValMtx = zeros(nCells, nMeasures);
percentMtx = zeros(nCells, nMeasures);

%Load wild type data for measurement of interest
if strcmp(type,'unscaledCycle')
    load('unscaledReferenceCycleLengths');
elseif strcmp(type,'scaledCycle')
    load('scaledReferenceCycleLengths');
elseif strcmp(type,'unscaledDivision')
    load('unscaledReferenceDivisionTimes');
elseif strcmp(type,'scaledDivision')
    load('scaledReferenceDivisionTimes');
elseif strcmp(type,'genIndiv')
    load('generationTimingReference');
elseif strcmp(type,'ExpressionRW10348')
    load('RW10348probabilities');
elseif strcmp(type,'ExpressionRW10714')
    load('RW10714probabilities');
elseif strcmp(type,'ExpressionBV82')
    load('BV82probabilities');
elseif strcmp(type,'ExpressionRW10434')
    load('RW10434probabilities');
elseif strcmp(type,'ExpressionRW10425')
    load('RW10425probabilities');
elseif strcmp(type,'rawPositions')
    load('positionStats');
 %   load('domainChanges');
elseif strcmp(type,'normalizedPositions')
    load('normalizedPositionStats');
elseif strcmp(type,'axis')
    load('embryoSizeStats');
else
    error('Measurement type %s not recognized',type);
end

%Initialize storage matrix for wild type values
%Row 1 will hold means, and row 2 will hold standard deviations
if ~strcmp(type,'axis')
wtMean = zeros(nCells,nMeasures);
wtStd = zeros(nCells,nMeasures);
for c=1:nCells
    %Make sure the cell has a name
    if ~isempty(cells{1,c})
        %Find which lineage it belongs to
        if strcmp('A',cells{1,c}(1))
            baseLen=length(cells{1,c})-1;
            i=1;
            startIdx=1;
            while(i<baseLen)
                %Figure out what generation of AB cells we are on.
                startIdx=startIdx+2^(i-1);
                i=i+1;
            end
            if startIdx>length(ABcycles);
                error('The average cell cycle length for %s has not been calculated.',cells{1,c});
            end
            i=1;
            suppIdx=0;
            while(i<baseLen)
                %Find index of this cell within that generation
                if (strcmp(cells{1,c}(2+i),'p') || strcmp(cells{1,c}(2+i),'r') || strcmp(cells{1,c}(2+i),'v'))
                    suppIdx=suppIdx+2^(baseLen-i-1);
                end
                i=i+1;
            end
            %Assign the proper statistics from the loaded data
            if length(ABcycles(2,:)) >= startIdx+suppIdx
                wtMean(c,:)=ABcycles{2,startIdx+suppIdx,:};
                wtStd(c,:)=ABcycles{3,startIdx+suppIdx,:};
            else
                wtMean(c,:) = 0;
                wtStd(c,:) = 100;
            end
        elseif strcmp('E',cells{1,c}(1))
            if strcmp('EMS',cells{1,c})
                wtMean(c,:)=Ecycles{2,1,:};
                wtStd(c,:)=Ecycles{3,1,:};
            else
                baseLen=length(cells{1,c});
                i=1;
                startIdx=2;
                while(i<baseLen)
                    % Figure out what generation of E cells we are on.
                    startIdx=startIdx+2^(i-1);
                    i=i+1;
                end
                if startIdx>length(Ecycles);
                    error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                end
                i=1;
                suppIdx=0;
                while(i<baseLen)
                    %Find index of this cell within that generation
                    if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                        suppIdx=suppIdx+2^(baseLen-i-1);
                    end
                    i=i+1;
                end
                wtMean(c,:)=Ecycles{2,startIdx+suppIdx,:};
                wtStd(c,:)=Ecycles{3,startIdx+suppIdx,:};
            end
        elseif strcmp('M',cells{1,c}(1))
            baseLen=length(cells{1,c})-1;
            i=1;
            startIdx=1;
            while(i<baseLen)
                %Figure out what generation of MS cells we are on.
                startIdx=startIdx+2^(i-1);
                i=i+1;
            end
            if startIdx>length(MScycles);
                error('The average cell cycle length for %s has not been calculated.',cells{1,c});
            end
            i=1;
            suppIdx=0;
            while(i<baseLen)
                %Find index of this cell within that generation
                if (strcmp(cells{1,c}(2+i),'p') || strcmp(cells{1,c}(2+i),'r'))
                    suppIdx=suppIdx+2^(baseLen-i-1);
                end
                i=i+1;
            end
            wtMean(c,:)=MScycles{2,startIdx+suppIdx,:};
            wtStd(c,:)=MScycles{3,startIdx+suppIdx,:};
        elseif strcmp('C',cells{1,c}(1))
            baseLen=length(cells{1,c});
            i=1;
            startIdx=1;
            while(i<baseLen)
                %Figure out what generation of C cells we are on.
                startIdx=startIdx+2^(i-1);
                i=i+1;
            end
            if startIdx>length(Ccycles);
                error('The average cell cycle length for %s has not been calculated.',cells{1,c});
            end
            i=1;
            suppIdx=0;
            while(i<baseLen)
                %Find index of this cell within that generation
                if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                    suppIdx=suppIdx+2^(baseLen-i-1);
                end
                i=i+1;
            end
            wtMean(c,:)=Ccycles{2,startIdx+suppIdx,:};
            wtStd(c,:)=Ccycles{3,startIdx+suppIdx,:};
        elseif strcmp('D',cells{1,c}(1))
            baseLen=length(cells{1,c});
            i=1;
            startIdx=1;
            while(i<baseLen)
                %Figure out what generation of D cells we are on.
                startIdx=startIdx+2^(i-1);
                i=i+1;
            end
            if startIdx>length(Dcycles);
                error('The average cell cycle length for %s has not been calculated.',cells{1,c});
            end
            i=1;
            suppIdx=0;
            while(i<baseLen)
                %Find index of this cell within that generation
                if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                    suppIdx=suppIdx+2^(baseLen-i-1);
                end
                i=i+1;
            end
            wtMean(c,:)=Dcycles{2,startIdx+suppIdx,:};
            wtStd(c,:)=Dcycles{3,startIdx+suppIdx,:};
        elseif (strcmp('P',cells{1,c}(1)) || strcmp('Z',cells{1,c}(1)))
            %P lineage names are a special case so each is compared
            if strcmp('P0',cells{1,c})
                wtMean(c,:)=Pcycles{2,1,:};
                wtStd(c,:)=Pcycles{3,1,:};
            elseif strcmp('P1',cells{1,c})
                wtMean(c,:)=Pcycles{2,2,:};
                wtStd(c,:)=Pcycles{3,2,:};
            elseif strcmp('P2',cells{1,c})
                wtMean(c,:)=Pcycles{2,3,:};
                wtStd(c,:)=Pcycles{3,3,:};
            elseif strcmp('P3',cells{1,c})
                wtMean(c,:)=Pcycles{2,4,:};
                wtStd(c,:)=Pcycles{3,4,:};
            elseif strcmp('P4',cells{1,c})
                wtMean(c,:)=Pcycles{2,5,:};
                wtStd(c,:)=Pcycles{3,5,:};
            elseif strcmp('Z3',cells{1,c})
                wtMean(c,:)=Pcycles{2,6,:};
                wtStd(c,:)=Pcycles{3,6,:};
            elseif strcmp('Z2',cells{1,c})
                wtMean(c,:)=Pcycles{2,7,:};
                wtStd(c,:)=Pcycles{3,7,:};
            end
        else
            disp(sprintf('Cell %s does not belong to any lineage',cells{1,c}));
        end
    end
end
else
    wtMean = meanSizes;
    wtStd = stdSizes;
end

%Compare measure to wild type mean and standard deviation and find p-value
%from loaded chart
load('pValChart');
for c=1:nCells
    if strcmp(type,'genIndiv')
        percentOff = measureMtx(c,:);
    else
        percentOff = (measureMtx(c,:)-wtMean(c,:))./wtMean(c,:);
    end
    nStdsOff = (measureMtx(c,:)-wtMean(c,:))./wtStd(c,:);
    row = floor(abs(nStdsOff*10));
    col = round((abs(nStdsOff)*100-row*10));
    for e = 1:nMeasures
        if row(e)>199
            row(e)=199;
            col(e)=9;
        end
        if col(e) == 10
            col(e) = 9;
        end
        if (isinf(nStdsOff(e)) || isnan(nStdsOff(e)))
            pValMtx(c,e) = 0.51;
            percentMtx(c,e) = 0.51;
        elseif nStdsOff(e)>0
            pValMtx(c,e) = pvals(row(e)+1,col(e)+1);
            percentMtx(c,e) = percentOff(e);
        else
            pValMtx(c,e) = -pvals(row(e)+1,col(e)+1);
            percentMtx(c,e) = percentOff(e);
        end
    end
end
if strcmp(type,'genIndiv')
    for c=1:nCells
        if strcmp(cells{1,c},'MS') ||strcmp(cells{1,c},'E') || ...
                strcmp(cells{1,c},'C') || strcmp(cells{1,c},'D') || ...
                strcmp(cells{1,c},'P3') || strcmp(cells{1,c},'P4') || ...
                strcmp(cells{1,c},'Z2') || strcmp(cells{1,c},'Z3')
            pValMtx(c,:)=0.4;
            percentMtx(c,:)=0;
        end
    end
end

percentMtx = percentMtx';
pValMtx = pValMtx';
