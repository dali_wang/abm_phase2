function dispprecentage(i , n, nrefresh)
%Why stare at an empty screen while your algorithm runs?
%The function displays the percentage of job the algorithm completed. 
%See also matlabs built in GUI 'waitbar'.

if ~mod(i ,nrefresh)
       fprintf('%c%c%c%c', 8, 8,8,8);
       fprintf('%3d%%', floor(100*i/n));
       
end