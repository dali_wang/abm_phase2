function analyzeOneEmbryoExprNormPosAndMultipleTesting(zipFileName, outputFolder, cells, measures, movies, tEnd, timeResolution, aspectRatio, strain, adjustPval)
%     zipFileName specifies the location of the zip file containing the nuclei
%        files to be analyzed
%     outputFolder is where all the analysis will be stored, the name of the zip
%        file will be used to specify the output directory within outputFolder
%     cells is a cell array of cells to be analyzed, {'all'} specifies a default set
%     measures defines the measurements to be used
%     movies is the set of movies to be saved
%     tEnd specifies the lineage separated ending times of lineging
%        (AB, MS, E, C, D, P and Z)
%     timeResolution specifies the number of seconds between successive images
%     aspectRatio specifies the aspect ratio between pixel sizes within and
%        between imaging planes
%     strain specifies the name of the C. elegans strain being examined,
%        statistics are compiled for BV82, RW10348, RW10434, RW10425, RW10714,
%        and no marker (which will be used if any other string is entered)
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%     Please direct any questions or feedback to baoz@mskcc.org or
%     juliacompbio@gmail.com


% Finds the end of the zip file name that will be used as the embryo name
zfnLen=length(zipFileName);
lastSlash=0;
for c=1:zfnLen
    if strcmp(zipFileName(c),'/') || strcmp(zipFileName(c),'\')
        lastSlash=c;
    end
end
if lastSlash==0
    embName=zipFileName;
elseif lastSlash-4<zfnLen
    embName=zipFileName(lastSlash+1:zfnLen-4);
else
    embName=zipFileName(lastSlash+1:zfnLen);
end
disp(sprintf('Processing embryo %s',embName));

% Create the output folder
outputFolder = [outputFolder '/' embName];
if ~exist(outputFolder,'dir')
    mkdir(outputFolder);
end
%Put the nuclei files in the output folder


nCells=length(cells); %determine number of cells to be analyzed
if nCells==1 && (strcmp(cells{1},'all') || strcmp(cells{1},'All'))
    %if all cells is specified, define variable
        load('cellsTo350');
        cells = cellsTo350;
        clear cellsTo350
        nCells=length(cells);
end
[genInfo, genType, genLabels, genIdx] = findGenerations(cells);

% load nuclei file information
if ~strcmp(zipFileName(zfnLen-2:zfnLen),'csv')
    if ~exist([outputFolder '/nuclei'],'dir')
        unzip(zipFileName,outputFolder);
    end
    MT = loadNucleiNewData({outputFolder},tEnd,aspectRatio);
else
    MT = loadNucleiNewData({zipFileName}, tEnd, aspectRatio);
end

% change time values from corresponding to file number to corresponding to minutes
lenMT=length(MT.value);
for c=1:lenMT
    MT.value(c).t=MT.value(c).t*timeResolution/60;
end

ids=name2id(MT.value,cells);
[scellid,IX] = sortrows([[MT.value.routid]' , [MT.value.t]'],[1 2] );%scellid lists the routids and time points each cell is alive for, IX telss you how they sorted rows correspond to the original M
[~, ind] = unique(scellid(:,1),'first');%gives the first entry in M that corresponds to each id
ind = [ind' ,length(MT.value)+1];%changes id vector to a row vector and ads the final index so each entry can be used as a range for each cell
MT.value = MT.value(IX);%resort M according to the order from sorted rows (could this give us trouble depending which row is changed first?)

% if "all" measurements were specified, write them individually
if length(measures)==1 && (strcmp(measures{1},'all') || strcmp(measures{1},'All'))
    measures={'Cell cycle', 'Generation asynchrony', 'Division time','Expression','Raw positions','Normalized positions'};
end
nMeasures=length(measures);
measureSize = 0;

% cell cycle data is displayed differently in the p-value matrix when it is
% judged as being "too long" of a cell cycle even though the cell division 
% has not been seen
cycleMeasureFlag = [];
for m = 1:nMeasures
    if (strcmp(measures{m},'Cell cycle') ||  strcmp(measures{1,m},'cycle') || ...
            strcmp(measures{1,m},'Cell cycle') || strcmp(measures{1,m},'cellCycle') || ...
            strcmp(measures{1,m},'cell cycle'))
        measureSize = measureSize + 2;
        cycleMeasureFlag = [cycleMeasureFlag 1 1];
    elseif (strcmp(measures{1,m},'Division Time') || strcmp(measures{1,m},'division') || ...
            strcmp(measures{1,m},'Division time') || strcmp(measures{1,m},'divisionTime') || ...
            strcmp(measures{1,m},'division time'))
        measureSize = measureSize + 1;
        cycleMeasureFlag = [cycleMeasureFlag 1];
    elseif (strcmp(measures{1,m},'Generation Asynchrony') || strcmp(measures{1,m},'generation') || ...
            strcmp(measures{1,m},'Generation asynchrony') || strcmp(measures{1,m},'generationAsynchrony') || ...
            strcmp(measures{1,m},'generation asynchrony'))
        measureSize = measureSize + 1;
        cycleMeasureFlag = [cycleMeasureFlag 1];
    elseif (strcmp(measures{1,m},'Expression') || strcmp(measures{1,m},'expression'))
        measureSize = measureSize + 1;
        cycleMeasureFlag = [cycleMeasureFlag 0];
    elseif (strcmp(measures{1,m},'rawPositions') || strcmp(measures{1,m},'raw positions') || strcmp(measures{1,m},'Raw positions')...
            || strcmp(measures{1,m},'Raw Positions'))
        measureSize = measureSize + 7;
        cycleMeasureFlag = [cycleMeasureFlag zeros(1,7)];
    elseif (strcmp(measures{1,m},'normalizedPositions') || strcmp(measures{1,m},'normalized positions') || strcmp(measures{1,m},'Normalized Positions') ||...
            strcmp(measures{1,m},'Normalized positions') || strcmp(measures{1,m},'Normalized Positions'))
        measureSize = measureSize + 7;
        cycleMeasureFlag = [cycleMeasureFlag zeros(1,7)];
    end
end

% If "all" movies are to be made, specify their names
nMovies=length(movies);
if nMovies==1 && (strcmp(movies{1},'all') || strcmp(movies{1},'All'))
    movies={'AB32', 'AB64', 'AB128', 'window', 'ellipses'};
    nMovies=4;
end

cycleFlag = 0;
for m = 1:nMeasures
    if (strcmp(measures{m},'Cell cycle') ||  strcmp(measures{1,m},'cycle') || ...
            strcmp(measures{1,m},'Cell cycle') || strcmp(measures{1,m},'cellCycle') || ...
            strcmp(measures{1,m},'cell cycle') || strcmp(measures{1,m},'Positions') || ...
            strcmp(measures{1,m},'positions'))
        cycleFlag = 1;
    end
end

phenoMatrix=zeros(measureSize,nCells);%pheno matrix will store p values
measuresMatrix=zeros(measureSize,nCells);%measures matrix will store absoulte measurements
percentMatrix=zeros(measureSize,nCells); %finish adding this stuff
%    Find cell cycle lengths first to determine if scaling is useful
cellStatus = zeros(measureSize,nCells);
[cellCycleInfo,cycleLength,divisionTime]=analyzeCycleTiming(MT,cells,ids,ind);

[scalingInfo,~,absGenMeans,genMeans,genIndiv,genMeanLinGroups,robustWeights,~,~]...
    =findScalingInfoUsingGenerationSlopeCycleAnyCellSet(cycleLength,cells,divisionTime,cellCycleInfo(4,:));

% Write embryo level information about pace of proliferation
fid=fopen([outputFolder '/embryoLevelInformation.txt'],'w');
fprintf(fid,['Scaling factor ' num2str(scalingInfo(1),'%7.2f') '\n']);
fprintf(fid,['Error from robus fitting ' num2str(scalingInfo(2),'%7.2f') '\n']);
if scalingInfo(3)==0
    fprintf(fid,'Embryo is scalable.\n');
elseif scalingInfo(3)==1
    fprintf(fid, 'Embryo is either slowing down or speeding up (see p-value matrix).\n');
elseif scalingInfo(3)==2
    fprintf(fid, 'Embryo has a non-linear overall clock.\n');
elseif scalingInfo(3)==3
    fprintf(fid, 'Embryo is either slowing down or speeding up (see p-value matrix) and has a non-linear overall clock.\n');
end

fprintf(fid,'Ratio to wild type division timing means by lineage group \n');
fprintf(fid,['AB ' num2str(genMeanLinGroups(1),'%7.2f') '\n']);
fprintf(fid,['MS ' num2str(genMeanLinGroups(2),'%7.2f') '\n']);
fprintf(fid,['E '  num2str(genMeanLinGroups(3),'%7.2f') '\n']);
fprintf(fid,['C '  num2str(genMeanLinGroups(4),'%7.2f') '\n']);
fprintf(fid,['D '  num2str(genMeanLinGroups(5),'%7.2f') '\n\n']);

fprintf(fid,'All ratio to wild type division timing generation means \n');
fprintf(fid,['AB4'  num2str(genMeans(1),'%7.2f')   '\n']);
fprintf(fid,['AB8 '  num2str(genMeans(2),'%7.2f')  '\n']);
fprintf(fid,['AB16 ' num2str(genMeans(3),'%7.2f')  '\n']);
fprintf(fid,['AB32 ' num2str(genMeans(4),'%7.2f')  '\n']);
fprintf(fid,['AB64 ' num2str(genMeans(5),'%7.2f')  '\n']);
fprintf(fid,['MS '   num2str(genMeans(6),'%7.2f')  '\n']);
fprintf(fid,['MS2 '  num2str(genMeans(7),'%7.2f')  '\n']);
fprintf(fid,['MS4 '  num2str(genMeans(8),'%7.2f')  '\n']);
fprintf(fid,['MS8 '  num2str(genMeans(9),'%7.2f')  '\n']);
fprintf(fid,['MS16 ' num2str(genMeans(10),'%7.2f') '\n']);
fprintf(fid,['E '    num2str(genMeans(11),'%7.2f') '\n']);
fprintf(fid,['E2 '   num2str(genMeans(12),'%7.2f') '\n']);
fprintf(fid,['E4 '   num2str(genMeans(13),'%7.2f') '\n']);
fprintf(fid,['C '    num2str(genMeans(14),'%7.2f') '\n']);
fprintf(fid,['C2 '   num2str(genMeans(15),'%7.2f') '\n']);
fprintf(fid,['C4 '   num2str(genMeans(16),'%7.2f') '\n']);
fprintf(fid,['C8 '   num2str(genMeans(17),'%7.2f') '\n']);
fprintf(fid,['D '    num2str(genMeans(18),'%7.2f') '\n']);
fprintf(fid,['D2 '   num2str(genMeans(19),'%7.2f') '\n']);
fprintf(fid,['D4 '   num2str(genMeans(20),'%7.2f') '\n\n']);

fprintf(fid,'Absolute generation means division time\n');
fprintf(fid,['AB4 '   num2str(absGenMeans(1),'%7.2f')  '\n']);
fprintf(fid,['AB8 '  num2str(absGenMeans(2),'%7.2f')  '\n']);
fprintf(fid,['AB16 ' num2str(absGenMeans(3),'%7.2f')  '\n']);
fprintf(fid,['AB32 ' num2str(absGenMeans(4),'%7.2f')  '\n']);
fprintf(fid,['AB64 ' num2str(absGenMeans(5),'%7.2f')  '\n']);
fprintf(fid,['MS '   num2str(absGenMeans(6),'%7.2f')  '\n']);
fprintf(fid,['MS2 '  num2str(absGenMeans(7),'%7.2f')  '\n']);
fprintf(fid,['MS4 '  num2str(absGenMeans(8),'%7.2f')  '\n']);
fprintf(fid,['MS8 '  num2str(absGenMeans(9),'%7.2f')  '\n']);
fprintf(fid,['MS16 ' num2str(absGenMeans(10),'%7.2f') '\n']);
fprintf(fid,['E '    num2str(absGenMeans(11),'%7.2f') '\n']);
fprintf(fid,['E2 '   num2str(absGenMeans(12),'%7.2f') '\n']);
fprintf(fid,['E4 '   num2str(absGenMeans(13),'%7.2f') '\n']);
fprintf(fid,['C '    num2str(absGenMeans(14),'%7.2f') '\n']);
fprintf(fid,['C2 '   num2str(absGenMeans(15),'%7.2f') '\n']);
fprintf(fid,['C4 '   num2str(absGenMeans(16),'%7.2f') '\n']);
fprintf(fid,['C8 '   num2str(absGenMeans(17),'%7.2f') '\n']);
fprintf(fid,['D '    num2str(absGenMeans(18),'%7.2f') '\n']);
fprintf(fid,['D2 '   num2str(absGenMeans(19),'%7.2f') '\n']);
fprintf(fid,['D4 '   num2str(absGenMeans(20),'%7.2f') '\n\n']);
fclose(fid);

% Determine phenotype matrix information about scaled cell cycle lengths
% when global scaling is appropriate
[ cyclePhenoMatrix, cyclePercentMatrix ] = measuresToPvalsAndPercentsWithPosition (cycleLength, cells, 'unscaledCycle');
if scalingInfo(1,3)==0
    lenMT=length(MT.value);
    MTscaled=MT;
    for c=1:lenMT
        MTscaled.value(1,c).t=MT.value(1,c).t/scalingInfo(1);
    end
    MT=MTscaled;
    clear MTscaled
    scaledCycleLength=cycleLength/scalingInfo(1);
    [ scaledCyclePhenoMatrix, scaledCyclePercentMatrix ] = measuresToPvalsAndPercentsWithPosition(scaledCycleLength, cells, 'scaledCycle');
    divisionTime=divisionTime/scalingInfo(1);
else
    % Global scaling is not appropriate, so no information is available
    % about scaled cell cycle lengths.
    scaledCyclePhenoMatrix=0.51*ones(1,nCells);
    scaledCycleLength=zeros(1,nCells);
    scaledCyclePercentMatrix=0.51*ones(1,nCells);
end



mIdx=1;
measureLabel = {};
tempMeasures=measures;
for m=1:nMeasures
    if (strcmp(measures{1,m},'Cell Cycle') || strcmp(measures{1,m},'cycle') || ...
            strcmp(measures{1,m},'Cell cycle') || strcmp(measures{1,m},'cellCycle') || ...
            strcmp(measures{1,m},'cell cycle'))
        % Determine cell cycle matricies
        measureLabel = [measureLabel, 'Raw cell cycle'];
        phenoMatrix(mIdx,:)=cyclePhenoMatrix;
        measuresMatrix(mIdx,:)=cycleLength;
        percentMatrix(mIdx,:)=cyclePercentMatrix;
        cellStatus(mIdx,:) = cellCycleInfo(4,:);
        fid=fopen([outputFolder '/absoluteCycleLength.txt'],'w');
        fprintf(fid,'Cell  Raw cycle length   P-value   Proportion off from wildtype  Division status\n');
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%7.2f') '  ' ...
                num2str(phenoMatrix(mIdx,c),'%9.5f') '  ' num2str(percentMatrix(mIdx,c),'%9.5f') ...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        mIdx=mIdx+1;
        measureLabel = [measureLabel, 'Scaled cell cycle'];
        phenoMatrix(mIdx,:)=scaledCyclePhenoMatrix;
        measuresMatrix(mIdx,:)=scaledCycleLength;
        percentMatrix(mIdx,:)=scaledCyclePercentMatrix;
        cellStatus(mIdx,:) = cellCycleInfo(4,:);
        fid=fopen([outputFolder '/scaledCycleLength.txt'],'w');
        fprintf(fid,'Cell  Scaled cycle length   P-value   Proportion off from wildtype  Division status\n');
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%7.2f') '  ' ...
                num2str(phenoMatrix(mIdx,c),'%9.5f') '  ' num2str(percentMatrix(mIdx,c),'%9.5f') ...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        if m~=nMeasures
            tempMeasures = [measures(1:m) 'Scaled cycle length' measures(m+1:nMeasures)];
        else
            tempMeasures = [measures 'Scaled cycle length'];
        end
        mIdx=mIdx+1;
        % For cell cycle length, the measures matrix holds the
        % number of minutes of the cell's lifetime.
        % Save a plot of cell cycle robust fitting with weights.
        figure;hold on
        h=colormap(jet(10));
        load('wtMeansUnscaledNoArrest.mat');
        weightIdx = 0;
        for c1=1:length(robustWeights)
            for c2 = 1:length(cellsStandard)
                if strcmp(cells{c1},cellsStandard{c2})
                    if cellCycleInfo(4,c1) == 0
                        weightIdx = weightIdx+1;
                        if ceil(robustWeights(weightIdx)*10)==0
                            idx=1;
                        else
                            idx=ceil(robustWeights(weightIdx)*10);
                        end
                        plot(means(c2),cycleLength(c1),'.','Color',h(idx,:),'Markersize',15)
                    end
                end
            end
        end
        y=polyval([scalingInfo(1) 0],min(means):max(means));
        plot(min(means):max(means),y,'-k','Linewidth',2)
        set(gca,'FontName','Book Antiqua')
        xlabel('Mean wild type division time','Fontsize',14)
        ylabel('This embryos division timing','Fontsize',14)
        title(sprintf('RMSD %s Blue values have a weight of zero, red a weight of 1', ...
            num2str(scalingInfo(2),'%7.3f')),'Fontsize',14)
        set(gcf,'Position',[51 51 1200 800])
        colorbar
        set(gcf,'PaperPositionMode','auto');
        print(gcf,[outputFolder '/robustEmbryonicClockFittingSummary.png'],'-dpng');
        close all
        % Save plot of generation rates
        figure;hold on
        % ms=30;
        plot(absGenMeans(1:5),genMeans(1:5),'.r','Markersize',30)
        plot(absGenMeans(6:10),genMeans(6:10),'.g','Markersize',30)
        plot(absGenMeans(11:13),genMeans(11:13),'.b','Markersize',30)
        plot(absGenMeans(14:17),genMeans(14:17),'.k','Markersize',30)
        plot(absGenMeans(18:20),genMeans(18:20),'.c','Markersize',30)
        legend('AB','MS','E','C','D','Location','BestOutside')
        set(gca,'FontName','Book Antiqua')
        title(sprintf('%s Lineage group rates - AB:  %6.3f, MS: %6.3f, E: %6.3f, C: %6.3f, D: %6.3f',...
            embName,genMeanLinGroups(1),genMeanLinGroups(2),genMeanLinGroups(3),genMeanLinGroups(4),...
            genMeanLinGroups(5)),'Fontsize',12)
        set(gcf,'Position',[51 51 1000 800])
        set(gcf,'PaperPositionMode','auto');
        print(gcf,[outputFolder '/possibleSlowingDown.png'],'-dpng');
        close all
        
    elseif (strcmp(measures{1,m},'Division Time') || strcmp(measures{1,m},'division') || ...
            strcmp(measures{1,m},'Division time') || strcmp(measures{1,m},'divisionTime') || ...
            strcmp(measures{1,m},'division time'))
        % Examine division timing for scaled cell cycle lengths if scaling
        % was appropriate or unscalaed cycle lengths if scaling was not
        % appropriate
        measuresMatrix(mIdx,:)=divisionTime;
        cellStatus(mIdx,:) = cellCycleInfo(4,:);
        if scalingInfo(3)==0
            [ phenoMatrix(mIdx,:), percentMatrix(mIdx,:) ] = measuresToPvalsAndPercentsWithPosition(divisionTime, cells, 'scaledDivision');
        else
            [ phenoMatrix(mIdx,:), percentMatrix(mIdx,:) ] = measuresToPvalsAndPercentsWithPosition (divisionTime, cells, 'unscaledDivision');
        end
        
        % Write division timing information to a text file.
        fid=fopen([outputFolder '/divisionTiming.txt'],'w');
        fprintf(fid,'Cell  Division time  P-value   Proportion off from wildtype  Division status\n');
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%7.2f') '  ' ...
                num2str(phenoMatrix(mIdx,c),'%9.5f') '  ' num2str(percentMatrix(mIdx,c),'%9.5f') ...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        mIdx=mIdx+1;
        measureLabel = [measureLabel, 'Division timing'];
        
    elseif (strcmp(measures{1,m},'Generation Asynchrony') || strcmp(measures{1,m},'generation') || ...
            strcmp(measures{1,m},'Generation asynchrony') || strcmp(measures{1,m},'generationAsynchrony') || ...
            strcmp(measures{1,m},'generation asynchrony'))
        % Examine asychrony between cells in the same generation of the
        % same sub-lineage group.
        measuresMatrix(mIdx,:) = genIndiv;
        cellStatus(mIdx,:) = cellCycleInfo(4,:);
        [ phenoMatrix(mIdx,:), percentMatrix(mIdx,:) ] = measuresToPvalsAndPercentsWithPosition (genIndiv, cells, 'genIndiv');
        
        % Write this information to a text file
        fid=fopen([outputFolder '/asychronyFromGeneration.txt'],'w');
        fprintf(fid,'Cell  Generation asychrony   P-value   Proportion off from wildtype  Division status\n');
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%8.4f') '  ' ...
                num2str(phenoMatrix(mIdx,c),'%9.5f') '  ' num2str(percentMatrix(mIdx,c),'%9.5f') ...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        mIdx=mIdx+1;
        measureLabel = [measureLabel, 'Generation asychrony'];
        
    elseif (strcmp(measures{1,m},'Expression') || strcmp(measures{1,m},'expression'))
        % Examine expression levels and compare to wild-type standard
        exprIdx = mIdx;
        % Examine expression values and make a binary call of whether they
        % are expressing or not (exprStatus)
        [ measuresMatrix(mIdx,:), exprWeights, rankTestPval, exprStatus, nonExist ] = analyzeExpressionValsTotalProcess(MT,cells,ids,ind);
        % Compare this status to the wild-type standard
        [ phenoMatrix(mIdx,:), colorVals ] = binaryExpressionToExpressionPvals(exprStatus, cells, strain, nonExist);
        cellStatus(mIdx,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6); %only significant if cell is around for > 5 minutes.
        % Write this information to a text file.
        fid=fopen([outputFolder '/expression.txt'],'w');
        fprintf(fid,'Cell  Expression value  Expression p-value Robust fitting weight  Rank sum minimum p-value Expression Status\n');
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%8.4f') '  ' ...
                num2str(phenoMatrix(mIdx,c),'%9.5f') '  ' num2str(exprWeights(c),'%9.5f') ...
                '  ' num2str(rankTestPval(c),'%9.5f') '  ' num2str(exprStatus(c),'%d') '\n']);
        end
        fclose(fid);
        % Display the expressing cells in lineage tree format.
        treeDisplayValueColorsFlexibleCells(colorVals,{'Red cells express, black cells do not'},...
            {[outputFolder '/expressionTreeVisualization.png']},cells);
        mIdx=mIdx+1;
        measureLabel = [measureLabel, 'Expression'];
        % Save a plot of expression robust fitting with weights.
        figure;hold on
        h=colormap(jet(10));
        for c = 1:nCells
            if ceil(exprWeights(c)*10)==0
                idx=1;
            else
                idx=ceil(exprWeights(c)*10);
            end
            if exprWeights(c) < 1.05
                plot(c,measuresMatrix(exprIdx,c),'.','Color',h(idx,:),'Markersize',20)
            end
        end
        set(gca,'FontName','Book Antiqua')
        ylabel('Expression level','Fontsize',14)
        title('Expression weighting - blue values have a weight of zero, orange a weight of 1', ...
            'Fontsize',14)
        lw=1;
        maxExpr = max(measuresMatrix(exprIdx,:));
        minExpr = min(measuresMatrix(exprIdx,:));
        if maxExpr == minExpr
            maxExpr = minExpr + 0.5;
        end
        labelIdx = zeros(1,genIdx);
        line([genInfo(1) genInfo(1)], [minExpr maxExpr],'Color','k','Linewidth',lw)
        for g = 2:genIdx-1
            labelIdx(g) = (genInfo(g) + genInfo(g-1)) / 2;
            if strcmp(genType{g}(1),genType{g-1}(1))
                line([genInfo(g) genInfo(g)], [minExpr maxExpr],'Color','k','Linewidth',lw)
            else
                line([genInfo(g) genInfo(g)], [minExpr maxExpr],'Color','k','Linewidth',2*lw)
            end
        end
        labelIdx(genIdx) = (genInfo(genIdx) + nCells) / 2;
        line([genInfo(genIdx) genInfo(genIdx)], [minExpr maxExpr],'Color','k','Linewidth',lw)
        set(gca,'XTick',labelIdx);
        set(gca,'XTickLabel',genLabels);
        set(gca,'FontName','Book Antiqua')
        for r=0.5 : 1 : mIdx+0.5
            line([0 nCells+0.5],[r r],'Color','k','Linewidth',lw)
        end
        if isnan(minExpr)==0 && isnan(maxExpr)==0
            axis([0 nCells minExpr maxExpr])
        end
        set(gcf,'Position',[51 51 1200 800])
        set(gcf,'PaperPositionMode','auto');
        print(gcf,[outputFolder '/robustExpressionFittingSummary.png'],'-dpng');
        close all
        
    elseif (strcmp(measures{1,m},'rawPositions') || strcmp(measures{1,m},'raw positions') || strcmp(measures{1,m},'Raw positions')...
            || strcmp(measures{1,m},'Raw Positions'))
        % Examine aligned cell positions
        [measuresMatrix(mIdx:mIdx+6,:), paths, axisSizes, MT] = ...
            analyzeCellPositionsNormalize(MT, cells, ids, ind, cellCycleInfo(4,:), timeResolution, 0);
        [ phenoMatrix(mIdx:mIdx+6,:), ~ ] = measuresToPvalsAndPercentsWithPosition(measuresMatrix(mIdx:mIdx+6,:), cells, 'rawPositions');
        
        % Examine embryo size and write to a text file.
        [ axisPvals, ~ ] = measuresToPvalsAndPercentsWithPosition(axisSizes, {'x','y','z'}, 'axis');
         fid=fopen([outputFolder '/embryoSizeInfo.txt'],'w');
        fprintf(fid,['X axis length ' num2str(axisSizes(1),'%7.2f') ...
            ' X axis p-value '  num2str(axisPvals(1),'%7.5f') ...
            ' L-R axis length ' num2str(axisSizes(2),'%7.2f') ...
            ' L-R axis p-value '  num2str(axisPvals(2),'%7.5f') ...
            ' D-V axis length ' num2str(axisSizes(3),'%7.2f') ...
            ' D-V axis p-value '  num2str(axisPvals(3),'%7.5f') ]);
        fclose(fid);
        
        % Deterime whether or not cells were present for at least 5 minutes.
        % This is the time point at which we examine their position.
        cellStatus(mIdx,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        cellStatus(mIdx+1,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        cellStatus(mIdx+2,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        % Determine whether or not cells divided.  This determines whether
        % we can examine their displacement.
        cellStatus(mIdx+3,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+4,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+5,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+6,:) = cellCycleInfo(4,:);
        % Write these values to a text file.
        fid=fopen([outputFolder '/rawPositionInfo.txt'],'w');
        fprintf(fid,['Cell A-P axis position  A-P axis position p-value  L-R axis position  L-R axis position p-value  D-V position  D-V position p-value' ...
            'A-P axis displacement  A-P axis displacement p-value  L-R axis displacement  L-R displacement p-value  D-V displacement  D-V displacement p-value' ...
            'Magnitude p-value Daughter status\n']);
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+1,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+1,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+2,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+2,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+3,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+3,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+4,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+4,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+5,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+5,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+6,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+6,c),'%9.5f')...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        measureLabel = [measureLabel, 'Raw A-P axis position',  'Raw L-R position', 'Raw D-V position',...
            'Raw A-P axis displacement', 'Raw L-R displacement', 'Raw D-V displacement', 'Raw total displacement'];
        
        % Save movies of the requested morphogenetic analyses.
        load('cellSets32_64_128');
        for n=1:nMovies
            if strcmp(movies{n}, 'AB32')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB32, [outputFolder '\AB32iceCreamAligned'], [outputFolder '\' embName '_AB32coneMovieAligned.avi'],50);
            elseif strcmp(movies{n}, 'AB64')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB64, [outputFolder '\AB64iceCreamAligned'], [outputFolder '\' embName '_AB64coneMovieAligned.avi'],50);
            elseif strcmp(movies{n}, 'AB128')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB128, [outputFolder '\AB128iceCreamAligned'], [outputFolder '\' embName '_AB128coneMovieAligned.avi'],50);
            elseif strcmp(movies{n},'AB32toAB128')
                renderMigrationPathArrowsAB32to128(paths,cells, [outputFolder '\AB32to128iceCreamAligned'], [outputFolder '\' embName '_AB32to128coneMovieAligned.avi'],150,1);
                renderArrowsAB32to128clusterColor(paths, cells, [outputFolder '\AB32to128iceCreamClustersAligned'], [outputFolder '\' embName '_AB32to128coneMovieClustersAligned.avi'],150,1);
            elseif strcmp(movies{n}, 'window')
                timeWindow = round(900/timeResolution);
                adjustedBirthAndDeath = round(cellCycleInfo(1:2,:)' / timeResolution*60);
                slidingWindowIceCreamConesNoPerl(paths, adjustedBirthAndDeath, cells, timeWindow, [outputFolder '\windowIceCreamAligned'], [outputFolder '\' embName '_slidingWindowAligned.avi'], round(min([MT.value(:).t])), min(tEnd) - timeWindow);
            elseif strcmp(movies{n}, 'ellipses')
                writePovrayRotatedEllipsoidsAndDotsNoPerl([outputFolder '\ellipsesAligned'], [outputFolder '\' embName '_covarianceEllipsesAligned.avi'], measuresMatrix(mIdx:mIdx+2,:)', cells, [0 0 0], 150, 'rgb');
            end
        end
        
        % Calculate and plot correlation matricies
        C64  = correlationMatrixJLMweighted(MT.value, CELLSAB64, 'cosCentered', 0, 0);
        C128  = correlationMatrixJLMweighted(MT.value, CELLSAB128, 'cosCentered', 0, 0);
        labels = {'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa','MSp','E','C','D'};
        figure;hold on
        subplot(1,2,1);
        plotCorrMtxAB64andAB128(C64, CELLSAB64, 'rwb', 'AB64');  %plotCorrMtxAB64andAB128(C64, labels, 'rwb', 'AB64');
        title('AB64','Fontsize',14);
        axis equal
        axis([0.5 98.5 0.5 98.5])
        subplot(1,2,2);
        plotCorrMtxAB64andAB128(C128, CELLSAB128, 'rwb', 'AB128'); %plotCorrMtxAB64andAB128(C128, labels, 'rwb', 'AB128');
        title('AB128','Fontsize',14);
        axis equal
        axis([0.5 194.5 0.5 194.5])
        set(gcf,'Position',[72 150 1693 720])
        set(gcf,'PaperPositionMode','auto');
        print(gcf,[outputFolder '/correlationMatrixAB64andAB128Raw.png'],'-dpng');
        close all;
        %endCorrelationMatrix
        mIdx=mIdx+7;
        
    elseif (strcmp(measures{1,m},'normalizedPositions') || strcmp(measures{1,m},'normalized positions') || ...
            strcmp(measures{1,m},'Normalized positions') || strcmp(measures{1,m},'Normalized Positions')) || ...
            (~isempty(movies) && ~((strcmp(measures{1,m},'rawPositions') || strcmp(measures{1,m},'raw positions') || strcmp(measures{1,m},'Raw positions')...
            || strcmp(measures{1,m},'Raw Positions'))))
        % Examine aligned and normalized cell positions
        [measuresMatrix(mIdx:mIdx+6,:), paths, axisSizes, MT] = ...
            analyzeCellPositionsNormalize(MT, cells, ids, ind, cellCycleInfo(4,:), timeResolution, 1);
        [ phenoMatrix(mIdx:mIdx+6,:), ~ ] = measuresToPvalsAndPercentsWithPosition(measuresMatrix(mIdx:mIdx+6,:), cells, 'normalizedPositions');

        % Examine embryo size and write to a text file.
        [ axisPvals, ~ ] = measuresToPvalsAndPercentsWithPosition(axisSizes, {'x','y','z'}, 'axis');
         fid=fopen([outputFolder '/embryoSizeInfo.txt'],'w');
        fprintf(fid,['A-P axis axis length ' num2str(axisSizes(1),'%7.2f') ...
            ' A-P axis axis p-value '  num2str(axisPvals(1),'%7.2f') ...
            ' L-R axis length ' num2str(axisSizes(2),'%7.2f') ...
            ' L-R axis p-value '  num2str(axisPvals(2),'%7.2f') ...
            ' D-V axis length ' num2str(axisSizes(3),'%7.2f') ...
            ' D-V axis p-value '  num2str(axisPvals(3),'%7.2f') ]);
        fclose(fid);
        
        % Deterime whether or not cells were present for at least 5 minutes.
        % This is the time point at which we examine their position.
        cellStatus(mIdx,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        cellStatus(mIdx+1,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        cellStatus(mIdx+2,:) = logical(cellCycleInfo(2,:) - cellCycleInfo(1,:) < 6);
        % Determine whether or not cells divided.  This determines whether
        % we can examine their displacement.        
        cellStatus(mIdx+3,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+4,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+5,:) = cellCycleInfo(4,:);
        cellStatus(mIdx+6,:) = cellCycleInfo(4,:);
        % Write these values to a text file.
        fid=fopen([outputFolder '/normalizedPositionInfo.txt'],'w');
        fprintf(fid,['Cell A-P position  A-P position p-value  L-R position  L-R position p-value  D-V position  D-V position p-value' ...
            'A-P displacement  A-P displacement p-value  L-R displacement  L-R displacement p-value  D-V displacement  D-V displacement p-value' ...
            'Magnitude p-value Daughter status\n']);
        for c=1:nCells
            fprintf(fid,[cells{c} '  ' num2str(measuresMatrix(mIdx,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+1,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+1,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+2,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+2,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+3,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+3,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+4,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+4,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+5,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+5,c),'%9.5f')...
                '  ' num2str(measuresMatrix(mIdx+6,c),'%7.2f') '  ' num2str(phenoMatrix(mIdx+6,c),'%9.5f')...
                '  ' num2str(cellCycleInfo(4,c),'%d') '\n']);
        end
        fclose(fid);
        measureLabel = [measureLabel, 'Norm A-P position',  'Norm L-R position', 'Norm D-V position',...
            'Norm A-P displacement', 'Norm L-R displacement', 'Norm D-V displacement', 'Norm total displacement'];
        
        % Save movies of the requested morphogenetic analyses.
        load('cellSets32_64_128');
        for n=1:nMovies
            if strcmp(movies{n}, 'AB32')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB32, [outputFolder '\AB32iceCreamNormalized'], [outputFolder '\' embName '_AB32coneMovieNormalized.avi'],50);
            elseif strcmp(movies{n}, 'AB64')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB64, [outputFolder '\AB64iceCreamNormalized'], [outputFolder '\' embName '_AB64coneMovieNormalized.avi'],50);
            elseif strcmp(movies{n}, 'AB128')
                renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB128, [outputFolder '\AB128iceCreamNormalized'], [outputFolder '\' embName '_AB128coneMovieNormalized.avi'],50);
            elseif strcmp(movies{n},'AB32toAB128')
                renderMigrationPathArrowsAB32to128(paths,cells, [outputFolder '\AB32to128iceCreamNormalized'], [outputFolder '\' embName '_AB32to128coneMovieNormalized.avi'],150,1);
                renderArrowsAB32to128clusterColor(paths, cells, [outputFolder '\AB32to128iceCreamClustersNormalized'], [outputFolder '\' embName '_AB32to128coneMovieClustersNormalized.avi'],150,1);
            elseif strcmp(movies{n}, 'window')
                timeWindow = round(900/timeResolution);
                adjustedBirthAndDeath = round(cellCycleInfo(1:2,:)' / timeResolution*60);
                slidingWindowIceCreamConesNoPerl(paths, adjustedBirthAndDeath, cells, timeWindow, [outputFolder '\windowIceCreamNormalized'], [outputFolder '\' embName '_slidingWindowNormalized.avi'], round(min([MT.value(:).t])), min(tEnd) - timeWindow);
            elseif strcmp(movies{n}, 'ellipses')
                writePovrayRotatedEllipsoidsAndDotsNoPerl([outputFolder '\ellipsesNormalized'], [outputFolder '\' embName '_covarianceEllipsesNormalized.avi'], measuresMatrix(mIdx:mIdx+2,:)', cells, [0 0 0], 150, 'rgb');
            end
        end
        
        % Calculate and plot correlation matricies.
        C64  = correlationMatrixJLMweighted(MT.value, CELLSAB64, 'cosCentered', 0, 0);
        C128  = correlationMatrixJLMweighted(MT.value, CELLSAB128, 'cosCentered', 0, 0);
        labels = {'ABala','ABalp','ABara','ABarp','ABpla','ABplp','ABpra','ABprp','MSa','MSp','E','C','D'};
        figure;hold on
        subplot(1,2,1);
        plotCorrMtxAB64andAB128(C64, CELLSAB64, 'rwb', 'AB64');  %plotCorrMtxAB64andAB128(C64, labels, 'rwb', 'AB64');
        title('AB64','Fontsize',14);
        axis equal
        axis([0.5 98.5 0.5 98.5])
        subplot(1,2,2);
        plotCorrMtxAB64andAB128(C128, CELLSAB128, 'rwb', 'AB128'); %plotCorrMtxAB64andAB128(C128, labels, 'rwb', 'AB128');
        title('AB128','Fontsize',14);
        axis equal
        axis([0.5 194.5 0.5 194.5])
        set(gcf,'Position',[72 150 1693 720])
        set(gcf,'PaperPositionMode','auto');
        print(gcf,[outputFolder '/correlationMatrixAB64andAB128Normalized.png'],'-dpng');
        close all;
        %endCorrelationMatrix
        mIdx=mIdx+7;
        
    end
end
%Check to see if a movie is made without analyzing positions and if so,
%make it here
positionFlag = 0;
for m = 1:nMeasures
    if strcmp(measures{1,m},'rawPositions') || strcmp(measures{1,m},'raw positions') ...
            || strcmp(measures{1,m},'Raw positions') || strcmp(measures{1,m},'Raw Positions') ...
            || strcmp(measures{1,m},'normalizedPositions') || strcmp(measures{1,m},'normalized positions') ...
            || strcmp(measures{1,m},'Normalized positions') || strcmp(measures{1,m},'Normalized Positions')
        positionFlag = 1;
    end
end
if positionFlag ==0 && ~isempty(movies)
    load('cellSets32_64_128');
    [startPositions, paths, ~, MT] = ...
        analyzeCellPositionsNormalize(MT, cells, ids, ind, cellCycleInfo(4,:), timeResolution, 1);
    for n=1:nMovies
        if strcmp(movies{n}, 'AB32')
            renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB32, [outputFolder '\AB32iceCreamNormalized'], [outputFolder '\' embName '_AB32coneMovieNormalized.avi'],50);
        elseif strcmp(movies{n}, 'AB64')
            renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB64, [outputFolder '\AB64iceCreamNormalized'], [outputFolder '\' embName '_AB64coneMovieNormalized.avi'],50);
        elseif strcmp(movies{n}, 'AB128')
            renderMigrationPathArrowsLoadedCorrect(MT, CELLSAB128, [outputFolder '\AB128iceCreamNormalized'], [outputFolder '\' embName '_AB128coneMovieNormalized.avi'],50);
        elseif strcmp(movies{n},'AB32toAB128')
            renderMigrationPathArrowsAB32to128(paths, cells, [outputFolder '\AB32to128iceCreamNormalized'], [outputFolder '\' embName '_AB32to128coneMovieNormalized.avi'],150,1);
            renderArrowsAB32to128clusterColor(paths, cells, [outputFolder '\AB32to128iceCreamClustersNormalized'], [outputFolder '\' embName '_AB32to128coneMovieClustersNormalized.avi'],150,1);
        elseif strcmp(movies{n}, 'window')
            timeWindow = round(900/timeResolution);
            adjustedBirthAndDeath = round(cellCycleInfo(1:2,:)' / timeResolution*60);
            slidingWindowIceCreamConesNoPerl(paths, adjustedBirthAndDeath, cells, timeWindow, [outputFolder '\windowIceCreamNormalized'], [outputFolder '\' embName '_slidingWindowNormalized.avi'], round(min([MT.value(:).t])), min(tEnd) - timeWindow);
        elseif strcmp(movies{n}, 'ellipses')
            writePovrayRotatedEllipsoidsAndDotsNoPerl([outputFolder '\ellipsesNormalized'], [outputFolder '\' embName '_covarianceEllipsesNormalized.avi'], startPositions(1:3,:)', cells, [0 0 0], 150, 'rgb');
        end
    end
end


mIdx = mIdx - 1;
measures=tempMeasures;
nMeasures=length(measures);

%Print all P-values in one text file - uncomment from here down
fid=fopen([outputFolder '/allPvalues.txt'],'w');
fprintf(fid,'cell names ');
for m=1:mIdx
    fprintf(fid,[measureLabel{m} '  ']);
end
fprintf(fid,'  Division status\n\n');
for c=1:nCells
    fprintf(fid,[cells{c} '  ' num2str(phenoMatrix(:,c)','%9.5f') '  ' ...
                ' ' num2str(cellCycleInfo(4,c),'%d') '\n']);
end
fclose(fid);

% Save a graph of number of cells every minute
tAll=unique([MT.value.t]);
nT=min(min(tEnd),length(tAll));
tAll=tAll(1:nT);
cellsPerT=zeros(1,nT);
fid=fopen([outputFolder '/cellsPerTimePoint.txt'],'w');
for t=1:nT
    cellsPerT(t)=length(find([MT.value.t]==tAll(t)));
    fprintf(fid,[num2str(tAll(t),'%7.2f') '  ' num2str(cellsPerT(t),'%9.5f') '\n']);
end
fclose(fid);
figure;
load('numberOfCellsPerTimeStats');
for t = 1:160
    yLow = [meanNcells(t) - 3*stdNcells(t) meanNcells(t+1) - 3*stdNcells(t+1)];
    yHigh = [meanNcells(t) + 3*stdNcells(t) meanNcells(t+1) + 3*stdNcells(t+1)];
    fill([t-0.1 t+0.9 t+1.1 t+0.1],[yLow(1) yLow(2) yHigh(2) yHigh(1)],[0.5 0.5 1],'EdgeColor','none')
end
plot(tAll,cellsPerT,'.-k')
xlabel('Minutes of embryogenesis','Fontsize',14)
ylabel('Number of cells in embryo','Fontsize',14)
set(gca,'FontName','Arial')
set(gcf,'Position',[51 51 1200 800])
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/numberOfCellsPerMinute.png'],'-dpng');
close all

% Save all p-value matrices and percentage off from the mean matricies as
% heat map images
saveOneEmbryoFigures(phenoMatrix, measureLabel, cells, cellStatus, embName, outputFolder, cycleMeasureFlag, 'P-values')
saveOneEmbryoFigures(percentMatrix, measureLabel, cells, cellStatus, embName, outputFolder, cycleMeasureFlag, 'Percents');
       
%adjust p-values for multiple testing
includePvals = logical(phenoMatrix ~= 0.51) & ~logical(cellStatus);
allPvals = reshape(phenoMatrix(includePvals),1,[]);
allPvals = sort(abs(allPvals));
nTests = length(allPvals);
if ~exist('adjustPval','var')
    adjustPval = 0.01;
end
k=0;
if nTests > k
    rejectFlag = 0;
    while(rejectFlag == 0)
        k = k+1;
        if allPvals(k) > k*adjustPval/nTests
            rejectFlag = 1;
            if k ~= 1
                pValCutoff = allPvals(k);
            else
                pValCutoff = 0;
            end
        end
    end
    adjustedPvals = 0.2*ones(mIdx,nCells);
    adjustedPvals(~includePvals) = 0.51;
    belowCutoff = logical(abs(phenoMatrix) < pValCutoff);
    adjustedPvals(belowCutoff) = phenoMatrix(belowCutoff);

% Output all p-values to a text file
fid=fopen([outputFolder '/allAdjustedPvalues.txt'],'w');
fprintf(fid, sprintf('Adjusted p-value cutoff %f and number of measurements %d \n',adjustPval,nTests));
fprintf(fid,'cell names ');
for m=1:mIdx
    fprintf(fid,[measureLabel{m} '  ']);
end
fprintf(fid,'  Division status\n\n');

for c=1:nCells
    fprintf(fid,[cells{c} '  ' num2str(adjustedPvals(:,c)','%9.5f') '  ' ...
                ' ' num2str(cellCycleInfo(4,c),'%d') '\n']);
end
fclose(fid);
    saveOneEmbryoFigures(adjustedPvals, measureLabel, cells, cellStatus, embName, outputFolder, cycleMeasureFlag, 'AdjustedP-values')
end
end

% This is a separate function to find the identity and number of
% generations of cells in sub-lineage groups of each type
function [genInfo, genType, genLabels, genIdx] = findGenerations(cells)

c=1;
genInfo = zeros(1,50);
genLabels = cell(1,50);
genType = cell(1,50);
genIdx = 0;
nCells = length(cells);
while c<nCells
    c=c+1;
    if length(cells{1,c-1})~=length(cells{1,c})
        genIdx = genIdx + 1;
        if strcmp(cells{1,c-1}(1),'A')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 2);
            genType{genIdx} = 'A';
            if nGen>2
                genLabels{genIdx} = ['AB' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        elseif strcmp(cells{1,c-1}(1),'M')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 2);
            genType{genIdx} = 'M';
            if nGen>2
                genLabels{genIdx} = ['MS' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        elseif strcmp(cells{1,c-1}(1),'E')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 1);
            genType{genIdx} = 'E';
            if nGen>2
                genLabels{genIdx} = ['E' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        elseif strcmp(cells{1,c-1}(1),'C')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 1);
            genType{genIdx} = 'C';
            if nGen>2
                genLabels{genIdx} = ['C' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        elseif strcmp(cells{1,c-1}(1),'D')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 1);
            genType{genIdx} = 'D';
            if nGen>2
                genLabels{genIdx} = ['D' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
            genInfo(genIdx) = c - 0.5;
            nGen = 2^(length(cells{1,c-1}) - 1);
            genType{genIdx} = 'P';
            if nGen>2
                genLabels{genIdx} = ['P' num2str(nGen)];
            else
                genLabels{genIdx} = '';
            end
        end
    end
end
genIdx = genIdx + 1;
if strcmp(cells{1,c-1}(1),'A')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 2);
    genType{genIdx} = 'A';
    if nGen>2
        genLabels{genIdx} = ['AB' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'M')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 2);
    genType{genIdx} = 'M';
    if nGen>2
        genLabels{genIdx} = ['MS' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'E')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'E';
    if nGen>2
        genLabels{genIdx} = ['E' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'C')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'C';
    if nGen>2
        genLabels{genIdx} = ['C' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'D')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'D';
    if nGen>2
        genLabels{genIdx} = ['D' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'P';
    if nGen>2
        genLabels{genIdx} = ['P' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
end
genInfo(genIdx) = nCells;
genInfo = genInfo(1:genIdx);
genType = genType(1:genIdx-1);
genLabels = genLabels(1:genIdx);
end
