function updateReferenceStatistics(newMatName,oldMatName,means,stds,cells)
% The matrices holding the statistics are all cell arrays for each
% sublineage.  Loading the ".mat" file will load variables named ABcycles,
% MScycles, Ecycles, Ccycles, Dcycles and Pcycles.  Eahc of these are
% 3 by number of cells cell arrays where the first row holds cell names,
% the second row holds that cell's mean(s) and the third row holds that
% cell's standar deviations(s).

% newMatName is the cell array that is 3 by number of cells which will hold
%  the new statistics.
% oldMatName is the cell array that is 3 by number of cells which holds the
%  current statistics
% means is a number of cells by number of measurements matrix holding the
%  new mean values
% stds is a number of cells by number of measurements matrix holding the
%  new standard deviations
% cells is a 1 by number of cells cell array holding the names of the cells
%  whose statistics are stored in the means and stds 

% Load the old natrix
load(oldMatName);

% Compare each cell in "cells" to each value in the loaded cell arrays.
% When a match is found, replace the statistics.
nCells = length(cells);
for c1=1:512
    for c2=1:nCells
        if strcmp(ABcycles{1,c1},cells{1,c2})
            ABcycles{2,c1}=means(c2,:);
            ABcycles{3,c1}=stds(c2,:);
        end
    end
end
for c1=1:127
    for c2=1:nCells
        if strcmp(MScycles{1,c1},cells{1,c2})
            MScycles{2,c1}=means(c2,:);
            MScycles{3,c1}=stds(c2,:);
        end
    end
end
for c1=1:32
    for c2=1:nCells
        if strcmp(Ecycles{1,c1},cells{1,c2})
            Ecycles{2,c1}=means(c2,:);
            Ecycles{3,c1}=stds(c2,:);
        end
    end
end
for c1=1:63
    for c2=1:nCells
        if strcmp(Ccycles{1,c1},cells{1,c2})
            Ccycles{2,c1}=means(c2,:);
            Ccycles{3,c1}=stds(c2,:);
        end
    end
end
for c1=1:31
    for c2=1:nCells
        if strcmp(Dcycles{1,c1},cells{1,c2})
            Dcycles{2,c1}=means(c2,:);
            Dcycles{3,c1}=stds(c2,:);
        end
    end
end
for c1=1:7
    for c2=1:nCells
        if strcmp(Pcycles{1,c1},cells{1,c2})
            Pcycles{2,c1}=means(c2,:);
            Pcycles{3,c1}=stds(c2,:);
        end
    end
end

% Save the new ".mat" file.  Any cells that were not in the "cells"
% variable will keep the same statistics.
save(newMatName,'ABcycles','MScycles','Ecycles','Ccycles','Dcycles','Pcycles');