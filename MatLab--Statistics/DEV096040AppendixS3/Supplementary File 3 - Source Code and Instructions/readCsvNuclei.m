function [Mt] = readCsvNuclei(csvFile, tEnd, aspectRatio)
% loads a 'nuclei' directory. Sets the embryo in a canonical orientation.
% Selects AB4 to AB64 and the corresponding C,D,E,MS,P & Z lineages.
% Mt contains cell xyz data, embryo is centered & oriented so a-p 
% axis is aligned with x axis (anterior is on the negative side of the axis), 
% d-v is aligned with y axis (dorsal is on negative side).
% cells are given a unique index #.
%
% stats are statistics on cell paths.
% 
% Usage -
%   [Mt081305, stats1] = readnucleiDir('data/081305') %edit this
%
% Input -
%dir        - the directory the nuclei file are located
%truncate   - should the initial/last few time point be tuncated. By
%             default true.


if (exist(csvFile,'file') == 0)
    error(['File ',csvFile,' does not exist!']);
end
fid = fopen(csvFile,'r');
if fid < 0
     error(['could not open file: ' csvFile]);  
end

%load info from csv file
C = textscan(fid, '%*s%s%d%f%*f%*f%f%*f%f%f%f%f%*f','delimiter',',','HeaderLines',1);%'%f%d%d%f%f%f%f%f%f%s%*d%f%*f%*d%*s%*f%*f%*f%f%*f', 'delimiter', ',');
fclose(fid);   
   
%create struct
disp('transferring data to struct');
n = length(C{1,1});
M = repmat(struct('t', 0, 'id', 0, 'successor', 0,'diameter', 0,'x', 0,'y', 0,'z', 0,'name', 0,'rfp',0), 1, n);
for i=1:n
    M(i).name = C{1,1}(i);
    if i<n && strcmp(C{1,1}{i},C{1,1}{i+1})
        M(i).successor = 1;
    else
        M(i).successor = 0;
    end
    M(i).t = C{1,2}(i);
    M(i).rfp = C{1,3}(i) - C{1,4}(i);
    M(i).z = C{1,5}(i);
    M(i).x = C{1,6}(i);
    M(i).y = C{1,7}(i);   
    M(i).diameter = C{1,8}(i);  
end    
    
[tx ty tz] = getRotationNewDataVersionPC(M,min(tEnd));%getRotationNewDataVersion2(M);
M = center(M);
M = Rotate(M,  0,  0, tz);
if tx>0 || ty>0
    M = Rotate(M, tx, ty,  0);
end

fprintf (' Extracting AB,C,D,E,MS,P,Z\n');
%truncate initial 4 cells and polar bodies
[M, resAB] = getLineage(M, 'AB', 4, 256, tEnd(1));%Extracting AB4-128
[M, resMS] = getLineage(M, 'MS', 1, 128, tEnd(2));
[M, resE] = getLineage(M, 'E', 1, 64, tEnd(3));
[M, resC] = getLineage(M, 'C', 1, 32, tEnd(4));
[M, resD] = getLineage(M, 'D', 1, 16, tEnd(5));
[M, resP] = getLineage(M, 'P', 1, 4, tEnd(6));
[M, resZ] = getLineage(M, 'Z', 1, 2, tEnd(6));
resEMS = ~cellfun(@isempty, regexpi([M.name], '\<EMS\>')) ;

Mt = M(resAB | resC | resD | resE |( resMS & ~resEMS)| resP | resZ);
Mt = traceRoute(Mt);
Mt = resizeGridNewScope(Mt, aspectRatio, aspectRatio, 1, aspectRatio);
% stats = getRouteStats(Mt);
end

function M = Rotate(M, tx, ty, tz)
    disp(sprintf('rotating around tx %f, ty %f, tz %f \n',tx,ty,tz));
    Rot = makehgtform('xrotate',tx,'yrotate',ty,'zrotate',tz);
    Rot = Rot(1:3,1:3);
    M = applyRotation(M, Rot);
end

function [M, res] = getLineage(M, cellnm, ncellstFrom, ncellstTo, tEnd)

cellnm = lower(cellnm);
sregexp = sprintf('\\<%s\\w{%d,%d}\\>',cellnm, log2(ncellstFrom), log2(ncellstTo));


sexpected = 2*ncellstTo-ncellstFrom;
n = sum(~cellfun(@isempty, regexpi(unique([M.name]),sregexp ))) ;
switch cellnm %Since EMS will be picked we will omit it on count
    case 'e'
        n = n-1;
end

resName = ~cellfun(@isempty, regexpi([M.name], sregexp));
resTime=[M.t]<=tEnd; %Find all the cells less than or equal to the ending time
resLastTime=[M.t]==tEnd; %Find all the cells equal to the ending time
resSetToZero= find(resLastTime & resName);%Find all cells of this lineage at the last time
lenLast=length(resSetToZero); 
for c=1:lenLast
    %set the successor of the cells present at the last time point to zero
    M(resSetToZero(c)).successor=0;
end
res=resName & resTime;
fprintf('%s cells found: %d out of %d\n',upper(cellnm),n, sexpected);
%if (n - sexpected ~= 0)
%    warning([upper(cellnm) ' cells found and expected differ']);
%end


end
