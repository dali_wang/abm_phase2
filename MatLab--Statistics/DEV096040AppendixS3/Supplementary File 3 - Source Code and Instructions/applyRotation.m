function M = applyRotation(M, Rot, center)
% Apply a 3D transformation on an embryo

    xNotNan = ~isnan([M.x]);
    yNotNan = ~isnan([M.y]);
    zNotNan = ~isnan([M.z]);
    notNan = xNotNan & yNotNan & zNotNan;
    m = [[M.x]; [M.y]; [M.z]]';
    if (nargin == 2)
        center = mean(m(notNan,:));
    end
    m = [m(:,1)'-center(1); m(:,2)'-center(2); m(:,3)'-center(3)]';
    m = m*Rot;
    m = [m(:,1)'+center(1);m(:,2)'+center(2);m(:,3)'+center(3)]';
     
    M = setVecField(M, 'x', m(:,1));
    M = setVecField(M, 'y', m(:,2));
    M = setVecField(M, 'z', m(:,3));
end
