function specifySphereSweepNoPerl(folderName,fileName,first,last,color,center,cameraDist,largeSizeFlag)


% Open the POV-Ray text file and specify the scene parameters.
cameraVec=[0 cameraDist 0 3];
disp('Writing PovRay file')
if ~exist(folderName,'dir')
    mkdir(folderName);
else
    delete([folderName '/*.pov'])
    delete([folderName '/*.bmp'])
end

currentFile=fopen([folderName '/rotationFile.pov'],'w');
fprintf(currentFile, '#include "colors.inc"\n#include "textures.inc"\n');
fprintf(currentFile, '#include "shapes.inc"\n#include "glass.inc"\n\n');
fprintf(currentFile, 'background { color rgb<1, 1, 1> }\n\n');

fprintf(currentFile, ['light_source {<' ...
    num2str(center(1, 1) + cameraVec(1), '%.5f') ', ' ...
    num2str(center(1, 2) + cameraVec(2), '%.5f') ', ' ...
    num2str(center(1, 3) + cameraVec(3) , '%.5f') '>' ...
    ' color White \n parallel \n point_at <'...
    num2str(center(1, 1), '%.5f') ', ' ...
    num2str(center(1, 2), '%.5f') ', ' ...
    num2str(center(1, 3), '%.5f') '> } \n\n' ...
    'fog{ distance 60  color rgb <1.0, 1.0, 1.0, 0.2, 0.2> \n'...
    'fog_type 2 \n fog_offset 0 \n fog_alt 1}\n']);

fprintf(currentFile, ['camera { orthographic location <' ...
    num2str(center(1, 1) + cameraVec(1), '%.5f') ', ' ...
    num2str(center(1, 2) + cameraVec(2), '%.5f') ', ' ...
    num2str(center(1, 3) + cameraVec(3), '%.5f') '>'...
    '\n  sky <0, 1, 0> \n'... rotate < -30, 0, 0> \n'......
    'look_at <' ... direction <0.25, 0, 0.25> \n
    num2str(center(1, 1), '%.5f') ', ' ...
    num2str(center(1, 2), '%.5f') ', ' ...
    num2str(center(1, 3), '%.5f') ...
    '> }\n\n']);
fprintf(currentFile, 'global_settings { max_trace_level 250 } \n');

% Specify the color vectors.
depthVector = [ 1.00  0.75  0.66 0.50  0.33 0.25  0.10  0.05    ];
depth=0;
rgbTemplate = ...
    [depthVector(1) 0.21 depthVector(4) 0.25; ...%1 pink
    depthVector(1) depth depth 0.25; ...%2 red
    depthVector(2) depthVector(4) depth 0.25; ...%3 orange
    depthVector(2) depthVector(2) depth 0.25; ...%4 yellow
    depth depthVector(2) depth 0.25; ...%5 green
    depth depthVector(4) depthVector(2) 0.25; ...%6 cyan
    depthVector(5) depth depthVector(3) 0.25;...%7 purple
    depthVector(8) depthVector(8) depthVector(2) 0.25;...%8 blue
    0.5 0.5 0.5 0.25; ...%9 gray
    depth depthVector(5) depth 0.25; ...%10 dark green
    0.10 0.10 0.10 0.25; ...%11 black
    depthVector(4) depth depth 0.25; ...%12 maroon
    depth depth depthVector(6) 0.25; ...% 13 navy blue
    depthVector(4) 0.2 depthVector(7) 0.25;...%1.00 1.00 1.00 0.75];%14 transparent
    0 0 0 0.25]; %15 black

% Determine the angle that each rod should be rotated by.
nCells=size(first,1);
for c=1:nCells
    rotate=zeros(1,3);
    difs=last(c,:)-first(c,:);
    rotate(1)=0;%pi/2-acos(difs(2)/sqrt(difs(1)*difs(1)+difs(2)*difs(2)+difs(3)*difs(3)));%atan(difs(2)/difs(3));%atan2(difs(2),difs(3));
    rotate(2)=atan(difs(3)/difs(1));%asind(difs(1)/sqrt(difs(1)*difs(1)+difs(3)*difs(3)));%%atan2(difs(1)*cos(rotate(1)),difs(3));
    rotate(3)=0;%asind(difs(2)/sqrt(difs(2)*difs(2)+difs(1)*difs(1)));%0;%atan(difs(1)/difs(2));%(cos(rotate(1)),sin(rotate(1))*sin(rotate(2)));
    
    if difs(1)<0
        if difs(3)>0
            rotate(2)=pi+rotate(2);
        else
            rotate(2)=-pi+rotate(2);
        end
    end
    rotate(2)=-rotate(2);
    
    % Determine the size of each rod.
    scale=sqrt((first(c,1)-last(c,1))*(first(c,1)-last(c,1))+...
        (first(c,2)-last(c,2))*(first(c,2)-last(c,2))+...
        (first(c,3)-last(c,3))*(first(c,3)-last(c,3)));
    
    % Write the line of text describing this cell's rod.
    if color(c)>0
        currentColor=rgbTemplate(color(c),:);
        fprintf(currentFile, ['sphere_sweep{ linear_spline \n' ...
            '2, <' num2str(first(c,1),'%.5f') ', ' num2str(first(c,2),'%.5f') ', ' num2str(first(c,3),'%.5f') '>,' ...
            num2str(scale/20,'%.5f') '\n' ...
            '< ' num2str(last(c,1),'%.5f') ', ' num2str(last(c,2),'%.5f') ', ' num2str(last(c,3),'%.5f') '>,' ...
            num2str(scale/7,'%.5f') '\n' ...
            'rotate< 360*clock, 0, 0> \n'...
            'texture { pigment { rgbt<' ...
            num2str(currentColor(1, 1), '%.5f') ', ' ...
            num2str(currentColor(1, 2), '%.5f') ', ' ...
            num2str(currentColor(1, 3), '%.5f') ', ' ...
            num2str(currentColor(1, 4), '%.5f') '> }' ...
            ' finish {ambient 1.0 diffuse 0.2 specular 0.2} } }\n']);
    end
end
fclose(currentFile);

% Specify the size of the image.
if exist('largeSizeFlag','var')
    if largeSizeFlag == 1
        width = '800';
        height = '600';
    else
        width = '512';
        height = '384';
    end
else
    width = '512';
    height = '384';
end

% Render animation in POV-Ray.
currentDir = pwd;
system([currentDir '\POV-Ray\bin\pvengine ' ... %call POV-Ray
    '+I' folderName '\rotationFile.pov ' ... %input file
    '+O' folderName '\rotatingView.bmp ' ... %outputFile
    '+KFI0 +KFF72 +W' width ' +H' height ' -GA +L' currentDir '\POV-Ray\bin\include']);

% Save movie.
system([currentDir '\EasyBMPtoAVI\EasyBMPtoAVI '...
    '-start ' folderName ' \rotatingView01.bmp ' ...
    '-end ' folderName '\rotatingView72.bmp ' ...
    '-framerate 7 -output ' fileName]);

% Delete individual frames.
delete([folderName '/*.bmp'])



