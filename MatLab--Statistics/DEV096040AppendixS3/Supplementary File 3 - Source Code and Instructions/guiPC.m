function [varargout] = guiPC(varargin)
% GUIPC MATLAB code for guiPC.fig
%      GUIPC, by itself, creates a new GUIPC or raises the existing
%      singleton*.
%
%      H = GUIPC returns the handle to a new GUIPC or the handle to
%      the existing singleton*.
%
%      GUIPC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUIPC.M with the given input arguments.
%
%      GUIPC('Property','Value',...) creates a new GUIPC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before guiPC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to guiPC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiPC

% Last Modified by GUIDE v2.5 18-Feb-2013 15:38:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiPC_OpeningFcn, ...
                   'gui_OutputFcn',  @guiPC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before guiPC is made visible.
function guiPC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiPC (see VARARGIN)
logo_CreateFcn(hObject, eventdata, handles)
% Choose default command line output for guiPC
handles.output = hObject;
handles.info = cell(1,9);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes guiPC wait for user response (see UIRESUME)
 uiwait(handles.figure1); %blocks execution until figure is deleted


% --- Outputs from this function are returned to the command line.
function [varargout] = guiPC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%myData = guidata(hObject);
% Get default command line output from handles structure
varargout{1} = [];% myData.info; %handles.info;%  

function zipFileEntry_Callback(hObject, eventdata, handles)
% hObject    handle to zipFileEntry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%guidata(hObject,handles);
%        str2double(get(hObject,'String')) returns contents of zipFileEntry as a double
zipFileName =  get(hObject,'String');% returns contents of edit1 as text
if exist(zipFileName)
    assignin('base','zipFileName',zipFileName);
else
    disp(sprintf('Zip file name entered %s does not exist',zipFileName));
end



% --- Executes during object creation, after setting all properties.
function zipFileEntry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zipFileEntry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function outputFolderEntry_Callback(hObject, eventdata, handles)
% hObject    handle to outputFolderEntry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%        str2double(get(hObject,'String')) returns contents of outputFolderEntry as a double
outputFolder =  get(hObject,'String');% returns contents of edit1 as text
assignin('base','outputFolder',outputFolder);



% --- Executes during object creation, after setting all properties.
function outputFolderEntry_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputFolderEntry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cellCycleButton.
function cellCycleButton_Callback(hObject, eventdata, handles)
% hObject    handle to cellCycleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

cellCycleFlag = get(hObject,'Value');% returns toggle state of togglebutton3
assignin('base','cellCycleFlag',cellCycleFlag);



% --- Executes on button press in ab32to128button.
function ab32to128button_Callback(hObject, eventdata, handles)
% hObject    handle to ab32to128button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ab32to128button
ab32to128Flag = get(hObject,'Value');
assignin('base','ab32to128Flag',ab32to128Flag);


function ABEnd_Callback(hObject, eventdata, handles)
% hObject    handle to ABEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ABEnd as text
%        str2double(get(hObject,'String')) returns contents of ABEnd as a double
ABendTime = str2double(get(hObject,'String'));
assignin('base','ABendTime',ABendTime);


% --- Executes during object creation, after setting all properties.
function ABEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ABEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ABpEnd_Callback(hObject, eventdata, handles)
% hObject    handle to ABpEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ABpEnd as text
%        str2double(get(hObject,'String')) returns contents of ABpEnd as a double
ABpendTime = str2double(get(hObject,'String'));
assignin('base','ABpendTime',ABpendTime);


% --- Executes during object creation, after setting all properties.
function ABpEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ABpEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function MSend_Callback(hObject, eventdata, handles)
% hObject    handle to MSend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MSend as text
%        str2double(get(hObject,'String')) returns contents of MSend as a double
MSendTime = str2double(get(hObject,'String'));
assignin('base','MSendTime',MSendTime);


% --- Executes during object creation, after setting all properties.
function MSend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MSend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Eend_Callback(hObject, eventdata, handles)
% hObject    handle to Eend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Eend as text
%        str2double(get(hObject,'String')) returns contents of Eend as a double
EendTime = str2double(get(hObject,'String'));
assignin('base','EendTime',EendTime);



% --- Executes during object creation, after setting all properties.
function Eend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Eend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Cend_Callback(hObject, eventdata, handles)
% hObject    handle to Cend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Cend as text
%        str2double(get(hObject,'String')) returns contents of Cend as a double
CendTime = str2double(get(hObject,'String'));
assignin('base','CendTime',CendTime);


% --- Executes during object creation, after setting all properties.
function Cend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DPend_Callback(hObject, eventdata, handles)
% hObject    handle to DPend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DPend as text
%        str2double(get(hObject,'String')) returns contents of DPend as a double
DPendTime = str2double(get(hObject,'String'));
assignin('base','DPendTime',DPendTime);


% --- Executes during object creation, after setting all properties.
function DPend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DPend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function secondsPerImage_Callback(hObject, eventdata, handles)
% hObject    handle to secondsPerImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of secondsPerImage as text
%        str2double(get(hObject,'String')) returns contents of secondsPerImage as a double
timeResolution = str2double(get(hObject,'String'));
assignin('base','timeResolution',timeResolution);

% --- Executes during object creation, after setting all properties.
function secondsPerImage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to secondsPerImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function aspectRatio_Callback(hObject, eventdata, handles)
% hObject    handle to aspectRatio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of aspectRatio as text
%        str2double(get(hObject,'String')) returns contents of aspectRatio as a double
aspectRatio = str2double(get(hObject,'String'));
assignin('base','aspectRatio',aspectRatio);

% --- Executes during object creation, after setting all properties.
function aspectRatio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to aspectRatio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function strain_Callback(hObject, eventdata, handles)
% hObject    handle to strain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of strain as text
%        str2double(get(hObject,'String')) returns contents of strain as a double
strain = get(hObject,'String');
assignin('base','strain',strain);

% --- Executes during object creation, after setting all properties.
function strain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to strain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pVal_Callback(hObject, eventdata, handles)
% hObject    handle to pVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pVal as text
%        str2double(get(hObject,'String')) returns contents of pVal as a double
adjustPval = str2double(get(hObject,'String'));
assignin('base','adjustPval',adjustPval);

% --- Executes during object creation, after setting all properties.
function pVal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in generationAsychronyButton.
function generationAsychronyButton_Callback(hObject, eventdata, handles)
% hObject    handle to generationAsychronyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of generationAsychronyButton
generationAsychronyFlag = get(hObject,'Value');
assignin('base','generationAsychronyFlag',generationAsychronyFlag);



% --- Executes on button press in divisionTimeButton.
function divisionTimeButton_Callback(hObject, eventdata, handles)
% hObject    handle to divisionTimeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of divisionTimeButton
divisionTimeFlag = get(hObject,'Value');
assignin('base','divisionTimeFlag',divisionTimeFlag);

% --- Executes on button press in markerExpressionButton.
function markerExpressionButton_Callback(hObject, eventdata, handles)
% hObject    handle to markerExpressionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of markerExpressionButton
expressionFlag = get(hObject,'Value');
assignin('base','expressionFlag',expressionFlag);

% --- Executes on button press in alignedNormalizedButton.
function alignedNormalizedButton_Callback(hObject, eventdata, handles)
% hObject    handle to alignedNormalizedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of alignedNormalizedButton
alignedNormalizedFlag = get(hObject,'Value');
assignin('base','alignedNormalizedFlag',alignedNormalizedFlag);

% --- Executes on button press in alignedPositionButton.
function alignedPositionButton_Callback(hObject, eventdata, handles)
% hObject    handle to alignedPositionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of alignedPositionButton
alignedPositionFlag = get(hObject,'Value');
assignin('base','alignedPositionFlag',alignedPositionFlag);

% --- Executes on button press in slidingWindowButton.
function slidingWindowButton_Callback(hObject, eventdata, handles)
% hObject    handle to slidingWindowButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
slidingWindowFlag = get(hObject,'Value');
assignin('base','slidingWindowFlag',slidingWindowFlag);
% Hint: get(hObject,'Value') returns toggle state of slidingWindowButton


% --- Executes on selection change in cellNumberPopUp.
function cellNumberPopUp_Callback(hObject, eventdata, handles)
% hObject    handle to cellNumberPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cellNumberPopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cellNumberPopUp
string = cellstr(get(hObject,'String'));
nums = zeros(1,3);
for i = 1:3
    nums(i) = str2num(string{i});
end
value = get(hObject,'Value');
switch nums(value)
    case 100
        assignin('base','endCells',100);
    case 200
        assignin('base','endCells',200);
    case 350
        assignin('base','endCells',350);
end


% --- Executes during object creation, after setting all properties.
function cellNumberPopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellNumberPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in covarianceEllipseButton.
function covarianceEllipseButton_Callback(hObject, eventdata, handles)
% hObject    handle to covarianceEllipseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
covarEllipseFlag = get(hObject,'Value');
assignin('base','covarEllipseFlag',covarEllipseFlag);
% Hint: get(hObject,'Value') returns toggle state of covarianceEllipseButton



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% % --- Executes on button press in togglebutton4.
% function asychronyFlag = togglebutton4_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton4 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% asychronyFlag = get(hObject,'Value');% returns toggle state of togglebutton4
% 
% 
% % --- Executes on button press in togglebutton5.
% function divisionTimeFlag = togglebutton5_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton5 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% divisionTimeFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton5
% 
% 
% % --- Executes on button press in togglebutton6.
% function expressionFlag = togglebutton6_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton6 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% expressionFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton6
% 
% 
% % --- Executes on button press in togglebutton7.
% function rawPositionFlag = togglebutton7_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton7 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% rawPositionFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton7
% 
% 
% % --- Executes on button press in togglebutton8.
% function normPositionFlag = togglebutton8_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton8 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% normPositionFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton8
% 
% 
% % --- Executes on button press in togglebutton9.
% function ab32movieFlag = togglebutton9_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton9 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% ab32movieFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton9
% 
% 
% % --- Executes on button press in togglebutton10.
% function ab64movieFlag = togglebutton10_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton10 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% ab64movieFlag = get(hObject,'Value');
% % Hint: get(hObject,'Value') returns toggle state of togglebutton10
% 
% 
% % --- Executes on button press in togglebutton11.
% function ab128movieFlag = togglebutton11_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton11 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% ab128movieFlag = get(hObject,'Value');
% 
% % Hint: get(hObject,'Value') returns toggle state of togglebutton11
% 
% 
% % --- Executes on button press in togglebutton12.
% function ab32to128movieFlag = togglebutton12_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton12 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% ab32to128movieFlag = get(hObject,'Value');
% 
% % Hint: get(hObject,'Value') returns toggle state of togglebutton12
% 
% 
% % --- Executes on button press in togglebutton13.
% function windowMovieFlag = togglebutton13_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton13 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% windowMovieFlag = get(hObject,'Value');
% 
% % Hint: get(hObject,'Value') returns toggle state of togglebutton13


% --- Executes during object creation, after setting all properties.
function logo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to logo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
logo = imread('logo.png','png');
imagesc(logo);
axis equal off;

% Hint: place code in OpeningFcn to populate logo


% --- Executes on button press in DoAll.
function DoAll_Callback(hObject, eventdata, handles)
% hObject    handle to DoAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
doAllFlag = get(hObject,'Value');
if doAllFlag
    assignin('base','cellCycleFlag',1);
    assignin('base','ab32to128Flag',1);
    assignin('base','generationAsychronyFlag',1);
    assignin('base','divisionTimeFlag',1);
    assignin('base','expressionFlag',1);
    assignin('base','alignedNormalizedFlag',1);
    assignin('base','alignedPositionFlag',1);
    assignin('base','slidingWindowFlag',1);
    assignin('base','covarEllipseFlag',1);
end
% Hint: get(hObject,'Value') returns toggle state of DoAll


% --- Executes on button press in RunButton.
function RunButton(hObject, eventdata, handles)
% hObject    handle to RunButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume;


% --- Executes on button press in RunButton.
function RunButton_Callback(hObject, eventdata, handles)
% hObject    handle to RunButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume;


% --- Executes on button press in inputBrowse.
function inputBrowse_Callback(hObject, eventdata, handles)
% hObject    handle to inputBrowse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[zipFile,path,~] = uigetfile('*.zip; *.csv');
zipFileName = [path zipFile];
set(handles.zipFileEntry,'String',zipFileName);
assignin('base','zipFileName',zipFileName);


% --- Executes on button press in outputBrowse.
function outputBrowse_Callback(hObject, eventdata, handles)
% hObject    handle to outputBrowse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputFolder = uigetdir;
set(handles.outputFolderEntry,'String',outputFolder);
assignin('base','outputFolder',outputFolder);


% --- Executes on selection change in strainNamePopUp.
function strainNamePopUp_Callback(hObject, eventdata, handles)
% hObject    handle to strainNamePopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns strainNamePopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from strainNamePopUp
value = get(hObject,'Value');
switch value
    case 1 %BV82
        assignin('base','strain','BV82');
    case 2 %RW10348
        assignin('base','strain','RW10348');
    case 3 %RW10425
        assignin('base','strain','RW10425');
    case 4 %RW10434
        assignin('base','strain','RW10434');
    case 5 %RW10714
        assignin('base','strain','RW10714');
    case 6 %None/Other
        assignin('base','strain','');
end


% --- Executes during object creation, after setting all properties.
function strainNamePopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to strainNamePopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
