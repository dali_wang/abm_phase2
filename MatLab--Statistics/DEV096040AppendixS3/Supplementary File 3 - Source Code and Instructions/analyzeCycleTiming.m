function [cellCycleInfo,cycleLength,divisionTime, MT]=analyzeCycleTiming(MT,cells,ids,ind)
% Takes in loaded embryo information and names of cells of interest

% Outputs
% cellCycleInfo, a 4 x number of cells vector containing:
   % birth time of cell, division time of cell, a flag for not observing
   % the cell's birth, and a flag for not observing the cell's division.
% cycleLength holds the number of minutes each cell is alive for.
% divisionTime contains the birth minute of the cell's daughters.
if nargin==1
    load('allCells');
end

nCells=length(cells);
cellCycleInfo=zeros(4,nCells);

%Find the first and last time points observed.
firstTime=min([MT.value.t]);
lastTime=max([MT.value.t]);

for c=1:nCells
    if ids(c)~=-1
        %Find the first time point the cell is alive for.
        cellCycleInfo(1,c)=MT.value(1,ind(ids(c))).t;
        %Set this flag to 1 if there is a possibility that we did not
        %observe the birth time of this cell.
        if cellCycleInfo(1,c)==firstTime
            cellCycleInfo(3,c)=1;
        end
        numTimePoints=length(find([MT.value.routid]==ids(c)))-1;
        %Division time point is the last time the cell is alive plus one
        %extra time point because otherwise neither the mother nor the
        %daughter would be considered alive for the minute that the
        %division happens.
        cellCycleInfo(2,c)=MT.value(1,ind(ids(c))+numTimePoints).t+ ...
            MT.value(1,ind(ids(c))+numTimePoints).t - MT.value(1,ind(ids(c))+numTimePoints-1).t ;
        %Set this flag to 1 if it is possible that we did not observe the
        %division time of this cell.
        if cellCycleInfo(2,c)==lastTime
            cellCycleInfo(4,c)=1;
        elseif strcmp(cells{1,c}(1),'P')
            if strcmp(cells{1,c},'P2')
                nextTimeMT=MT.value(1,logical([MT.value.t]'==cellCycleInfo(2,c)));
                nNextCells=length(nextTimeMT);
                daughter=0;curCell=1;
                while(daughter==0 && curCell<=nNextCells)
                    if (strcmp('P3',nextTimeMT(1,curCell).name) ||...
                           strcmp('C',nextTimeMT(1,curCell).name))
                        daughter=1;
                    end
                    curCell=curCell+1;
                end
                if daughter~=1
                    cellCycleInfo(4,c)=1;
                end
            elseif strcmp(cells{1,c},'P3')
                nextTimeMT=MT.value(1,logical([MT.value.t]'==cellCycleInfo(2,c)));
                nNextCells=length(nextTimeMT);
                daughter=0;curCell=1;
                while(daughter==0 && curCell<=nNextCells)
                    if (strcmp('P4',nextTimeMT(1,curCell).name) ||...
                           strcmp('D',nextTimeMT(1,curCell).name))
                        daughter=1;
                    end
                    curCell=curCell+1;
                end
                if daughter~=1
                    cellCycleInfo(4,c)=1;
                end
            elseif strcmp(cells{1,c},'P4')
                nextTimeMT=MT.value(1,logical([MT.value.t]'==cellCycleInfo(2,c)));
                nNextCells=length(nextTimeMT);
                daughter=0;curCell=1;
                while(daughter==0 && curCell<=nNextCells)
                    if (strcmp('Z3',nextTimeMT(1,curCell).name) ||...
                           strcmp('Z2',nextTimeMT(1,curCell).name))
                        daughter=1;
                    end
                    curCell=curCell+1;
                end
                if daughter~=1
                    cellCycleInfo(4,c)=1;
                end
            end
        else
            nextTimeMT=MT.value(1,logical([MT.value.t]'==cellCycleInfo(2,c)));
            nNextCells=length(nextTimeMT);
            daughter=0;curCell=1;
            while(daughter==0 && curCell<=nNextCells)
                if (strcmp(strcat(cells{1,c},'a'),nextTimeMT(1,curCell).name) ||...
                        strcmp(strcat(cells{1,c},'l'),nextTimeMT(1,curCell).name) || ...
                        strcmp(strcat(cells{1,c},'p'),nextTimeMT(1,curCell).name) || ...
                        strcmp(strcat(cells{1,c},'r'),nextTimeMT(1,curCell).name) ||...
                        strcmp(strcat(cells{1,c},'d'),nextTimeMT(1,curCell).name) ||...
                        strcmp(strcat(cells{1,c},'v'),nextTimeMT(1,curCell).name))
                    daughter=1;
                end
                curCell=curCell+1;
            end
            if daughter~=1
                cellCycleInfo(4,c)=1;
            end
        end
    else
        cellCycleInfo(4,c)=1;
    end
end
%These are the measures we will use later.
cycleLength=cellCycleInfo(2,:)-cellCycleInfo(1,:);
if strcmp(cells{1,1},'ABal') && cellCycleInfo(3,1)==0
    divisionTime=cellCycleInfo(2,:)-cellCycleInfo(1,1);%assumes ABal is #1
    adjustTime = [MT.value.t] - cellCycleInfo(1,1);
    len = length(MT.value);
    for c = 1:len
        MT.value(1,c).t = adjustTime(c);
    end
else
    divisionTime=cellCycleInfo(2,:);
end