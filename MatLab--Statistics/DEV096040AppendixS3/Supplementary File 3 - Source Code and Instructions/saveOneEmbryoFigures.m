function saveOneEmbryoFigures(phenoMatrix, measures, cells, cellCycleInfo, embName, outputFolder, cycleFlag, type)
% zip file name specifies the location of the zip file containing the nuclei
%   files to be analyzed, its name will be used to specify the output
%   directory within the folder output folder
% cells is a cell array of cells to be analyzed, {'all'} specifies a default set
% measures defines the measurements to be used
% movies is the set of movies to be saved
% tEnd specifies the lineage separated ending times of lineging
%   (AB, MS, E, C, D, P and Z)
% time resolution specifies the number of seconds between successive images


nMeasures=length(measures);
%Assign values depending on type of figures
if exist('type','var')
    if strcmp(type,'p-values') || strcmp(type,'P-values')
        colorRange = [-0.010 0.0050];%[-0.005 0.005];
        reScaleVal = 0.0044;
        lowerBound = -0.005;
        sigCutoff = 0.0025;
        cMap = frenchPvalCycle(50);
        scaleWholeMtx = 1;
    elseif strcmp(type,'percents') || strcmp(type,'Percents')
        colorRange = [-100 60];
        reScaleVal = 0.49;
        lowerBound = -0.5;
        sigCutoff = 0.2;
        cMap = [0 1 0;...
            0.25 1 0.25;...
            0.5 1 0.5;...
            1 1 1;...
            1 1 1;...
            1 0 0;...
            1 0.25 0.25;...
            1 0.5 0.5;...
            1 1 1;...
            1 1 1;...
            1 1 1;...
            1 1 1;...
            0.5 0.5 1;...
            0.25 0.25 1;...
            0 0 1;...
            0 0 0;];
        scaleWholeMtx = 100;
    elseif strcmp(type, 'adjustedP-values') || strcmp(type, 'AdjustedP-values')
        colorRange = [-0.010 0.0050];%[-0.005 0.005];
        reScaleVal = 0.0044;
        lowerBound = -0.005;
        sigCutoff = 0.0025;
        cMap = frenchPvalCycle(50);
        scaleWholeMtx = 1;
%         colorRange = [0 0.5];
%         reScaleVal = 0.4;
%         sigCutoff = 0.002;
%         lowerBound = 0;
%         cMap = [1 0 0;...
%             1 1 1;...
%             1 1 1;...
%             1 1 1;...
%             0 0 0;];
%         scaleWholeMtx = 1;
    end
else
    type = 'P-values';
    colorRange = [-0.010 0.0050];%[-0.005 0.005];
    reScaleVal = 0.0044;
    lowerBound = -0.005;
    sigCutoff = 0.0025;
    cMap = frenchPvalCycle(50);
    scaleWholeMtx = 1;
end
% Total phenotype matrix
postProcessSingleEmbPhenoMtxSpecificType(phenoMatrix, cellCycleInfo, cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('%s phenotype matrix for %s', type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/phenotypeMatrix' type '.png'],'-dpng');
close all

% Fate phenotype matricies
[allHypMtx, hypIdx]=rearrangeByFate(phenoMatrix,cells,'allHyp');
postProcessSingleEmbPhenoMtxSpecificType(allHypMtx, cellCycleInfo(:,hypIdx(logical(hypIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('All descendents hypodermis %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/allHypPhenotypeMatrix' type  '.png'],'-dpng');
close all

[someHypMtx, hypIdx]=rearrangeByFate(phenoMatrix,cells,'someHyp');
postProcessSingleEmbPhenoMtxSpecificType(someHypMtx, cellCycleInfo(:,hypIdx(logical(hypIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('Some descendts hypodermis %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/someHypPhenotypeMatrix' type '.png'],'-dpng');
close all

[allNeurMtx, NeurIdx]=rearrangeByFate(phenoMatrix,cells,'allNeur');
postProcessSingleEmbPhenoMtxSpecificType(allNeurMtx, cellCycleInfo(:,NeurIdx(logical(NeurIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('All descendents neurons %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/allNeurPhenotypeMatrix' type '.png'],'-dpng');
close all

[someNeurMtx, NeurIdx]=rearrangeByFate(phenoMatrix,cells,'someNeur');
postProcessSingleEmbPhenoMtxSpecificType(someNeurMtx, cellCycleInfo(:,NeurIdx(logical(NeurIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('Some descendents neurons %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/someNeurPhenotypeMatrix' type '.png'],'-dpng');
close all

[allPharMtx, PharIdx]=rearrangeByFate(phenoMatrix,cells,'allPhar');
postProcessSingleEmbPhenoMtxSpecificType(allPharMtx, cellCycleInfo(:,PharIdx(logical(PharIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('All descendents pharynx %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/allPharPhenotypeMatrix' type '.png'],'-dpng');
close all

[somePharMtx, PharIdx]=rearrangeByFate(phenoMatrix,cells,'somePhar');
postProcessSingleEmbPhenoMtxSpecificType(somePharMtx, cellCycleInfo(:,PharIdx(logical(PharIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('Some descendents pharynx %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/somePharPhenotypeMatrix' type '.png'],'-dpng');
close all

[allMuscMtx, MuscIdx]=rearrangeByFate(phenoMatrix,cells,'allMusc');
postProcessSingleEmbPhenoMtxSpecificType(allMuscMtx, cellCycleInfo(:,MuscIdx(logical(MuscIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('All descendents muscle %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/allMuscPhenotypeMatrix' type '.png'],'-dpng');
close all

[someMuscMtx, MuscIdx]=rearrangeByFate(phenoMatrix,cells,'someMusc');
postProcessSingleEmbPhenoMtxSpecificType(someMuscMtx, cellCycleInfo(:,MuscIdx(logical(MuscIdx))), cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff);
set(gca,'YTick',1:nMeasures)
set(gca,'YTickLabel',measures)
set(gca,'FontName','Arial')
set(gca,'Fontsize',14)
set(gcf,'Position',[51 51 2000 833])
title(sprintf('Some descendents muscle %s phenotype matrix for %s',type, embName),'Fontsize',14)
set(gcf,'PaperPositionMode','auto');
print(gcf,[outputFolder '/someMuscPhenotypeMatrix' type '.png'],'-dpng');
close all




