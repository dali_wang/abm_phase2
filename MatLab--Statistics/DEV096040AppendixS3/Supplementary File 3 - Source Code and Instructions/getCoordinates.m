function [x y z t] = getCoordinates(M, ids)
%get time and xyz coordinates.
x = [M(ids).x];
y = [M(ids).y];
z = [M(ids).z];
t = [M(ids).t];

end