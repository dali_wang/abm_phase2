function M = traceRoute(M)
%Assign a route id to each unique route (referred to as routid).
%a route is the set of position a single cell travels through.
%Input: M (nX5):
%           1 - time, 2 - id,3 - successor, 4,5,6- xyz
%       cellname (nX1)(optional):
%           names of embryo cells in a cell         
%       
%adjustable parameters
% TIME_COL = 1;
% ID_COL = 2;
% SUCCES_COL = 3;
% ROUTE_COL = 7;

n = length(M);
% if nargin == 1
%     M = sortrows(M,[TIME_COL ID_COL]);
%     [uval, first_of] = unique(M(:,1), 'first') ;
% 
%     %terminate all routes
% 
%     M(max(M(:,1))== M(:,1) ,SUCCES_COL) = 0;
% 
%     %add a column for route id
%     M = [M,zeros(n,1)];
%     routid = 1;
% 
%     for i=1:n
%         currentid = i;
%         if ( M(currentid,ROUTE_COL) > 0)
%             continue;
%         end
%         M(currentid,ROUTE_COL) = routid;
% 
%         while  (currentid <= n ) %continue until reaching the end of the route
%             M(currentid,ROUTE_COL) = routid;
%             successor = M(currentid,SUCCES_COL);
% 
%             if ((successor == 0) || (currentid > n ))
%                 break;
%             end
%             %look for successor in next time block
%             nexttimestep = first_of(find(uval ==  M(currentid,TIME_COL))+1 );
%             j=0;
%             while( successor ~= M(nexttimestep+j,ID_COL))
%                 j = j+1;
%             end
%             currentid = nexttimestep+j;
%         end
% 
% 
% 
%         routid = routid+1;
%     end
% else

[scellname,IX] = sort([M.name]);
%routecell = unique(scellname);
M = M(IX);

%add a column for route id
routid = 1;

for i=1:n
    M(i).routid = routid;
    if (M(i).successor == 0)
        routid = routid+1;
    end

end
    
%end







