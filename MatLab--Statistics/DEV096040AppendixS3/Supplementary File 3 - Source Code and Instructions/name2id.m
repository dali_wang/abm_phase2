function id = name2id(M, cellnm, type)

%Convert cell name to routeid (a unique index given to a cell)
%Input : M - matrix of cells
%        cellnm - cell name
%        type - optional , 'e' for exact match
%
if nargin == 2
    type = 'e';
end

if ischar(cellnm)
    id = name2id_helper(M, cellnm, type);
else
    n = length(cellnm);
    id = zeros(1,n);
    for i = 1:n
        id(i) = name2id_helper(M, cellnm(i), type);
    end
end

if isempty(id)
    error(['cell : ' cellnm ' not found'])
end

end



function id = name2id_helper(M, cellnm, type)
if ~isempty(strfind(type, 'e')) %exact
    sregexp = strcat('\<' ,cellnm ,'\>');
else
    sregexp = cellnm;
end

silent = ~isempty(strfind(type, 's'));


res =  ~cellfun(@isempty, regexpi([M.name], sregexp)) ; %finds the entries of M that respond to this cell returns a large logical vector
r = [M.routid]; %returns the id at each entry
id =  unique(r(res));  %returns the id that this cell's entries are labeled by

if isempty(id)
    id = -1;
%     if ~silent
%         warning(strcat(cellnm{:},' - no cells found'));
%     end
elseif length(id) ~= 1 && strcmp('e', type) && ~silent
    warning('found more than 1 cells');
    id=id(2);
end

end