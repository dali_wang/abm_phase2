function positions = treeDisplayValueColorsFlexibleCells(values,titleString,imageName,cells)

nCells = length(cells);
nEmbs = length(titleString);

c=1;
genInfo = zeros(50,2); % First holds and index for the lineage group,
% Second holds the size of the generation
genIdx = 0;
cellLin = zeros(1,nCells); % Holds the lineage group of each cell

% Find the cells whose first time points we do not always observe, and hence
% whose position in the lineage tree is drawn manually.
manualIdxs = zeros(1,12);

    if strcmp(cells{1,c},'ABal')
        manualIdxs(1) = c;
    elseif strcmp(cells{1,c},'ABar')
        manualIdxs(2) = c;
    elseif strcmp(cells{1,c},'ABpl')
        manualIdxs(3) = c;
    elseif strcmp(cells{1,c},'ABpr')
        manualIdxs(4) = c;
    elseif strcmp(cells{1,c},'MS')
        manualIdxs(5) = c;
    elseif strcmp(cells{1,c},'E')
        manualIdxs(6) = c;
    elseif strcmp(cells{1,c},'C')
        manualIdxs(7) = c;
    elseif strcmp(cells{1,c},'D')
        manualIdxs(8) = c;
    elseif strcmp(cells{1,c},'P3')
        manualIdxs(9) = c;
    elseif strcmp(cells{1,c},'P4')
        manualIdxs(10) = c;
    elseif strcmp(cells{1,c},'Z3')
        manualIdxs(11) = c;
    elseif strcmp(cells{1,c},'Z2')
        manualIdxs(12) = c;
    end

while c<nCells
    c=c+1;
    if strcmp(cells{1,c},'ABal')
        manualIdxs(1) = c;
    elseif strcmp(cells{1,c},'ABar')
        manualIdxs(2) = c;
    elseif strcmp(cells{1,c},'ABpl')
        manualIdxs(3) = c;
    elseif strcmp(cells{1,c},'ABpr')
        manualIdxs(4) = c;
    elseif strcmp(cells{1,c},'MS')
        manualIdxs(5) = c;
    elseif strcmp(cells{1,c},'E')
        manualIdxs(6) = c;
    elseif strcmp(cells{1,c},'C')
        manualIdxs(7) = c;
    elseif strcmp(cells{1,c},'D')
        manualIdxs(8) = c;
    elseif strcmp(cells{1,c},'P3')
        manualIdxs(9) = c;
    elseif strcmp(cells{1,c},'P4')
        manualIdxs(10) = c;
    elseif strcmp(cells{1,c},'Z3')
        manualIdxs(11) = c;
    elseif strcmp(cells{1,c},'Z2')
        manualIdxs(12) = c;
    end
    
    % Find the lineage group and generation of each cell based on its first letter and length
    if length(cells{1,c-1})~=length(cells{1,c}) || ~strcmp(cells{1,c-1}(1),cells{1,c}(1))
        genIdx = genIdx + 1;
            if strcmp(cells{1,c-1}(1),'A')
                genInfo(genIdx,1) = 1;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 2);
            elseif strcmp(cells{1,c-1}(1),'M')
                genInfo(genIdx,1) = 2;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 2);
            elseif strcmp(cells{1,c-1}(1),'E')
                genInfo(genIdx,1) = 3;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 1);
            elseif strcmp(cells{1,c-1}(1),'C')
                genInfo(genIdx,1) = 4;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 1);
            elseif strcmp(cells{1,c-1}(1),'D')
                genInfo(genIdx,1) = 5;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 1);
            elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
                genInfo(genIdx,1) = 6;
                genInfo(genIdx,2) = 2^(length(cells{1,c-1}) - 1);
            end
    end
    if strcmp(cells{1,c-1}(1),'A')
        cellLin(c-1) = 1;
    elseif strcmp(cells{1,c-1}(1),'M')
        cellLin(c-1) = 2;
    elseif strcmp(cells{1,c-1}(1),'E')
        cellLin(c-1) = 3;
    elseif strcmp(cells{1,c-1}(1),'C')
        cellLin(c-1) = 4;
    elseif strcmp(cells{1,c-1}(1),'D')
        cellLin(c-1) = 5;
    elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
        cellLin(c-1) = 6;
    end
end
if strcmp(cells{1,c}(1),'A')
    cellLin(c) = 1;
elseif strcmp(cells{1,c}(1),'M')
    cellLin(c) = 2;
elseif strcmp(cells{1,c}(1),'E')
    cellLin(c) = 3;
elseif strcmp(cells{1,c}(1),'C')
    cellLin(c) = 4;
elseif strcmp(cells{1,c}(1),'D')
    cellLin(c) = 5;
elseif strcmp(cells{1,c}(1),'P') || strcmp(cells{1,c}(1),'Z')
    cellLin(c) = 6;
end

nLinGroupGens = zeros(1,6);
genStart = zeros(1,6);
linYdif = zeros(1,6);
for c = 1:5
    linIdx = find(genInfo(:,1) == c); % find generations in this lineage group
    nLinGroupGens(c) = genInfo(linIdx(length(linIdx)),2); % find biggest generation
    if c == 1
        genStart(c) = 1; % the first lineage group starts at 1
        linYdif(c) = 10/(log2(nLinGroupGens(c)) - 1); % amount of y difference each generation is
        % number of generations displayed (starts at AB2 not AB)
    else
        linIdx = find(genInfo(:,1) == c-1);
        genStart(c) = genStart(c-1) + genInfo(linIdx(length(linIdx)),2) + 1;
        % the next lineage groups starts at the last lineage group + the
        % number of cells in the largest generation of the last lineage
        % group plus 1
        if (c == 2 || c == 3 || c == 4 )
            linYdif(c) = 10/(log2(nLinGroupGens(c))+1);
        else
            linYdif(c) = 9/(log2(nLinGroupGens(c))+1);
        end            
    end
end

nLinGroupGens = log2(nLinGroupGens);% + 1;
%nLinGroupGens(1) = nLinGroupGens(1) - 2;

positions = zeros(nCells,4);
crossbars = zeros(nCells,4);
for c = 1:nCells
    len = length(cells{1,c});
    if cellLin(c) == 1 || cellLin(c) == 2
        nGen = len - 2;
    else
        nGen = len - 1; 
    end
    incriment = 1;
    for p = len:-1:2
        if strcmp(cells{c}(p),'p') || strcmp(cells{c}(p),'r') || strcmp(cells{c}(p),'v')
            incriment = incriment + 2^(len-p);
        end
    end
    
    if cellLin(c) ~= 6
        positions(c,1) = genStart(cellLin(c)) - 2^(nLinGroupGens(cellLin(c)) - nGen - 1) + 2^(nLinGroupGens(cellLin(c)) - nGen) * incriment;
        % lineage group start + a term for each generation starting further
        % along + this cell's position within the genration
        positions(c,2) = genStart(cellLin(c)) - 2^(nLinGroupGens(cellLin(c)) - nGen - 1) + 2^(nLinGroupGens(cellLin(c)) - nGen) * incriment;
        positions(c,3) = -10 + linYdif(cellLin(c))*(nLinGroupGens(cellLin(c)) - nGen);
        positions(c,4) = -10 + linYdif(cellLin(c))*(nLinGroupGens(cellLin(c)) - nGen + 1);
        
        crossbars(c,1) = genStart(cellLin(c)) - 2^(nLinGroupGens(cellLin(c)) - nGen - 1) - 2^(nLinGroupGens(cellLin(c)) - nGen - 2) + 2^(nLinGroupGens(cellLin(c)) - nGen) * incriment;
        crossbars(c,2) = genStart(cellLin(c)) - 2^(nLinGroupGens(cellLin(c)) - nGen - 1) + 2^(nLinGroupGens(cellLin(c)) - nGen - 2)+ 2^(nLinGroupGens(cellLin(c)) - nGen) * incriment;
        crossbars(c,3) = -10 + linYdif(cellLin(c))*(nLinGroupGens(cellLin(c)) - nGen);
        crossbars(c,4) = -10 + linYdif(cellLin(c))*(nLinGroupGens(cellLin(c)) - nGen);
    else
        
    end
    
end

for e = 1:nEmbs
    figure;hold on
    for c = 1:nCells
        % Draw each cell's vertical line.
        line([positions(c,1) positions(c,2)], [positions(c,3) positions(c,4)],...
            'Color',values(c,:),'Linewidth',2);
        % Draw each cell's horizontal line.
        line([crossbars(c,1) crossbars(c,2)], [crossbars(c,3) crossbars(c,4)],...
            'Color', values(c,:),'Linewidth',2);
    end
    
    set(gcf,'Position',[100 100 1500 500])%[200 265 243 165])%[200 265 495 324])%
    
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    
    %Manually draw top of tree one line at a time
    manualPositions = zeros(12, 4);
    manualCrossbars = zeros(12, 4);
    if manualIdxs(1) ~= 0 && manualIdxs(2) ~= 0 %ABa
        manualCrossbars(4,1) = positions(manualIdxs(1),1);
        manualCrossbars(4,2) = positions(manualIdxs(2),1);
        manualCrossbars(4,3) = positions(manualIdxs(1),4);
        manualCrossbars(4,4) = positions(manualIdxs(2),4);
        
        manualPositions(4,1) = (manualCrossbars(4,1) + manualCrossbars(4,2))/2;
        manualPositions(4,2) = (manualCrossbars(4,1) + manualCrossbars(4,2))/2;
        manualPositions(4,3) = manualCrossbars(4,3);
        manualPositions(4,4) = manualCrossbars(4,3) + 0.5;
    end
    if manualIdxs(3) ~= 0 && manualIdxs(4) ~= 0 %ABp
        manualCrossbars(5,1) = positions(manualIdxs(3),1);
        manualCrossbars(5,2) = positions(manualIdxs(4),1);
        manualCrossbars(5,3) = positions(manualIdxs(3),4);
        manualCrossbars(5,4) = positions(manualIdxs(4),4);
        
        manualPositions(5,1) = (manualCrossbars(5,1) + manualCrossbars(5,2))/2;
        manualPositions(5,2) = (manualCrossbars(5,1) + manualCrossbars(5,2))/2;
        manualPositions(5,3) = manualCrossbars(5,3);
        manualPositions(5,4) = manualCrossbars(5,3) + 0.5;
    end
    if manualIdxs(5) ~= 0 && manualIdxs(6) ~= 0 %EMS
        manualCrossbars(6,1) = positions(manualIdxs(5),1);
        manualCrossbars(6,2) = positions(manualIdxs(6),1);
        manualCrossbars(6,3) = positions(manualIdxs(5),4);
        manualCrossbars(6,4) = positions(manualIdxs(6),4);
        
        manualPositions(6,1) = (manualCrossbars(6,1) + manualCrossbars(6,2))/2;
        manualPositions(6,2) = (manualCrossbars(6,1) + manualCrossbars(6,2))/2;
        manualPositions(6,3) = manualCrossbars(6,3);
        manualPositions(6,4) = manualCrossbars(6,3) + 0.5;
    end
    if manualIdxs(11) ~= 0 && manualIdxs(12) ~= 0 %Z3/2
        prevMax = max(positions(:,1));
        manualPositions(10,1) = prevMax + 2;
        manualPositions(10,2) = prevMax + 2;
        manualPositions(10,3) = -10;
        manualPositions(10,4) = -5;
        
        manualPositions(11,1) = prevMax + 3;
        manualPositions(11,2) = prevMax + 3;
        manualPositions(11,3) = -10;
        manualPositions(11,4) = -5;      
        if manualIdxs(10) ~= 0 && manualIdxs(9) ~=0 %P4
            manualCrossbars(9,1) = manualPositions(10,1);
            manualCrossbars(9,2) = manualPositions(11,1);
            manualCrossbars(9,3) = manualPositions(10,4);
            manualCrossbars(9,4) = manualPositions(10,4);
            
            manualPositions(9,1) = (manualCrossbars(9,1) + manualCrossbars(9,2))/2;
            manualPositions(9,2) = (manualCrossbars(9,1) + manualCrossbars(9,2))/2;
            manualPositions(9,3) = manualPositions(10,4);
            manualPositions(9,4) = positions(manualIdxs(8),4); %go to the same height as D did
        end
    else
        if manualIdxs(10) ~= 0 && manualIdxs(9) ~=0 %P4
            prevMax = max(positions(:,1));
            manualPositions(9,1) = prevMax + 2;
            manualPositions(9,2) = prevMax + 2;
            manualPositions(9,3) = -5;
            manualPositions(9,4) = positions(manualIdxs(8),4);
        end
    end
    if manualIdxs(9) ~=0 && manualIdxs(10) ~=0 %P3
        manualCrossbars(8,1) = positions(manualIdxs(8),1);
        manualCrossbars(8,2) = manualPositions(9,1);
        manualCrossbars(8,3) = positions(manualIdxs(8),4);
        manualCrossbars(8,4) = positions(manualIdxs(8),4);
        
        manualPositions(8,1) = (manualCrossbars(8,1) + manualCrossbars(8,2))/2;
        manualPositions(8,2) = (manualCrossbars(8,1) + manualCrossbars(8,2))/2;
        manualPositions(8,3) = manualCrossbars(8,3); %start at the height of D
        if manualIdxs(8) ~= 0
            manualPositions(8,4) = positions(manualIdxs(7),4); %end at the height of C
        else
            manualPositions(8,4) = manualPositions(9,4) + 0.5;
        end
    end
    if manualIdxs(8) ~= 0 && manualIdxs(7) ~= 0 %P2
        manualCrossbars(7,1) = positions(manualIdxs(7),1);
        manualCrossbars(7,2) = manualPositions(8,1);
        manualCrossbars(4,3) = positions(manualIdxs(7),4);
        manualCrossbars(4,4) = positions(manualIdxs(7),4);
        
        manualPositions(7,1) = (manualCrossbars(7,1) + manualCrossbars(7,2))/2;
        manualPositions(7,2) = (manualCrossbars(7,1) + manualCrossbars(7,2))/2;
        manualPositions(7,3) = positions(manualIdxs(7),4);
        manualPositions(7,4) = manualPositions(6,4);
    end
    if manualIdxs(4) ~= 0 && manualIdxs(5) ~=0
       manualCrossbars(2,1) = manualPositions(4,1); 
       manualCrossbars(2,2) = manualPositions(5,1); 
       manualCrossbars(2,3) = manualPositions(4,4);
       manualCrossbars(2,4) = manualPositions(4,4);
       
       manualPositions(2,1) = (manualCrossbars(2,1) + manualCrossbars(2,2))/2;
       manualPositions(2,2) = (manualCrossbars(2,1) + manualCrossbars(2,2))/2;
       manualPositions(2,3) = manualCrossbars(2,3);
       manualPositions(2,4) = manualCrossbars(2,3) + 0.5;
    end
    if manualIdxs(6) ~= 0 && manualIdxs(7) ~=0
       manualCrossbars(3,1) = manualPositions(6,1); 
       manualCrossbars(3,2) = manualPositions(7,1); 
       manualCrossbars(3,3) = manualPositions(6,4);
       manualCrossbars(3,4) = manualPositions(6,4);
       
       manualPositions(3,1) = (manualCrossbars(3,1) + manualCrossbars(3,2))/2;
       manualPositions(3,2) = (manualCrossbars(3,1) + manualCrossbars(3,2))/2;
       manualPositions(3,3) = manualCrossbars(3,3);
       manualPositions(3,4) = manualCrossbars(3,3) + 0.5;
    end
    if manualIdxs(2) ~=0 && manualIdxs(3) ~= 0
       manualCrossbars(1,1) = manualPositions(2,1); 
       manualCrossbars(1,2) = manualPositions(3,1); 
       manualCrossbars(1,3) = manualPositions(2,4);
       manualCrossbars(1,4) = manualPositions(2,4);
       
       manualPositions(1,1) = (manualCrossbars(1,1) + manualCrossbars(1,2))/2;
       manualPositions(1,2) = (manualCrossbars(1,1) + manualCrossbars(1,2))/2;
       manualPositions(1,3) = manualCrossbars(1,3);
       manualPositions(1,4) = manualCrossbars(1,3) + 0.5; 
    end

    for c = 1:12
         line([manualPositions(c,1) manualPositions(c,2)], [manualPositions(c,3) manualPositions(c,4)],...
            'Color',[0 0 0],'Linewidth',2);
        line([manualCrossbars(c,1) manualCrossbars(c,2)], [manualCrossbars(c,3) manualCrossbars(c,4)],...
            'Color', [ 0 0 0 ],'Linewidth',2);
    end
    
     xMin = min([min(positions(:,1)) min(positions(:,2)) min(manualPositions(:,1)) min(manualPositions(:,2))]) - 0.5;
    xMax = max([max(positions(:,1)) max(positions(:,2))  max(manualPositions(:,1)) max(manualPositions(:,2))]) + 0.5;
    yMin = min([min(positions(:,3)) min(positions(:,4)) min(manualPositions(:,3)) min(manualPositions(:,4))]);
    yMax = max([max(positions(:,3)) max(positions(:,4)) max(manualPositions(:,3)) max(manualPositions(:,4))]);  
    axis([xMin xMax yMin yMax])
    title(titleString{e})
    % Save image.
    set(gcf,'PaperPositionMode','auto');
    print(gcf,imageName{e},'-dpng');
    close all
end