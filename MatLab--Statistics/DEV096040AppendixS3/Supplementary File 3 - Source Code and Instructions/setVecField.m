function struct = setVecField(struct, fieldname,vec)
% Load a vector of values to a vector of structs.
%USAGE - implements struct(:).field = vec(:)
%        if field does not exist it will be created.
%Input - struct : a struct that contains fields
%        fieldname  : is field name as a string
%        vec    : vector of values


n = length(struct);
if (~isfield(struct, fieldname))
    warning ('fieldname is not part of struct~');
end

if (n ~= length(vec))
    error ('Struct and vector are not in the same length');
end

for i=1:n
    struct(i).(fieldname) = vec(i);
end