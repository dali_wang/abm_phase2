function varargout = outputBrowserPC_BrowseOrEnterDir(varargin)
% OUTPUTBROWSERPC_BROWSEORENTERDIR MATLAB code for outputBrowserPC_BrowseOrEnterDir.fig
%      OUTPUTBROWSERPC_BROWSEORENTERDIR, by itself, creates a new OUTPUTBROWSERPC_BROWSEORENTERDIR or raises the existing
%      singleton*.
%
%      H = OUTPUTBROWSERPC_BROWSEORENTERDIR returns the handle to a new OUTPUTBROWSERPC_BROWSEORENTERDIR or the handle to
%      the existing singleton*.
%
%      OUTPUTBROWSERPC_BROWSEORENTERDIR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTPUTBROWSERPC_BROWSEORENTERDIR.M with the given input arguments.
%
%      OUTPUTBROWSERPC_BROWSEORENTERDIR('Property','Value',...) creates a new OUTPUTBROWSERPC_BROWSEORENTERDIR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before outputBrowserPC_BrowseOrEnterDir_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to outputBrowserPC_BrowseOrEnterDir_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help outputBrowserPC_BrowseOrEnterDir

% Last Modified by GUIDE v2.5 20-Feb-2013 11:50:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @outputBrowserPC_BrowseOrEnterDir_OpeningFcn, ...
                   'gui_OutputFcn',  @outputBrowserPC_BrowseOrEnterDir_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before outputBrowserPC_BrowseOrEnterDir is made visible.
function outputBrowserPC_BrowseOrEnterDir_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to outputBrowserPC_BrowseToDir (see VARARGIN)

%SHOW DEFAULT IMAGE HERE

% Choose default command line output for outputBrowserPC_BrowseToDir
handles.output = hObject;
if ~isempty(varargin)
    %     outputDirLen = length(varargin{1});
    %     lastSlash=0;
    %     for c=1:outputDirLen
    %         if strcmp(varargin{1}(c),'/') || strcmp(varargin{1}(c),'\')
    %             lastSlash=c;
    %         end
    %     end
    %     if lastSlash ~=0
    %         embName = varargin{1}(lastSlash+1:outputDirLen);
    %     else
    %         embName = '';
    %     end
    %
    %     if length(varargin) == 1
    %         % handles = setfield(handles,'outputFolder',varargin{1});
    %         handles.outputFolder = varargin{1};
    %         handles.embName = embName;
    %     end
    %     % Update handles structure
    %     guidata(hObject, handles);
    %     folderDisplay_Callback(hObject, eventdata, handles);
    %     viewPvalMtx_Callback(hObject, eventdata, handles);
    %
    %     handles.output = hObject;
    %     outputDirLen = length(varargin{1});
    %     lastSlash=0;
    %     for c=1:outputDirLen
    %         if strcmp(varargin{1}(c),'/') || strcmp(varargin{1}(c),'\')
    %             lastSlash=c;
    %         end
    %     end
    %     if lastSlash ~=0
    %         embName = varargin{1}(lastSlash+1:outputDirLen);
    %     else
    %         embName = '';
    %     end
    % Choose default command line output for outputBrowserPC
    outputDir = varargin{1};
    handles.outputFolder = '';
    handles.embName = '';
    set(handles.outputFolder,'String',outputDir);
    set(handles.folderDisplay,'String',outputDir);
    outputDirLen = length(outputDir);
    lastSlash=0;
    for c=1:outputDirLen
        if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
            lastSlash=c;
        end
    end
    if lastSlash ~=0
        embName = outputDir(lastSlash+1:outputDirLen);
    else
        embName = '';
    end
    set(handles.embName,'String',embName);
    guidata(hObject, handles);
    assignin('base','outputFolder',outputDir);
    %     outputDirLen = length(varargin{1});
    %     lastSlash=0;
    %     for c=1:outputDirLen
    %         if strcmp(varargin{1}(c),'/') || strcmp(varargin{1}(c),'\')
    %             lastSlash=c;
%         end
%     end
%     if lastSlash ~=0
%         embName = varargin{1}(lastSlash+1:outputDirLen);
%     else
%         embName = '';
%     end
%     
%     %handles = setfield(handles,'outputFolder',varargin{1});
%     handles.outputFolder = '';
%      handles.embName = '';
%     % Update handles structure
%     guidata(hObject, handles);
%     set(handles.folderDisplay,'String',varargin{1});
    folderDisplay_Callback(hObject, eventdata, handles);
    viewPvalMtx_Callback(hObject, eventdata, handles);
else
    handles.outputFolder = '';
    handles.embName = '';
    % Update handles structure
    guidata(hObject, handles);
end
% outputDirLen = length(varargin{1});
% lastSlash=0;
% for c=1:outputDirLen
%     if strcmp(varargin{1}(c),'/') || strcmp(varargin{1}(c),'\')
%         lastSlash=c;
%     end
% end
% if lastSlash ~=0
%     embName = varargin{1}(lastSlash+1:outputDirLen);
% else
%     embName = '';
% end

% if length(varargin) == 1
% %     %handles = setfield(handles,'outputFolder',varargin{1});
%      handles.outputFolder = '';
%      handles.embName = '';
% % end
% % Update handles structure
% guidata(hObject, handles);

%folderDisplay_Callback(hObject, eventdata, handles);
%viewPvalMtx_Callback(hObject, eventdata, handles);
% UIWAIT makes outputBrowserPC_BrowseToDir wait for user response (see UIRESUME)
uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = outputBrowserPC_BrowseOrEnterDir_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = [];%handles.output;


% --- Executes on button press in viewPvalMtx.
function viewPvalMtx_Callback(hObject, eventdata, handles)
% hObject    handle to viewPvalMtx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/phenotypeMatrixAdjustedP-values.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying p-value matrix');
else
    set(handles.currentDisplayText,'String','P-value matrix image does not exist');
end


% --- Executes on button press in slidingWindow.
function slidingWindow_Callback(hObject, eventdata, handles)
% hObject    handle to slidingWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
outputDirLen = length(outputDir);
lastSlash=0;
for c=1:outputDirLen
    if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    embName = outputDir(lastSlash+1:outputDirLen);
else
    embName = '';
end
movieName = [outputDir '/' embName '_slidingWindowNormalized.avi'];
if exist(movieName,'file')
    currentMovie = VideoReader(movieName);
    nFrames = currentMovie.NumberOfFrames;
    vidHeight = currentMovie.Height;
    vidWidth = currentMovie.Width;
    % Preallocate movie structure.
    mov(1:nFrames) = ...
        struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
        'colormap', []);
    % Read one frame at a time.
    for k = 1 : nFrames
        mov(k).cdata = read(currentMovie, k);
    end
    % Size a figure based on the video's width and height.
    hf = handles.axes1;
    cla(hf);
   % set(hf, 'position', [150 150 vidWidth vidHeight])
    % Play back the movie once at the video
    set(handles.currentDisplayText,'String','Playing time window displacement movie');
    movie(hf, mov, 1, currentMovie.FrameRate);
else
    movieName = [outputDir '/' embName '_slidingWindowAligned.avi'];
    if exist(movieName,'file')
        currentMovie = VideoReader(movieName);
        nFrames = currentMovie.NumberOfFrames;
        vidHeight = currentMovie.Height;
        vidWidth = currentMovie.Width;
        % Preallocate movie structure.
        mov(1:nFrames) = ...
            struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
            'colormap', []);
        % Read one frame at a time.
        for k = 1 : nFrames
            mov(k).cdata = read(currentMovie, k);
        end
        % Size a figure based on the video's width and height.
        hf = handles.axes1;
        set(hf, 'position', [150 150 vidWidth vidHeight])
        % Play back the movie once at the vide
        set(handles.currentDisplayText,'String','Playing time window displacement movie');
        movie(hf, mov, 1, currentMovie.FrameRate);
    else
        set(handles.currentDisplayText,'String','Time window displacement movie does not exist');
    end
    set(handles.currentDisplayText,'String','Time window displacement movie does not exist');
end


% --- Executes on button press in viewGlobalClock.
function viewGlobalClock_Callback(hObject, eventdata, handles)
% hObject    handle to viewGlobalClock (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/robustEmbryonicClockFittingSummary.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying global clock fitting');
else
    set(handles.currentDisplayText,'String','Global clock fittingimage does not exist');
end



% --- Executes on button press in viewSlowingDown.
function viewSlowingDown_Callback(hObject, eventdata, handles)
% hObject    handle to viewSlowingDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/possibleSlowingDown.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying lineage group timing');
else
    set(handles.currentDisplayText,'String','Lineage group timing matrix image does not exist');
end


% --- Executes on button press in viewExprLineageTree.
function viewExprLineageTree_Callback(hObject, eventdata, handles)
% hObject    handle to viewExprLineageTree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/expressionTreeVisualization.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying expression lineage tree');
else
    set(handles.currentDisplayText,'String','Expression lineage tree image does not exist');
end


% --- Executes on button press in viewExpressionVals.
function viewExpressionVals_Callback(hObject, eventdata, handles)
% hObject    handle to viewExpressionVals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/robustExpressionFittingSummary.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying cell expression values');
else
    set(handles.currentDisplayText,'String','Cell expression values image does not exist');
end


% --- Executes on button press in AB32to128iceCream.
function AB32to128iceCream_Callback(hObject, eventdata, handles)
% hObject    handle to AB32to128iceCream (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
outputDirLen = length(outputDir);
lastSlash=0;
for c=1:outputDirLen
    if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    embName = outputDir(lastSlash+1:outputDirLen);
else
    embName = '';
end
movieName = [outputDir '/' embName '_AB32to128coneMovieNormalized.avi'];
if exist(movieName,'file')
    currentMovie = VideoReader(movieName);
    nFrames = currentMovie.NumberOfFrames;
    vidHeight = currentMovie.Height;
    vidWidth = currentMovie.Width;
    % Preallocate movie structure.
    mov(1:nFrames) = ...
        struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
        'colormap', []);
    % Read one frame at a time.
    for k = 1 : nFrames
        mov(k).cdata = read(currentMovie, k);
    end
    % Size a figure based on the video's width and height.
    hf = handles.axes1;
    cla(hf);
   % set(hf, 'position', [150 150 vidWidth vidHeight])
    % Play back the movie once at the video
    set(handles.currentDisplayText,'String','Playing AB32 to 138 movie');
    movie(hf, mov, 1, currentMovie.FrameRate);
else
    movieName = [outputDir '/' embName '_AB32to128coneMovieAligned.avi'];
    if exist(movieName,'file')
        currentMovie = VideoReader(movieName);
        nFrames = currentMovie.NumberOfFrames;
        vidHeight = currentMovie.Height;
        vidWidth = currentMovie.Width;
        % Preallocate movie structure.
        mov(1:nFrames) = ...
            struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
            'colormap', []);
        % Read one frame at a time.
        for k = 1 : nFrames
            mov(k).cdata = read(currentMovie, k);
        end
        % Size a figure based on the video's width and height.
        hf = handles.axes1;
        set(hf, 'position', [150 150 vidWidth vidHeight])
        % Play back the movie once at the vide
        set(handles.currentDisplayText,'String','Playing AB32 to 128 movie');
        movie(hf, mov, 1, currentMovie.FrameRate);
    else
        set(handles.currentDisplayText,'String','AB32 to 128 movie does not exist');
    end
    set(handles.currentDisplayText,'String','AB32 to 128 movie does not exist');
end

% --- Executes on button press in covarianceEllipse.
function covarianceEllipse_Callback(hObject, eventdata, handles)
% hObject    handle to covarianceEllipse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
outputDirLen = length(outputDir);
lastSlash=0;
for c=1:outputDirLen
    if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    embName = outputDir(lastSlash+1:outputDirLen);
else
    embName = '';
end
movieName = [outputDir '/' embName '_covarianceEllipsesNormalized.avi'];
if exist(movieName,'file')
    currentMovie = VideoReader(movieName);
    nFrames = currentMovie.NumberOfFrames;
    vidHeight = currentMovie.Height;
    vidWidth = currentMovie.Width;
    % Preallocate movie structure.
    mov(1:nFrames) = ...
        struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
        'colormap', []);
    % Read one frame at a time.
    for k = 1 : nFrames
        mov(k).cdata = read(currentMovie, k);
    end
    % Size a figure based on the video's width and height.
    hf = handles.axes1;
    cla(hf);
   % set(hf, 'position', [150 150 vidWidth vidHeight])
    % Play back the movie once at the video
    set(handles.currentDisplayText,'String','Playing covariance ellipse movie');
    movie(hf, mov, 1, currentMovie.FrameRate);
else
    movieName = [outputDir '/' embName '_covarianceEllipsesAligned.avi'];
    if exist(movieName,'file')
        currentMovie = VideoReader(movieName);
        nFrames = currentMovie.NumberOfFrames;
        vidHeight = currentMovie.Height;
        vidWidth = currentMovie.Width;
        % Preallocate movie structure.
        mov(1:nFrames) = ...
            struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
            'colormap', []);
        % Read one frame at a time.
        for k = 1 : nFrames
            mov(k).cdata = read(currentMovie, k);
        end
        % Size a figure based on the video's width and height.
        hf = handles.axes1;
        set(hf, 'position', [150 150 vidWidth vidHeight])
        % Play back the movie once at the vide
        set(handles.currentDisplayText,'String','Playing covariance ellipse movie');
        movie(hf, mov, 1, currentMovie.FrameRate);
    else
        set(handles.currentDisplayText,'String','Covariance ellipse movie does not exist');
    end
    set(handles.currentDisplayText,'String','Covariance ellipse movie does not exist');
end


function currentDisplayText_Callback(hObject, eventdata, handles)
% hObject    handle to currentDisplayText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentDisplayText as text
%        str2double(get(hObject,'String')) returns contents of currentDisplayText as a double


% --- Executes during object creation, after setting all properties.
function currentDisplayText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to currentDisplayText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function folderDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to folderDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of folderDisplay as text
%        str2double(get(hObject,'String')) returns contents of folderDisplay as a double
outputDir = get(handles.folderDisplay,'String');
 set(handles.folderDisplay,'String',outputDir);


% --- Executes during object creation, after setting all properties.
function folderDisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to folderDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in NumCellsOverTime.
function NumCellsOverTime_Callback(hObject, eventdata, handles)
% hObject    handle to NumCellsOverTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/numberOfCellsPerMinute.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying number of cells over time');
else
    set(handles.currentDisplayText,'String','Number of cells per time point image does not exist');
end



% --- Executes on selection change in subsetPvalPopUp.
function subsetPvalPopUp_Callback(hObject, eventdata, handles)
% hObject    handle to subsetPvalPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns subsetPvalPopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from subsetPvalPopUp

value = get(hObject,'Value');
outputDir = get(handles.folderDisplay,'String');
switch value
    case 1 %'All descendents are hypodermis'
        imName = [outputDir '/allHypPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells whose descendents are all hypodermis');
    case 2 %'All descendents are muscle'
        imName = [outputDir '/allMuscPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells whose descendents are all muscles');
    case 3 %'All descendents are neurons'
        imName = [outputDir '/allNeurPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells whose descendents are all neurons');
    case 4 %'All descendents are pharynx'
        imName = [outputDir '/allPharPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells whose descendents are all pharyngeal');
    case 5 %'Some descendents are hypodermis'
        imName = [outputDir '/someHypPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells who have some hypodermal descendents');    
    case 6 %'Some descendents are muscle'
        imName = [outputDir '/someMuscPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells who have some muscular descendents');     
    case 7 %'Some descendents are neurons'
        imName = [outputDir '/someNeurPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells who have some neuronal descendents'); 
    case 8 %'Some descendents are pharynx'
        imName = [outputDir '/somePharPhenotypeMatrixAdjustedP-values.png'];
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying p-value matrix for cells who have some pharyngeal descendents'); 
end



% --- Executes during object creation, after setting all properties.
function subsetPvalPopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to subsetPvalPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in corrMtxAB64.
function corrMtxAB64_Callback(hObject, eventdata, handles)
% hObject    handle to corrMtxAB64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
imName = [outputDir '/correlationMatrixAB64andAB128Normalized.png'];
if exist(imName,'file')
    curImage = imread(imName,'png');
    imagesc(curImage);
    axis equal off;
    set(handles.currentDisplayText,'String','Displaying correlation matrix for AB64 and AB128');
else
    imName = [outputDir '/correlationMatrixAB64andAB128Raw.png'];
    if  exist(imName,'file')
        curImage = imread(imName,'png');
        imagesc(curImage);
        axis equal off;
        set(handles.currentDisplayText,'String','Displaying correlation matrix for AB64 and AB128');
    else
        set(handles.currentDisplayText,'String','Correlation matrix for AB64 and AB128 does not exist');
    end
end



% --- Executes on button press in clusteringMovie.
function clusteringMovie_Callback(hObject, eventdata, handles)
% hObject    handle to clusteringMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = get(handles.folderDisplay,'String');
outputDirLen = length(outputDir);
lastSlash=0;
for c=1:outputDirLen
    if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    embName = outputDir(lastSlash+1:outputDirLen);
else
    embName = '';
end
movieName = [outputDir '/' embName '_AB32to128coneMovieClustersNormalized.avi'];
if exist(movieName,'file')
    currentMovie = VideoReader(movieName);
    nFrames = currentMovie.NumberOfFrames;
    vidHeight = currentMovie.Height;
    vidWidth = currentMovie.Width;
    % Preallocate movie structure.
    mov(1:nFrames) = ...
        struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
        'colormap', []);
    % Read one frame at a time.
    for k = 1 : nFrames
        mov(k).cdata = read(currentMovie, k);
    end
    % Size a figure based on the video's width and height.
    hf = handles.axes1;
    cla(hf);
   % set(hf, 'position', [150 150 vidWidth vidHeight])
    % Play back the movie once at the video
    set(handles.currentDisplayText,'String','Playing migration group clustering movie');
    movie(hf, mov, 1, currentMovie.FrameRate);
else
    movieName = [outputDir '/' embName '__AB32to128coneMovieClustersAligned.avi'];
    if exist(movieName,'file')
        currentMovie = VideoReader(movieName);
        nFrames = currentMovie.NumberOfFrames;
        vidHeight = currentMovie.Height;
        vidWidth = currentMovie.Width;
        % Preallocate movie structure.
        mov(1:nFrames) = ...
            struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
            'colormap', []);
        % Read one frame at a time.
        for k = 1 : nFrames
            mov(k).cdata = read(currentMovie, k);
        end
        % Size a figure based on the video's width and height.
        hf = handles.axes1;
        set(hf, 'position', [150 150 vidWidth vidHeight])
        % Play back the movie once at the vide
        set(handles.currentDisplayText,'String','Playing migration group clustering movie');
        movie(hf, mov, 1, currentMovie.FrameRate);
    else
        set(handles.currentDisplayText,'String','Migration group clustering movie does not exist');
    end
    set(handles.currentDisplayText,'String','Migration group clustering movie does not exist');
end

% --- Executes on button press in chooseDirButton.
function chooseDirButton_Callback(hObject, eventdata, handles)
% hObject    handle to chooseDirButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outputDir = uigetdir;
set(handles.outputFolder,'String',outputDir);
set(handles.folderDisplay,'String',outputDir);
outputDirLen = length(outputDir);
lastSlash=0;
for c=1:outputDirLen
    if strcmp(outputDir(c),'/') || strcmp(outputDir(c),'\')
        lastSlash=c;
    end
end
if lastSlash ~=0
    embName = outputDir(lastSlash+1:outputDirLen);
else
    embName = '';
end
set(handles.embName,'String',embName);
guidata(hObject, handles);
assignin('base','outputFolder',outputDir);
