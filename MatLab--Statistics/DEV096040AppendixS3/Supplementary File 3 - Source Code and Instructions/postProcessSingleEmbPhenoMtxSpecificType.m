function reScale=postProcessSingleEmbPhenoMtxSpecificType(phenoMatrix, daughterInfo, cells, cycleFlag, ...
    colorRange, reScaleVal, cMap, scaleWholeMtx, lowerBound, sigCutoff)
[nRows nCells]=size(phenoMatrix);
reScale=phenoMatrix;
for r=1:nRows
    for c=1:nCells
        if abs(phenoMatrix(r,c))>reScaleVal && phenoMatrix(r,c)~=0.51
            %If measurement is definitely wild type, rescale it to the top
            %of the wild type range
            reScale(r,c)=reScaleVal;
        end
        if daughterInfo(r,c) == 1
            if cycleFlag(r) == 1
                if reScale(r,c) > 0 && reScale(r,c) < sigCutoff
                    reScale(r,c)=lowerBound-reScale(r,c);
                else
                    reScale(r,c)=1;
                end
            else
                reScale(r,c)=1;
            end
        end
    end
end
figure;imagesc(reScale*scaleWholeMtx);colormap(cMap);caxis(colorRange);colorbar
lw=1;

c=1;
genInfo = zeros(1,50);
genLabels = cell(1,50);
genType = cell(1,50);
genIdx = 0;
while c<nCells
    c=c+1;
    if length(cells{1,c-1})~=length(cells{1,c})
        genIdx = genIdx + 1;
            if strcmp(cells{1,c-1}(1),'A')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 2);
                genType{genIdx} = 'A';
                if nGen>2
                    genLabels{genIdx} = ['AB' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            elseif strcmp(cells{1,c-1}(1),'M')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 2);
                genType{genIdx} = 'M';
                if nGen>2
                    genLabels{genIdx} = ['MS' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            elseif strcmp(cells{1,c-1}(1),'E')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 1);
                genType{genIdx} = 'E';
                if nGen>2
                    genLabels{genIdx} = ['E' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            elseif strcmp(cells{1,c-1}(1),'C')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 1);
                genType{genIdx} = 'C';
                if nGen>2
                    genLabels{genIdx} = ['C' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            elseif strcmp(cells{1,c-1}(1),'D')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 1);
                genType{genIdx} = 'D';
                if nGen>2
                    genLabels{genIdx} = ['D' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
                genInfo(genIdx) = c - 0.5;
                nGen = 2^(length(cells{1,c-1}) - 1);
                genType{genIdx} = 'P';
                if nGen>2
                    genLabels{genIdx} = ['P' num2str(nGen)];
                else
                    genLabels{genIdx} = '';
                end
            end
    end
end
genIdx = genIdx + 1;
if strcmp(cells{1,c-1}(1),'A')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 2);
    genType{genIdx} = 'A';
    if nGen>2
        genLabels{genIdx} = ['AB' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'M')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 2);
    genType{genIdx} = 'M';
    if nGen>2
        genLabels{genIdx} = ['MS' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'E')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'E';
    if nGen>2
        genLabels{genIdx} = ['E' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'C')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'C';
    if nGen>2
        genLabels{genIdx} = ['C' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'D')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'D';
    if nGen>2
        genLabels{genIdx} = ['D' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
elseif strcmp(cells{1,c-1}(1),'P') || strcmp(cells{1,c-1}(1),'Z')
    genInfo(genIdx) = c - 0.5;
    nGen = 2^(length(cells{1,c-1}) - 1);
    genType{genIdx} = 'P';
    if nGen>2
        genLabels{genIdx} = ['P' num2str(nGen)];
    else
        genLabels{genIdx} = '';
    end
end
genInfo(genIdx) = nCells;
genInfo = genInfo(1:genIdx);
genType = genType(1:genIdx-1);
genLabels = genLabels(1:genIdx);
labelIdx = zeros(1,genIdx);
labelIdx(1) = genInfo(1,1)/2;
line([genInfo(1) genInfo(1)], [0 nRows + 0.5],'Color','k','Linewidth',lw) 
for g = 2:genIdx-1
    labelIdx(g) = (genInfo(g) + genInfo(g-1)) / 2;
    if strcmp(genType{g}(1),genType{g-1}(1))
        line([genInfo(g) genInfo(g)], [0 nRows + 0.5],'Color','k','Linewidth',lw) 
    else
        line([genInfo(g) genInfo(g)], [0 nRows + 0.5],'Color','k','Linewidth',2*lw) 
    end
end
labelIdx(genIdx) = (genInfo(genIdx) + nCells) / 2;
line([genInfo(genIdx) genInfo(genIdx)], [0 nRows + 0.5],'Color','k','Linewidth',lw) 
set(gca,'XTick',labelIdx);
set(gca,'XTickLabel',genLabels);

for r=0.5 : 1 : nRows+0.5
    line([0 nCells+0.5],[r r],'Color','k','Linewidth',lw)
end


