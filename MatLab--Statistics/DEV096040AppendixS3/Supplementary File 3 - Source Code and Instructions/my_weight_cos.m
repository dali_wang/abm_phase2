function [y mag] = my_weight_cos(v1,v2)
%calculate the cosine value between 2 vectors

mag=norm(v1)*norm(v2);
y = v1*v2'/mag;
end