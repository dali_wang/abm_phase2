function [scalingInfo,cycleRatio,absGenMeans,genMeans,genIndiv,genMeanLinGroups,robustWeights,meanGenIndiv,stdGenIndiv]...
    =findScalingInfoUsingGenerationSlopeCycleAnyCellSet(cycleLength,cells,divisionTime,daughterInfo)

% cycleLength is the number of minutes each cell is in the embryo
% cells is the same length as cycleLength and holds the names of the cells
% divisionTime holds the minute that each cell divides at
% daughterInfo is a binary vector which is 0 if the cell has not divided
%  and 1 if the cell has divided 

% There can be multiple rows in each vector which represent multiple
% embryos

% Find the ratio between each cell's cycle length and it's average cycle
% length in stored wild-type average values
daughterInfo=~daughterInfo;
[nEmbs nCells]=size(cycleLength);
cycleRatio=zeros(nEmbs,nCells);
scalingInfo=zeros(nEmbs,3);
load('wtMeansUnscaledNoArrest.mat');
curMeans = zeros(1,nCells);
for c1=1:nCells
    for c2 = 1:352
        if strcmp(cells{c1},cellsStandard{c2})
            nonZero=logical(daughterInfo(:,c1));
            cycleRatio(nonZero,c1)=means(c2)./cycleLength(nonZero,c1);
            curMeans(c1) = means(c2);
        end
    end
end

% Figure out if each generation is normal or not
genInfo = zeros(20,3);
genIdx = 1;
genInfo(genIdx,1) = 1;
genInfo(genIdx,2) = length(cells{1,1});
c=1;
if strcmp(cells{1,c}(1),'A')
    genInfo(genIdx,3) = 1;
elseif strcmp(cells{1,c}(1),'M')
    genInfo(genIdx,3) = 2;
elseif strcmp(cells{1,c}(1),'E')
    genInfo(genIdx,3) = 3;
elseif strcmp(cells{1,c}(1),'C')
    genInfo(genIdx,3) = 4;
elseif strcmp(cells{1,c}(1),'D')
    genInfo(genIdx,3) = 5;
end
while c<nCells
    c=c+1;
    if length(cells{1,c-1})~=length(cells{1,c})
        genIdx = genIdx + 1;
        genInfo(genIdx,1) = c;
        genInfo(genIdx,2) = length(cells{1,c});
        if genInfo(genIdx,2)>0
            if strcmp(cells{1,c}(1),'A')
                genInfo(genIdx,3) = 1;
            elseif strcmp(cells{1,c}(1),'M')
                genInfo(genIdx,3) = 2;
            elseif strcmp(cells{1,c}(1),'E')
                genInfo(genIdx,3) = 3;
            elseif strcmp(cells{1,c}(1),'C')
                genInfo(genIdx,3) = 4;
            elseif strcmp(cells{1,c}(1),'D')
                genInfo(genIdx,3) = 5;
            end
        end
    end
end
nGens=genIdx;
genIdx = genIdx + 1;
genInfo(genIdx,1) = nCells;
genInfo(genIdx,2) = length(cells{1,nCells});
if strcmp(cells{1,nCells}(1),'A')
    genInfo(genIdx,3) = 1;
elseif strcmp(cells{1,nCells}(1),'M')
    genInfo(genIdx,3) = 2;
elseif strcmp(cells{1,nCells}(1),'E')
    genInfo(genIdx,3) = 3;
elseif strcmp(cells{1,nCells}(1),'C')
    genInfo(genIdx,3) = 4;
elseif strcmp(cells{1,nCells}(1),'D')
    genInfo(genIdx,3) = 5;
end

% Find the ration between this generation's mean proliferation rate and the
% wild-type average proliferation rate for this generation
genMeans=zeros(nEmbs,nGens);
absGenMeans=zeros(nEmbs,nGens);
genIndiv=zeros(size(cycleRatio));
for e=1:nEmbs
    for g=1:nGens
        thisGen=genInfo(g,1):(genInfo(g+1,1)-1);
        thisGen=thisGen(daughterInfo(e,thisGen));
        nonZero=logical(daughterInfo(e,thisGen));
        if ~isempty(nonZero)
            genMeans(e,g)=mean(cycleRatio(e,thisGen(nonZero)));
            absGenMeans(e,g)=mean(divisionTime(e,thisGen(nonZero)));
            for c=thisGen
                genIndiv(e,c)=genMeans(e,g)-cycleRatio(e,c);
            end
        end
    end
    
    % Find the global proliferation rate using robust line fitting 
    nonZero=(logical(cycleRatio(e,:)) & logical(curMeans));
    nNonZero=length(nonZero);
    if nNonZero>0
        [b, stats] = robustfit(curMeans(nonZero),cycleLength(e,nonZero),'fair',5,'off');
        scalingInfo(e,1)=b;
        scalingInfo(e,2)=stats.robust_s;%*nCells/length(find(nonZero));
        robustWeights=stats.w;
    end
end

% Fit a line to the generation proliferation rates and see if the
% proliferation rate is slowing down over time
nLins=5;
meanGenIndiv=mean(cycleRatio,1);
stdGenIndiv=std(cycleRatio,[],1);
genMeanLinGroups=zeros(nEmbs,nLins);
load('wtCycleRatioMeansStds');%('wtGenerationTimingNoArresters');%('wtGenerationTiming');
for e=1:nEmbs
    for j = 1:5
        curGens = find(genInfo(:,3) == j)';
        foundGens = logical(genMeans(e,curGens));
        p=polyfit(curGens(foundGens),genMeans(e,curGens(foundGens)),1);
        genMeanLinGroups(e,j)=p(1);
    end
    load('linGroupSlopeStds')
    nStdsOff=genMeanLinGroups(e,:)./linGroupSlopeStd;
    if length(find(nStdsOff>2.5))>2 || length(find(nStdsOff<-2.5))>2
        scalingInfo(e,3)=scalingInfo(e,3)+1;
    end
    if scalingInfo(e,2)>7
        scalingInfo(e,3)=scalingInfo(e,3)+2;
    end
end
