function [meanReds weights rankTestPval exprStatus nonExist]=analyzeExpressionValsTotalProcess(MT,cells,ids,ind)
% Takes in loaded embryos (MT) and a set of cells (cells) and
% returns expression values (meanReds), weights of robust linear fitting (weights),
% rank sum test values (rankTestPval), expression statuses (exprStatus) and
% binary vector of cells that are not seen (nonExist).

nEmbs = length(MT);
nCells = length(cells);
meanReds = zeros(nEmbs, nCells);
redTraj = cell(nEmbs, nCells);
weights = zeros(nEmbs, nCells);
rankTestPval = zeros(nEmbs,nCells);
exprStatus = zeros(nEmbs,nCells);
nonExist = zeros(1,nCells);

for e=1:nEmbs
    disp(sprintf('Calculating expression values for embryo %s',MT(1,e).key))
    fitCells = zeros(1,nCells);
    fitIdx = 0;
    for c=1:nCells
        if ids(c)>0
            fitIdx = fitIdx + 1;
            fitCells(fitIdx) = c;
            % Find current cell's expression values
            idsCur = ind(ids(c)):ind(ids(c)+1)-1;
            redTraj{e,c} = [MT(1,e).value(1,idsCur).rfp];
            meanReds(e,c) = mean(redTraj{e,c}); % the mean expression over its lifetime
            if isnan(meanReds(e,c))
                meanReds(e,c) = 0;
            end
            len = length(redTraj{e,c});
            if len>5
                curPvals = zeros(1,len-2);
                for i = 1:len-2
                    % Calculate the rank sum test for each time point,
                    % meaing that expression could start at any time over
                    % the cell's life
                    x = length(find(isnan(redTraj{e,c}(1:i))));
                    y = length(find(isnan(redTraj{e,c}(i+1:len-2))));
                    xLen = length(find(redTraj{e,c}(1:i)));
                    yLen = length(find(redTraj{e,c}(i+1:len-2)));
                    if x ==0 && y == 0 && xLen ~=0 && yLen ~= 0
                        curPvals(i) = ranksum(redTraj{e,c}(1:i), redTraj{e,c}(i+1:len-2));
                    else
                        curPvals(i) = 0.51;
                    end
                end
                % We use the minimum p-value as the most likely time of
                % expression onset
                rankTestPval(e,c) = min(curPvals);
            else
                % If we don't have enough time points, the test is
                % unreliable and it's biologically unlikely that the cell
                % starts expressing in this time interval.
                rankTestPval(e,c) = 0.51;
                nonExist(c) = 1;
            end
        else
            nonExist(c) = 1;
        end
    end
    fitCells = fitCells(1:fitIdx);
    % Caluclate the robust fit of a line to all expression values
    [~, stats] = robustfit(1:fitIdx,meanReds(e,fitCells),'fair');
    fitIdx = 0;
    tempWeights=stats.w; % The weights tell us which points fit the line best (1) and worst (0)
    for c = 1:nCells
        if ids(c) > 0
            fitIdx = fitIdx + 1;
            weights(e,c) = tempWeights(fitIdx);
        else
            weights(e,c) = 1.1;
        end
    end
    for c = 1:nCells
        if (weights(e,c) > 0.7 || meanReds(e,c) < 0)%definitely not expressing
            exprStatus(e,c) = 0;
        elseif weights(e,c) < 0.3 %definitely expressing
            exprStatus(e,c) = 1;
        elseif rankTestPval(e,c) < 0.001 %border line weight, traj below p-value cutoff
            exprStatus(e,c) = 1;
        else %border line weight, traj above p-value cutoff
            exprStatus(e,c) = 0;
        end
    end
end