function [ pValMtx, colorVals ] = binaryExpressionToExpressionPvals(exprVals, cells, type, nonExist)
%Takes a matrix of measures of a specific type, the cells measured and the
%type of measurement and returns a matrix of pValues

%Initialize p-value matrix
[nEmbs nCells]=size(exprVals);
pValMtx = zeros(nEmbs, nCells);
colorVals = zeros(nCells, 3, nEmbs);

%Load wild type data for measurement of interest
if strcmp(type,'RW10348')
    load('RW10348probabilities');
elseif strcmp(type,'RW10714')
    load('RW10714probabilities');
elseif strcmp(type,'BV82')
    load('BV82probabilities');
elseif strcmp(type,'RW10434')
    load('RW10434probabilities');
elseif strcmp(type,'RW10425')
    load('RW10425probabilities');
else
    disp(sprintf('Wild type statistics are not compiled for strain %s',type));
    load('NoExpression')
end

%Initialize storage matrix for wild type values
%Row 1 will hold means, and row 2 will hold standard deviations
wtStat=zeros(2,nCells);

for e = 1:nEmbs
    for c = 1:nCells
        %Make sure the cell has a name
        if ~isempty(cells{1,c})
            %Find which lineage it belongs to
            if strcmp('A',cells{1,c}(1))
                baseLen=length(cells{1,c})-1;
                i=1;
                startIdx=1;
                while(i<baseLen)
                    %Figure out what generation of AB cells we are on.
                    startIdx=startIdx+2^(i-1);
                    i=i+1;
                end
                if startIdx>length(ABcycles);
                    error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                end
                i=1;
                suppIdx=0;
                while(i<baseLen)
                    %Find index of this cell within that generation
                    if (strcmp(cells{1,c}(2+i),'p') || strcmp(cells{1,c}(2+i),'r') || strcmp(cells{1,c}(2+i),'v'))
                        suppIdx=suppIdx+2^(baseLen-i-1);
                    end
                    i=i+1;
                end
                %Assign the proper statistics from the loaded data
                if length(ABcycles(2,:)) >= startIdx+suppIdx
                    if exprVals(e,c) == 1
                        if ABcycles{2,startIdx+suppIdx} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if ABcycles{2,startIdx+suppIdx} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                else
                    pValMtx(e,c) = 0.51;
                end
            elseif strcmp('E',cells{1,c}(1))
                if strcmp('EMS',cells{1,c})
                    if exprVals(e,c) == 1
                        if Ecycles{2,1} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Ecycles{2,1} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                else
                    baseLen=length(cells{1,c});
                    i=1;
                    startIdx=2;
                    while(i<baseLen)
                        % Figure out what generation of E cells we are on.
                        startIdx=startIdx+2^(i-1);
                        i=i+1;
                    end
                    if startIdx>length(Ecycles);
                        error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                    end
                    i=1;
                    suppIdx=0;
                    while(i<baseLen)
                        %Find index of this cell within that generation
                        if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                            suppIdx=suppIdx+2^(baseLen-i-1);
                        end
                        i=i+1;
                    end
                    if exprVals(e,c) == 1
                        if Ecycles{2,startIdx+suppIdx} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) = 1;
                    else
                        if Ecycles{2,startIdx+suppIdx} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                end
            elseif strcmp('M',cells{1,c}(1))
                baseLen=length(cells{1,c})-1;
                i=1;
                startIdx=1;
                while(i<baseLen)
                    %Figure out what generation of MS cells we are on.
                    startIdx=startIdx+2^(i-1);
                    i=i+1;
                end
                if startIdx>length(MScycles);
                    error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                end
                i=1;
                suppIdx=0;
                while(i<baseLen)
                    %Find index of this cell within that generation
                    if (strcmp(cells{1,c}(2+i),'p') || strcmp(cells{1,c}(2+i),'r'))
                        suppIdx=suppIdx+2^(baseLen-i-1);
                    end
                    i=i+1;
                end
                if exprVals(e,c) == 1
                    if MScycles{2,startIdx+suppIdx} < 0.01
                        pValMtx(e,c) = 0.0001;
                    else
                       pValMtx(e,c) = 0.3;
                    end
                    colorVals(c,1,e) =  1;
                else
                    if MScycles{2,startIdx+suppIdx} > 0.99
                        pValMtx(e,c) = -0.0001;
                    else
                        pValMtx(e,c) = 0.3;
                    end
                end
            elseif strcmp('C',cells{1,c}(1))
                baseLen=length(cells{1,c});
                i=1;
                startIdx=1;
                while(i<baseLen)
                    %Figure out what generation of C cells we are on.
                    startIdx=startIdx+2^(i-1);
                    i=i+1;
                end
                if startIdx>length(Ccycles);
                    error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                end
                i=1;
                suppIdx=0;
                while(i<baseLen)
                    %Find index of this cell within that generation
                    if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                        suppIdx=suppIdx+2^(baseLen-i-1);
                    end
                    i=i+1;
                end
                if exprVals(e,c) == 1
                    if Ccycles{2,startIdx+suppIdx} < 0.01
                        pValMtx(e,c) = 0.0001;
                    else
                        pValMtx(e,c) = 0.3;
                    end
                    colorVals(c,1,e) =  1;
                else
                    if Ccycles{2,startIdx+suppIdx} > 0.99
                        pValMtx(e,c) = -0.0001;
                    else
                        pValMtx(e,c) = 0.3;
                    end
                end
            elseif strcmp('D',cells{1,c}(1))
                baseLen=length(cells{1,c});
                i=1;
                startIdx=1;
                while(i<baseLen)
                    %Figure out what generation of D cells we are on.
                    startIdx=startIdx+2^(i-1);
                    i=i+1;
                end
                if startIdx>length(Dcycles);
                    error('The average cell cycle length for %s has not been calculated.',cells{1,c});
                end
                i=1;
                suppIdx=0;
                while(i<baseLen)
                    %Find index of this cell within that generation
                    if (strcmp(cells{1,c}(1+i),'p') || strcmp(cells{1,c}(1+i),'r'))
                        suppIdx=suppIdx+2^(baseLen-i-1);
                    end
                    i=i+1;
                end
                if exprVals(e,c) == 1
                    if Dcycles{2,startIdx+suppIdx} < 0.01
                        pValMtx(e,c) = 0.0001;
                    else
                        pValMtx(e,c) = 0.3;
                    end
                    colorVals(c,1,e) =  1;
                else
                    if Dcycles{2,startIdx+suppIdx} > 0.99
                        pValMtx(e,c) = -0.0001;
                    else
                        pValMtx(e,c) = 0.3;
                    end
                end
            elseif (strcmp('P',cells{1,c}(1)) || strcmp('Z',cells{1,c}(1)))
                %P lineage names are a special case so each is compared
                if strcmp('P0',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,1} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,1} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('P1',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,2} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,2} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('P2',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,3} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,3} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('P3',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,4} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,4} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('P4',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,5} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,5} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('Z3',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,6} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,6} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                elseif strcmp('Z2',cells{1,c})
                    if exprVals(e,c) == 1
                        if Pcycles{2,7} < 0.01
                            pValMtx(e,c) = 0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                        colorVals(c,1,e) =  1;
                    else
                        if Pcycles{2,7} > 0.99
                            pValMtx(e,c) = -0.0001;
                        else
                            pValMtx(e,c) = 0.3;
                        end
                    end
                end
            else
                disp(sprintf('Cell %s does not belong to any lineage',cells{1,c}));
            end
        end
    end
end
for e = 1:nEmbs
    for c = 1:nCells
        for i = 1:3
            if colorVals(c,i,e) > 1
                colorVals(c,i,e) = 1;
            elseif colorVals(c,i,e) < 0
                colorVals(c,i,e) = 0;
            end
        end
    end
    for comp = 1:3
        colorVals(logical(nonExist(:,e)),comp,e) = 1;
    end
end
