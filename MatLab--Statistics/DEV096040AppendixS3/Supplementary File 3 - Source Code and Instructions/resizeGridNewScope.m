function M = resizeGridNewScope(M, dx, dy, dz, dd)
%Convert cell positions from pixel units to  microns^3. 
%Voxel size is (0.1,0.1,1) microns^3. The user can specify a 
%different voxel size depending on the microscope.
% dx, dy, dz,dd are optional

n = length(M);
if nargin == 1
    dx = 0.25;
    dy = 0.25;
    dz = 1;
    dd = .25;
end

for i=1:n
    M(i).x = dx * M(i).x;
    M(i).y = dy * M(i).y;
    M(i).z = dz * M(i).z;
    M(i).diameter = dd * M(i).diameter;
    
end

