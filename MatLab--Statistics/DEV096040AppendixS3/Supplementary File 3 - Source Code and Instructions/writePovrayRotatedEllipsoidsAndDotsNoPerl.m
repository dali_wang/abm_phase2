function writePovrayRotatedEllipsoidsAndDotsNoPerl(folderName, fileName, position, inputCells, center, cameraDist, colorMap)

% folderName is the name of the folder where the movie will be saved.
% fileName is the name of the movie to be saved within folderName.
% position is the 3 by number of cells array specifying each cell's
%   placement
% inputCells hold the name of the cells specified in position
% center is a 3 by 1 vector specifying the center of the embryo
% cameraDist specifies the distance of the camera from the embryos, 150 is
%   the default but it will change with imaging parameters.
% colorMap specifies the set of colors to choose from, default is 'rgb'

% Initialize variables
nInputCells = length(inputCells);
color = []; rotation = [];
load('AB32to128standardEllipses_attempt2'); %load wild-typ ellipse positions
load('cellSets32_64_128');
cells = [CELLSAB32 CELLSAB64 CELLSAB128];
nCells = length(cells);
cellPos = zeros(nCells,3);
foundFlag = ones(1,nCells);

% Find input cells in wild-type loaded sets and their position.
for c=1:nCells
    for c2 = 1:nInputCells
        if strcmp(cells(c),inputCells(c2))
            foundFlag(c) = 0;
            cellPos(c,:) = position(c2,:);
        end
    end
end
color = color(~logical(foundFlag));
rotation = -rotation;

% Shift AB32 positions to the left and AB128 positions to the right.
xShift = 62;
lateTranslate = zeros(337,3);
lateTranslate(1:45,1) = - xShift;
lateTranslate(144:337,1) = xShift;
ellipseTransp = 0.65;
rad = 1;

% Initialize the text folder for POV-Ray.
if ~exist(folderName,'dir')
    mkdir(folderName);
else
    delete([folderName '/*.pov'])
    delete([folderName '/*.bmp'])
end

% Set the scene.
cameraVec = [0 cameraDist 0 3];%[0 0 cameraDist 3];
currentFile=fopen([folderName '/rotationFile.pov'],'w');
fprintf(currentFile, '#include "colors.inc"\n#include "textures.inc"\n');
fprintf(currentFile, '#include "shapes.inc"\n#include "glass.inc"\n\n');
fprintf(currentFile, 'background { color rgb<1, 1, 1> }\n\n');

fprintf(currentFile, ['light_source {<' ...
    num2str(center(1, 1) + cameraVec(1), '%.5f') ', ' ...
    num2str(center(1, 2) + cameraVec(2), '%.5f') ', ' ...
    num2str(center(1, 3) + cameraVec(3) , '%.5f') '>' ...
    ' color White \n parallel \n point_at <'...
    num2str(center(1, 1), '%.5f') ', ' ...
    num2str(center(1, 2), '%.5f') ', ' ...
    num2str(center(1, 3), '%.5f') '> } \n\n' ...
    ]);

fprintf(currentFile, ['camera { orthographic location <' ...
    num2str(center(1, 1) + cameraVec(1), '%.5f') ', ' ...
    num2str(center(1, 2) + cameraVec(2), '%.5f') ', ' ...
    num2str(center(1, 3) + cameraVec(3), '%.5f') '>'...
    '\n  sky <0, 1, 0> \n'...
    'look_at <' ...
    num2str(center(1, 1), '%.5f') ', ' ...
    num2str(center(1, 2), '%.5f') ', ' ...
    num2str(center(1, 3), '%.5f') ...
    '> }\n\n']);

fprintf(currentFile, 'global_settings { max_trace_level 250 } \n');
depthVector = 0.8*[ 1.00  0.75  0.66 0.50  0.33 0.25  0.10  0.05    ];
depth=0;
transp = 0.2;

%Set up the color vector choices.
if strcmp(colorMap,'rgb')
    rgbTemplate = ...%[0.5*ones(15,1) linspace(0.5, 0, 15)'  linspace(0.5, 0, 15)' transp*ones(15,1)]
        [depthVector(1) 0.21 depthVector(4) transp; ...%1 pink
        depthVector(1) depth depth transp; ...%2 red
        depthVector(2) depthVector(4) depth transp; ...%3 orange
        depthVector(2) depthVector(2) depth transp; ...%4 yellow
        depth depthVector(2) depth transp; ...%5 green
        depth depthVector(4) depthVector(2) transp; ...%6 cyan
        depthVector(5) depth depthVector(3) transp;...%7 purple
        depthVector(8) depthVector(8) depthVector(2) transp;...%8 blue
        0.5 0.5 0.5 transp; ...%9 gray
        depth depthVector(5) depth transp; ...%10 dark green
        0.10 0.10 0.10 transp; ...%11 black
        depthVector(4) depth depth transp; ...%12 maroon
        depth depth depthVector(6) transp; ...% 13 navy blue
        depthVector(4) 0.2 depthVector(7) transp;...%1.00 1.00 1.00 0.75];%14 transparent
        0 0 0 transp]; %15 black
elseif strcmp(colorMap,'rb')
    rgbTemplate = zeros(100,4);
    for c = 1:100
        if 0.95-0.1*floor(c/10)-0.1*rem(c-1,10)<0
            transp = 0;
        else
            transp = 0.95-0.1*floor(c/10)-0.1*rem(c-1,10);
        end
        rgbTemplate(c,:) = [0.1*floor(c/10) 0 0.1*rem(c,10) transp];
    end
end

% Write the standard ellipses and dots for position in this embryo.
nCells = length(color);
for c=1:nCells
    if color(c)>0
        currentColor=rgbTemplate(color(c),:);
        %write ellipse
        fprintf(currentFile, ['sphere { <0 0 0> 1.0 ' ...
            'scale <' num2str(newStd(c,1),'%.5f') ', ' num2str(newStd(c,3),'%.5f') ', ' num2str(newStd(c,2),'%.5f') '> ' ...
            'rotate < 0, ' num2str(rotation(c,3), '%.5f') ', 0> ' ... % rotate angle 1 about the z axis
            'rotate < ' num2str(rotation(c,2), '%.5f') ', 0, 0> ' ... % rotate angle 2 about the x axis
            'rotate < 0, ' num2str(rotation(c,1), '%.5f') ', 0 > ' ... % rotate angle 3 about the z axis
            'translate < ' num2str(means(c,1),'%.5f') ', ' num2str(means(c,3),'%.5f') ', ' num2str(means(c,2),'%.5f') '> ' ...
            'rotate < 360*clock, 0, 0> ' ...
            'translate < ' num2str(lateTranslate(c,1),'%.5f') ', ' num2str(lateTranslate(c,3),'%.5f') ', ' num2str(lateTranslate(c,2),'%.5f') '> ' ...
            'texture { pigment { rgbt<' num2str(currentColor(1, 1), '%.5f') ', ' num2str(currentColor(1, 2), '%.5f') ', ' ...
            num2str(currentColor(1, 3), '%.5f') ', ' num2str(ellipseTransp, '%.5f') '> }' ...
            ' finish {ambient 1.0 diffuse 0.2 specular 0.2} } }\n\n']);
        
        % Write sphere.
        fprintf(currentFile, ['sphere { <0 0 0>, 1.0 scale <' num2str(rad,'%.5f') ...
            ', ' num2str(rad,'%.5f') ', ' num2str(rad,'%.5f') '> ' ...
            'translate < ' num2str(cellPos(c,1),'%.5f') ', ' num2str(cellPos(c,3),'%.5f') ', ' num2str(cellPos(c,2),'%.5f') '> ' ...
            'rotate < 360*clock, 0, 0>'...
            'translate < ' num2str(lateTranslate(c,1),'%.5f') ', ' num2str(lateTranslate(c,3),'%.5f') ', ' num2str(lateTranslate(c,2),'%.5f') '> ' ...
            'texture { pigment { rgbt<' num2str(currentColor(1, 1), '%.5f') ', ' num2str(currentColor(1, 2), '%.5f') ', ' ...
            num2str(currentColor(1, 3), '%.5f') ', ' num2str(currentColor(1, 4), '%.5f') '> }' ...
            ' finish {ambient 1.0 diffuse 0.2 specular 0.2} } }\n\n']);
        
    end
end
fclose(currentFile);

% Set image size.
width = '800';
height = '600';

% Render individual frames in POV-Ray.
currentDir = pwd;
system([currentDir '\POV-Ray\bin\pvengine ' ... %call POV-Ray
    '+I' folderName '\rotationFile.pov ' ... %input file
    '+O' folderName '\rotatingView.bmp ' ... %outputFile
    '+KFI0 +KFF72 +W' width ' +H' height ' -GA +L' currentDir '\POV-Ray\bin\include']);

% Save movie.
system([currentDir '\EasyBMPtoAVI\EasyBMPtoAVI '...
    '-start ' folderName ' \rotatingView01.bmp ' ...
    '-end ' folderName '\rotatingView72.bmp ' ...
    '-framerate 7 -output ' fileName]);

% Delete individual frames.
delete([folderName '/*.bmp'])

