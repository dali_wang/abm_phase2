function renderArrowsAB32to128clusterColor(paths,cells,folderName,fileName,cameraDist,largeSizeFlag)
%if output is desired [first, last]=renderArrowsAB32to128clusterColor(paths,cells,folderName,fileName,views,cameraDist,largeSizeFlag)
% MT is the loaded information for each embryo (from loadNucleiNewData).
% cells is a cell array with all the cell names to be examined.
% folderName is where the movie will be saved.
% fileName is the name of the file that will be saved within folderName.
% cameraDist is how far from the nuclei that the camera should be
%   positioned.  This will vary based on the imaging parameters.

% First is where all the cells are born, last is where the cells divide.

% Initialize variables
nAllCells = length(cells);
load('cellSets32_64_128');
showCells = [CELLSAB32 CELLSAB64 CELLSAB128];
nShowCells = length(showCells);
first=zeros(nShowCells,3);
last=zeros(nShowCells,3);
colors=zeros(nShowCells,1);


for cAll=1:nAllCells
    for cShow = 1:nShowCells
        if strcmp(cells{cAll},showCells{cShow})
            if ~isempty(paths{cAll})
                 % Identify the coordinates for each cell
                len = length(paths{cAll}(:,1));
                
                % Record the position of the cell when it is first born.
                first(cShow,1) = paths{cAll}(1,1);
                first(cShow,2) = paths{cAll}(1,3);
                first(cShow,3) = paths{cAll}(1,2);
                
                % Record the position of the cell just before it divides or dies.
                last(cShow,1) = paths{cAll}(len,1);
                last(cShow,2) = paths{cAll}(len,3);
                last(cShow,3) = paths{cAll}(len,2);
            end
        end
    end
end

% Cluster the cells into groups for each stage and assign colors based on
% which group each cell belongs to.
bandwidth = [13.5 13 11.5];
startIdx = [1 46 143];
endIdx = [45 142 337];
for g = 1:3
    curData = [first(startIdx(g):endIdx(g),:) last(startIdx(g):endIdx(g),:) - first(startIdx(g):endIdx(g),:)] ; 
    [~,colors(startIdx(g):endIdx(g)),~] = MeanShiftCluster(curData',bandwidth(g));
    for c = startIdx(g):endIdx(g)
        if colors(c) > 15
            colors(c) = rem(colors(c),15);
            if colors(c) == 0
                colors(c) = 1;
            end
        end        
    end
end

% Shift the AB32 and AB128 sizes to the left and right.
xMax = max( [ max(first(:,1)) max(last(:,1)) ]);
xMin = min( [ min(first(:,1)) min(last(:,1)) ]);
xRange = xMax - xMin;
if xRange > 62
    xShift = xRange;
else
    xShift = 62;
end
first(1:45,1) = first(1:45,1) - xShift;
last(1:45,1) = last(1:45,1) - xShift;
first(144:337,1) = first(144:337,1) + xShift;
last(144:337,1) = last(144:337,1) + xShift;
center=[0 0 0];

% Create the movie.
if exist('largeSizeFlag','var')
    if largeSizeFlag == 1
        specifySphereSweepNoPerl(folderName,fileName,first,last,colors,center,cameraDist,largeSizeFlag)
    end
else
    specifySphereSweepNoPerl(folderName,fileName,first,last,colors,center,cameraDist,largeSizeFlag)
end

