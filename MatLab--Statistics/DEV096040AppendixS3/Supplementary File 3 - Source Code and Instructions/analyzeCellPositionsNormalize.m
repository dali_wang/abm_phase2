function [measures, paths, axisSizes, MT] = analyzeCellPositionsNormalize(MT, cells, ids, ind, daughterInfo, timeResolution, normalizeFlag)
% given:
%   MT - loaded cell info 
%   cells - list of cells to be examined
%   ind - indices where each cell starts
%   ids - cell ID of each cell
%   daughterInfo - 0 if the cell's daughters are present, 1 if not
% return
%   position - number of cells by 3 x, y and z positions at fifth minute
%   displacement - number of cells by 3 x, y and z displacement
%   spherical - number of cells by 3 spherical coordinates of displacement

nCells = length(cells);
position = zeros(nCells,3);
displacement = zeros(nCells,3);
totalDist = zeros(nCells,1);
paths = cell(1,nCells);

axisSizes = zeros(1,3);
lenMT = length(MT.value);
minIdx = round(lenMT/100);
maxIdx = minIdx*99;
sortedPositions = zeros(3,lenMT);
sortedPositions(1,:) = sort([MT.value(:).x]);
sortedPositions(2,:) = sort([MT.value(:).y]);
sortedPositions(3,:) = sort([MT.value(:).z]);
for comp = 1:3
    axisSizes(comp) = sortedPositions(comp,maxIdx) - sortedPositions(comp,minIdx);
end
if exist('normalizeFlag','var') 
    if normalizeFlag == 1
        load('embryoSizeStats'); %brings up meanSizes and stdSizes
        nEntries = length(MT.value);
        for c = 1:nEntries
            % Normalize positions
            MT.value(c).x = MT.value(c).x/axisSizes(1)*meanSizes(1);
            MT.value(c).y = MT.value(c).y/axisSizes(2)*meanSizes(2);
            MT.value(c).z = MT.value(c).z/axisSizes(3)*meanSizes(3);
        end
    end
end

% Find the number of time points that is about 5 minutes because this is
% when we examine the cell position.
fiveMins = round(300/timeResolution);

for c=1:nCells
    if ids(c)~=-1
        % Obtain all the coordinates for each cell
        [x y z] = getCoordinates(MT.value, ind(ids(c)):ind(ids(c)+1)-1);
        paths{1,c} = [x' y' z'];
        len = length(x);
        if len >= fiveMins
            position(c,:) = paths{1,c}(fiveMins,:);
        end
        if daughterInfo(c) == 0
            % If we have seen the cell division, record its displacement.
            displacement(c,:) = paths{1,c}(len,:) - paths{1,c}(1,:);
            totalDist(c) = sqrt(displacement(c,1)*displacement(c,1) + ...
                displacement(c,2)*displacement(c,2) + displacement(c,3)*displacement(c,3));
        end
    end
end

measures = [position' ; displacement'; totalDist';];


