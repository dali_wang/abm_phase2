function slidingWindowIceCreamConesNoPerl(paths, birthAndDeath, cells, windowSize, folderName, fileName, tStart, tEnd)
% Make povray diagrams of the migration that occurs over time periods of
% window size

% paths is a cell array with each entry holding every time point of movement
%   for one cell.
% birthAndDeath is a number of cells by 2 array with each row corresponding
%   to one cell's start and end point.
% cells holds the names of cells to be examined.
% windowSize holds the number of time points to be examined in each image.
% folderName is the name of the folder where the movie will be saved.
% fileName is the name of the movie to be saved within folderName.
% tStart is the first time point to be examined.
% tEnd is the last time point to be examined.
nCells = length(cells);

for t = tStart:1:tEnd
    % Initialize variables
    moms = zeros(1,128);
    momIdx = 0;
    cones = zeros(700,6);
    color = zeros(1,700);
    coneIdx = 0;
    for c = 1:nCells
        if ~isempty(paths{c})
        colorFlag = 0;
        
        if birthAndDeath(c,1) <= t && birthAndDeath(c,2) > t + windowSize
            % Is this cell alive throughout this whole time range?
            coneIdx = coneIdx + 1;
            colorFlag = 1;
            tCur = round(t - birthAndDeath(c,1) + 1);
            cones(coneIdx, :) = [paths{c}(tCur,:) paths{c}(round(tCur + windowSize),:)];
        elseif birthAndDeath(c,1) <= t &&  birthAndDeath(c,2) > t && birthAndDeath(c,2) <= t + windowSize
            % Is this cell alive at the beginning of the time range but dividing or dying before the end?
            momIdx = momIdx + 1;
            moms(momIdx) = c;
            coneIdx = coneIdx + 1;
            colorFlag = 1;
            tCur = round(t - birthAndDeath(c,1) + 1);
            len = length(paths{c}(:,1));
            cones(coneIdx, :) = [paths{c}(tCur,:) paths{c}(len,:)];
        elseif birthAndDeath(c,1) > t &&  birthAndDeath(c,1) < t + windowSize
            % Is the cell born during this time range?
            lenCur = length(cells{1,c});
            colorFlag = 1;
            lastIdx = round(t + windowSize - birthAndDeath(c,1));
            if lastIdx > length(paths{c}(:,1))
                lastIdx = length(paths{c}(:,1));
            end
            if lastIdx ~= 0
                % If so find its mother cell.
                coneIdx = coneIdx + 1;
                for cMom = 1:momIdx
                    if strcmp(cells{1,c}(1:lenCur - 1),cells{1,moms(cMom)})
                        len = length(paths{moms(cMom)}(:,1));
                        cones(coneIdx,1:3) = paths{moms(cMom)}(len,:);
                    end
                end
                if cones(coneIdx,1) ~= 0 || cones(coneIdx,2) ~= 0 || cones(coneIdx,3) ~= 0
                    cones(coneIdx,4:6) = paths{c}(lastIdx,:)';
                else
                    cones(coneIdx,:) = [paths{c}(1,:) paths{c}(lastIdx,:)];
                end
            end
        end
        
        % Assign color based on lineage group.
        if colorFlag == 1;
            curCell = cells{c};
            lenName=length(curCell);
            if lenName>=5
                if strcmp(curCell(1:5),'ABala')
                    color(coneIdx) = 3; %orange
                elseif strcmp(curCell(1:5),'ABalp')
                    color(coneIdx) = 6;%2;
                elseif strcmp(curCell(1:5),'ABara')
                    color(coneIdx) = 7;%3;
                elseif strcmp(curCell(1:5),'ABarp')
                    color(coneIdx) = 4;
                elseif strcmp(curCell(1:5),'ABpla')
                    color(coneIdx) = 2;%5;
                elseif strcmp(curCell(1:5),'ABplp')
                    color(coneIdx) = 8;%6;
                elseif strcmp(curCell(1:5),'ABpra')
                    color(coneIdx) = 12;%7;
                elseif strcmp(curCell(1:5),'ABprp')
                    color(coneIdx) = 13;%8;
                end
            end
            if strcmp(curCell(1),'M')
                if length(curCell)>2
                    if strcmp(curCell(3),'a')
                        color(coneIdx) = 5;
                    else
                        color(coneIdx) = 10;
                    end
                else
                    color(coneIdx) = 5;%14;
                end
            elseif strcmp(curCell(1),'E')
                color(coneIdx) = 1;%9; %magenta
            elseif strcmp(curCell(1),'C')
                color(coneIdx) = 14;%10;
            elseif strcmp(curCell(1),'D')
                color(coneIdx) = 9;%12;
            elseif strcmp(curCell(1),'P') || strcmp(curCell(1),'Z')
                color(coneIdx) = 11;%13;
            end
        end
        end
    end
    
    % Cut vector to size of number of cells.
    color = color(1:coneIdx);
    tempCones = cones(1:coneIdx,:);
    
    % Rearrange order of variables to reflect movie setup.
    cones = zeros(coneIdx,6);
    cones(:,1) = tempCones(:,1);
    cones(:,2) = tempCones(:,3);
    cones(:,3) = tempCones(:,2);
    cones(:,4) = tempCones(:,4);
    cones(:,5) = tempCones(:,6);
    cones(:,6) = tempCones(:,5);
    cones = cones(1:coneIdx,:);
    % Write povray files
    center = [0 0 0];
    cameraDist = 50;
    specifySphereSweepSlidingWindowNoPerl(folderName,cones(:,1:3),cones(:,4:6),color,center,cameraDist,t);
end

% Save movie.
  currentDir = pwd;
  system([currentDir '\EasyBMPtoAVI\EasyBMPtoAVI '...
    '-start ' folderName '\timePoint_' num2str(tStart,'%03d') '.pov ' ...
    '-end ' folderName '\timePoint_' num2str(tEnd,'%03d') '.bmp ' ...
    '-framerate 7 -output ' fileName]);

% Delete individual frames.
delete([folderName '/*.bmp'])
