function [Mt] = readnucleiDirWithRed(dir, tEnd, aspectRatio)
% loads a 'nuclei' directory. Sets the embryo in a canonical orientation.
% Selects AB4 to AB64 and the corresponding C,D,E,MS,P & Z lineages.
% Mt contains cell xyz data, embryo is centered & oriented so a-p 
% axis is aligned with x axis (anterior is on the negative side of the axis), 
% d-v is aligned with y axis (dorsal is on negative side).
% cells are given a unique index #.
%
% stats are statistics on cell paths.
% 
% Usage -
%   [Mt081305, stats1] = readnucleiDir('data/081305')
%
% Input -
%dir        - the directory the nuclei file are located
%truncate   - should the initial/last few time point be tuncated. By
%             default true.


M  = readnucleiDirHelperWithRed(dir,max(tEnd));
% try

% M = Rotate(M, pi, 0, 0);

% xMax=max([M.x]);
% yMax=max([M.y]);
% if yMax>xMax
%     Mtemp=M;
%     lenM=length(M);
%     for c=1:lenM
%         Mtemp(c).x=M(c).y;
%         Mtemp(c).y=M(c).x;
%     end
%     M=Mtemp;
% end

fprintf (' Extracting AB,C,D,E,MS,P,Z\n');
%truncate initial 4 cells and polar bodies

[M, resABa] = getLineage(M, 'ABa', 1, 256, tEnd(1));
[M, resABp] = getLineage(M, 'ABp', 1, 256, tEnd(2));
[M, resMS] = getLineage(M, 'MS', 1, 128, tEnd(3));
[M, resE] = getLineage(M, 'E', 1, 32, tEnd(4));
[M, resC] = getLineage(M, 'C', 1, 64, tEnd(5));
[M, resD] = getLineage(M, 'D', 1, 16, tEnd(6));
[M, resP] = getLineage(M, 'P', 1, 4, tEnd(6));
[M, resZ] = getLineage(M, 'Z', 1, 2, tEnd(6));
resEMS = ~cellfun(@isempty, regexpi([M.name], '\<EMS\>')) ;

Mt = M(resABa | resABp | resC | resD | resE |( resMS & ~resEMS)| resP | resZ);

[tx ty tz] = getRotationNewDataVersionPC(Mt,min(tEnd));%getRotationNewDataVersion2(M);
% catch
%     warning('error retrieving orientation');
%     tx = 0;
%     ty = 0;
%     tz = 0;
% end
Mt = center(Mt,min(tEnd));
Mt = Rotate(Mt,  0,  0, tz);
if tx>0 || ty>0
   Mt = Rotate(Mt, tx, ty,  0);
end


Mt = traceRoute(Mt);
Mt = resizeGridNewScope(Mt, aspectRatio, aspectRatio, 1, aspectRatio);
% stats = getRouteStats(Mt);
end

function M = Rotate(M, tx, ty, tz)
    disp(sprintf('rotating around tx %f, ty %f, tz %f \n',tx,ty,tz));
    Rot = makehgtform('xrotate',tx,'yrotate',ty,'zrotate',tz);
    Rot = Rot(1:3,1:3);
    M = applyRotation(M, Rot);
end

function [M, res] = getLineage(M, cellnm, ncellstFrom, ncellstTo, tEnd)

cellnm = lower(cellnm);
sregexp = sprintf('\\<%s\\w{%d,%d}\\>',cellnm, log2(ncellstFrom), log2(ncellstTo));


sexpected = 2*ncellstTo-ncellstFrom;
n = sum(~cellfun(@isempty, regexpi(unique([M.name]),sregexp ))) ;
switch cellnm %Since EMS will be picked we will omit it on count
    case 'e'
        n = n-1;
end

resName = ~cellfun(@isempty, regexpi([M.name], sregexp));
resTime=[M.t]<=tEnd; %Find all the cells less than or equal to the ending time
resLastTime=[M.t]==tEnd; %Find all the cells equal to the ending time
resSetToZero= find(resLastTime & resName);%Find all cells of this lineage at the last time
lenLast=length(resSetToZero); 
for c=1:lenLast
    %set the successor of the cells present at the last time point to zero
    M(resSetToZero(c)).successor=0;
end
res=resName & resTime;
fprintf('%s cells found: %d out of %d\n',upper(cellnm),n, sexpected);
%if (n - sexpected ~= 0)
%    warning([upper(cellnm) ' cells found and expected differ']);
%end


end
