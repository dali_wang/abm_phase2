function C  = correlationMatrixJLMweighted(M,cells,type,nTruncate,lag)
% Calculates a correlation matrix. or different type
% of properties one can calculate between two cells. 
% for instance one can calculate the rmsd between two
% paths (specify 'rmsd' in type), or more naturaly the
% correlation coefficient of 2 paths in 3D ('cosCentered'). Individual
% correlation coponents are summed to yield the cross correlation
%
%Input:
%type - 'cos' correlation between paths [-1,1]
%         'cosV' correlation between velocities
%         'cosMtxNorm' correlation between paths [-1,1] using matrix norms
%         'velocity' the velocities between pairs of cells
%nTruncate - number of time steps that will be discarded at the beginning
%of a cell's trajectory
%lag - lagging offset to cell 1 (id1) , so lag = +1 would shift time points
%of id1 to T+1
%
%Example :
%   C0 = cosineMatrix(M,'cosine',  0)
%   plotCorrMtx(C0,cells, 'lower,bw');%
%

if nargin < 5
    lag = 0;
end

% ids = [name2id(M, 'AB......')';
%     name2id(M, 'ms...')';name2id(M, 'ms....')';
%     name2id(M, 'ea.')';name2id(M, 'ep.')';
%     name2id(M, 'c...')';
%     name2id(M, 'd.')';name2id(M, 'd..')';
%   ];



ids = name2id(M, cells);

[scellid,IX] = sortrows([[M.routid]' , [M.t]'],[1 2] );%scellid lists the routids and time points each cell is alive for, IX telss you how they sorted rows correspond to the original M
[routecell ind] = unique(scellid(:,1),'first');%gives the first entry in M that corresponds to each id
ind = [ind' ,length(M)+1];%changes id vector to a row vector and ads the final index so each entry can be used as a range for each cell
M = M(IX);%resort M according to the order from sorted rows (could this give us trouble depending which row is changed first?)
n = length(ids);
C = zeros(n);
% C(:) = nan;
% CX = C;
% CY = C;
% CZ = C;
for  i=1:n
    if ids(i) > 0
    ids1 = ind(ids(i))+nTruncate:ind(ids(i)+1)-1;
    %ids1 = ids1(~[M(ids1).truncate]) ;
%     dispprecentage(i , n, 5)

    for j=i:n
        if ids(j) > 0
        ids2 = ind(ids(j))+nTruncate:ind(ids(j)+1)-1;
        %ids2 = ids2(~[M(ids2).truncate]) ;
        [t, i1, i2] = intersect([M(ids1).t]+ lag,[M(ids2).t] );%returns the indicies WITHIN ids1 and ids2 where both cells are alive
        if (~isempty(t))
            switch (type)
                case 'cos'
                    [cx cy cz] = cos(M, ids1(i1),  ids2(i2));
                    c = cx +cy +cz ;
                case 'cosCentered'
                    c = cosCentered(M, ids1(i1),  ids2(i2));
                   
                case 'cosOrigin'
                    [cx cy cz] = cosOrigin(M, ids1(i1),  ids2(i2));
                    c = cx +cy +cz ;
                case 'cosV'
                    [cx cy cz] = cosV(M, ids1(i1),  ids2(i2));
                    c = cx +cy +cz ;
                case 'cosMtxNorm'
                    c = cosMtxNorm(M, ids1(i1),  ids2(i2));
                case 'velocity'
                    c = velocity(M, ids1(i1),  ids2(i2));
                case 'velocityContact'
                    c = velocityContact(M, ids1(i1),  ids2(i2));
                case 'varR'
                    c = varR(M ,ids1(i1),  ids2(i2));
                case 'rmsd'
                    c = rmsd(M, ids1(i1),  ids2(i2));
                 case 'maxdist'
                    c = maxdist(M ,ids1(i1),  ids2(i2));
                 case 'overlap'
                    c = length(t);
                    
                otherwise
                    error('type not recognized, I know only : cos, cosCentered,cosOrigin, cosV, cosMtxNorm, velocity, rmsd, maxdist');
            end

            C(i,j)=c ;
            C(j,i)= C(i,j);
        end
        end
    end
    end
end

% fprintf('\n')

end


function  [cx cy cz] = cos(M ,ids1,ids2)
%correlation of movement between 2 cells
%
cx = [nan]; cy = [nan]; cz = [nan];

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);

cx = my_cos(x1,x2);
cy = my_cos(y1,y2);
cz = my_cos(z1,z2);
end




function  [cx cy cz] = cosOrigin(M ,ids1,ids2  )
%correlation of movement between 2 cells
%
cx = [nan]; cy = [nan]; cz = [nan];

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
%d1 = sqrt((x1(1)-x1(end))^2+(y1(1)-y1(end))^2+(z1(1)-z1(end))^2);
%d2 = sqrt((x2(1)-x2(end))^2+(y2(1)-y2(end))^2+(z2(1)-z2(end))^2);
%if  d1 > 4 & d2 > 4
cx = my_cos(x1-x1(1),x2-x2(1));
cy = my_cos(y1-y1(1),y2-y2(1));
cz = my_cos(z1-z1(1),z2-z2(1));

end


function  c = cosCentered(M ,ids1,ids2  )
%correlation of movement between 2 cells
%
[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
%d1 = sqrt((x1(1)-x1(end))^2+(y1(1)-y1(end))^2+(z1(1)-z1(end))^2);
%d2 = sqrt((x2(1)-x2(end))^2+(y2(1)-y2(end))^2+(z2(1)-z2(end))^2);
%if  d1 > 4 & d2 > 4
if length(x1)>5
    [cx xmag] = my_weight_cos(x1-mean(x1),x2-mean(x2));
    [cy ymag] = my_weight_cos(y1-mean(y1),y2-mean(y2));
    [cz zmag] = my_weight_cos(z1-mean(z1),z2-mean(z2));
    allMag=xmag+ymag+zmag;
    c = cx*xmag/allMag + cy*ymag/allMag + cz*zmag/allMag;
else
    c=0;
end

end


function  c = rmsd(M ,ids1,ids2  )
%correlation of movement between 2 cells
%


[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
X = [x1-mean(x1) ;y1-mean(y1) ;z1-mean(z1)]';
Y = [x2-mean(x2) ;y2-mean(y2) ;z2-mean(z2)]';
c = sqrt(abs(sum(sum(X'.^2))+sum(sum(Y'.^2))-2*sum(svd(X'*Y)))/length(X));

end



function  [cx cy cz] = cosV(M ,ids1,ids2  )
%correlation of movement between 2 cells
%
cx = [nan]; cy = [nan]; cz = [nan];
if length(ids1) < 3
    return;
end


[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);

cx = my_cos(diff(x1),diff(x2));
cy = my_cos(diff(y1),diff(y2));
cz = my_cos(diff(z1),diff(z2));

end

function  c = cosMtxNorm(M ,ids1,ids2  )
%correlation of movement between 2 cells
%
cx = [nan]; cy = [nan]; cz = [nan];

X1 = getCoordinates(M, ids1);
X2 = getCoordinates(M, ids2);

c = norm(X1'*X2, 2)/(norm(X1'*X1, 2)*norm(X2'*X2, 2))

end

function  slope = velocityContact(M ,ids1,ids2  )
%distance of movement between 2 cells
%
CELL_DIAMETER = 8;
slope = nan;
%d = nan;
if length(ids1 ) < 4
    return;
end

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
d = sqrt((x1-x2).^2+(y1-y2).^2+(z1-z2).^2);
if abs(mean(d)-CELL_DIAMETER)/CELL_DIAMETER >.1
    slope = nan;
else
    a  = polyfit(1:length(d),d,1);
    slope = a(1);
end


end


function  slope = velocity(M ,ids1,ids2  )
%distance of movement between 2 cells
%
slope = nan;
%d = nan;
if length(ids1 ) < 4
    return;
end

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
d = sqrt((x1-x2).^2+(y1-y2).^2+(z1-z2).^2);
a  = polyfit(1:length(d),d,1);
slope = a(1);

end


function  s = varR(M ,ids1,ids2  )
%variance of distance of movement between 2 cells
%
s = nan;
%d = nan;

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
d = sqrt((x1-x2).^2+(y1-y2).^2+(z1-z2).^2);
s = var(d);

end

function  md = maxdist(M ,ids1,ids2  )
%distance of movement between 2 cells

[x1 y1 z1] = getCoordinates(M, ids1);
[x2 y2 z2] = getCoordinates(M, ids2);
md = max(sqrt((x1-x2).^2+(y1-y2).^2+(z1-z2).^2));

end

%matlabpool(2);

%used for debugging
%fprintf('%s , %s\n',id2name(M,(ids(i))), id2name(M,(ids(j))) );
%id2name(M,(ids(i))), id2name(M,(ids(j)))


% c_ = [name2id(M, 'ABal....')';name2id(M, 'ABpl....')';name2id(M, 'ABar....')';name2id(M, 'ABpr....')';
%     name2id(M, 'c...')';
%     name2id(M, 'd.')';name2id(M, 'd..')';
%     name2id(M, 'ea.')';name2id(M, 'ep.')';
%     name2id(M, 'ms...')';name2id(M, 'ms....')';];
