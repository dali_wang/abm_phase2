To run the simulation file you will need the following:
	NetLogo 3D
	Current Version of NetLogo file: WormSimCurr.nlogo3d
	Nuclei Files: named 1mod.txt, 2mod.txt, etc
	Division Direction file: DivisionDirWQ.txt
	Time Table: ABM_time_tableWQ.txt

First Open NetLogo file in NetLogo 3-D
This has default paths set and some default variables declared.
You will need to change the paths to point to where you have downloaded the specified files. For the initFile 1mod.txt is usually used.

nucleiInputFolder: specifies the folder containing the Nuclei Files.

nucleiOutputFolder: specifies where to save the new nuclei files made as the simulation runs. These are created at intervals specified by the outTimeResolution

outputDisplacementFile: points to where you want to create a new output file for outputting displacement information

outTimeResolution: specifies how often, in seconds, to create new nuclei files.

setup-normal: sets the simulation up to be run.

go: starts the simulation and runs until "untilTick" (seconds)

cellFocusBool: determines if simulation will use cell focusing while determining migration patterns -- takes a long time to run

dataSet(not currently in use): used to denote different dataSets
