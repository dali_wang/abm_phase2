Scaling factor 0.97
Error from robus fitting 1.46
Embryo is scalable.
Ratio to wild type division timing means by lineage group 
AB 0.01
MS 0.01
E 0.05
C 0.04
D 0.06

All ratio to wild type division timing generation means 
AB41.00
AB8 0.99
AB16 0.99
AB32 1.01
AB64 1.02
MS 0.00
MS2 0.00
MS4 1.01
MS8 0.98
MS16 1.02
E 1.00
E2 1.04
E4 0.00
C 0.00
C2 0.95
C4 1.04
C8 1.06
D 0.00
D2 0.96
D4 0.94

Absolute generation means division time
AB4 14.75
AB8 32.00
AB16 56.94
AB32 85.09
AB64 120.50
MS 0.00
MS2 0.00
MS4 21.00
MS8 42.00
MS16 67.75
E 97.88
E2 134.69
E4 0.00
C 0.00
C2 23.00
C4 62.50
C8 106.00
D 0.00
D2 26.00
D4 51.00

