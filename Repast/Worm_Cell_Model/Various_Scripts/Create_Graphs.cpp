#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

using namespace std;

int main()
{
    ofstream output_file;
	ifstream input_file;
	int num_of_lines;
	string dummy_line, extra_text;

	output_file.open("/home/gvanloo_15/ABM/In_Prog_Code/Various_Scripts/Graph.csv", ofstream::out);

	for(int num_of_files = 1; num_of_files <= 160; num_of_files++)
	{
		if(num_of_files >= 100)
			extra_text = "";
		else if(num_of_files >= 10)
			extra_text = "0";
		else
			extra_text = "00";

		input_file.open( ("/home/gvanloo_15/ABM/In_Prog_Code/Text_Files/nuclei/t" + extra_text + to_string(num_of_files) + "-nuclei.txt").c_str(), ifstream::in);
		num_of_lines = 0;

		if(input_file.is_open())
		{
			while(!input_file.eof())
			{
				getline(input_file, dummy_line);
				num_of_lines++;
			}
		}


		output_file << num_of_lines << endl;
		input_file.close();
	}
	output_file.close();
}
