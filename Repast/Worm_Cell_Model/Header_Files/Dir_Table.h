#ifndef Dir_Table_h
#define Dir_Table_h

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

/*
 * DIR TABLE - USED TO STORE THE INFORMATION FROM THE DIRECTION FILE AND SEARCH THE TABLE
 */

class Dir_Table
{
	public:
		Dir_Table(){};
		~Dir_Table(){};
		void Initialize_With_File(std::string);
		std::string Remove_Parenthesis(std::string);
		void Search_Table_Division(std::string, std::string&, std::string&, double&, double&, double&, bool&);
		void Search_Table_Daughter_Of(std::string, int, std::string&, bool&);
	private:
		struct direction_table
		{	
			std::string parent, daughter_1, daughter_2;
			double x_dir_vec, y_dir_vec, z_dir_vec;
		};
		std::vector<direction_table> table;
};
#endif
