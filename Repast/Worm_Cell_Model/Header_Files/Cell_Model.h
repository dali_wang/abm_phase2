#ifndef Cell_Model_h
#define Cell_Model_h

#include <string>
#include <iostream>
#include <math.h>
#include <fstream>
#include <boost/mpi.hpp>
#include "Time_Table.h"
#include "Dir_Table.h"
#include "Mig_Table.h"
#include "Cell.h"
#include "Cell_Agent_Provider.h"
#include "Cell_Agent_Receiver.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/Schedule.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/SharedContinuousSpace.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/GridComponents.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Moore2DGridQuery.h"

/*
 * CELL MODEL - USED TO RUN SIMULATION
 */

class Cell_Model
{
	private:
		Dir_Table direction_info_table;
		Time_Table time_info_table;
		Mig_Table migration_info_table;
		Cell_Agent_Provider *cell_provider;
		Cell_Agent_Receiver *cell_receiver;
		double div_cycle_time, migStdDev;
		int max_time_file_reads, num_of_cells, times_accessed_file, id, time_resolution, time_limit, init_file_number;
		bool cell_focus_switch;
		std::string mod_file_dir, displacement_file, nuclei_file_dir, initial_cell_file;
		repast::SharedContext<Cell> cell_context;
		repast::SharedContinuousSpace<Cell, repast::WrapAroundBorders, repast::SimpleAdder<Cell> > *cont_ellipsoid;
		repast::SharedDiscreteSpace<Cell, repast::WrapAroundBorders, repast::SimpleAdder<Cell> > *dis_ellipsoid;

		const bool NEW_CELL = true;
		const bool OLD_CELL = false;
		const bool CAN_DIVIDE = true;
		const bool CANNOT_DIVIDE = false;
		const bool CANNOT_MOVE = false;
		const bool CAN_MOVE = true;
		const bool CAN_USE = true;
		const bool CANNOT_USE = false;
		const double X_SCALE = 0.087;
		const double Y_SCALE = 0.087;
		const double Z_SCALE = 0.504;
		const double SIZE_SCALE = 0.087;
		const double SIZE_MULTI = 0.87;
		const double NO_DIV_CYCLE = 40.0;
		const double ORIGIN_X = -50.0;
		const double ORIGIN_Y = -50.0;
		const double ORIGIN_Z = -50.0;
		const double GRID_LENGTH = 100.0;
		const double GRID_HEIGHT = 100.0;
		const double GRID_DEPTH = 100.0;
		const int DIS_BUFFER_AREA = 2;
		const int CONT_BUFFER_AREA = 0;
		const int SCAN_DISTANCE = 1;
		const int CELL_TYPE = 0;
		const int DUMMY_TYPE = 1;

	public:
		Cell_Model(std::string, int, char**, boost::mpi::communicator*); 
		~Cell_Model();
		Cell* Create_Cell();
		void Generate_Cells();
		void Initialize_Context();
		std::string Remove_Parenthesis(std::string);
		void Initialize_Simulation(repast::ScheduleRunner&);
		void Move_Cells(std::vector<repast::AgentId>&);
		void Wander();
		void Move_To_Origin(Cell*);
		void Calc_DeltaXYZ(Cell*);
		//tmp for testing
		void soapBubble(Cell*);
		double distanceCell(Cell*, Cell*);
		//end tmp
		double moveStdDev();
		bool Daughter_Of(std::string, std::string, Cell*);
		void Divide();
		void Copy_Cell(Cell*, Cell*);
		void Set_Coords(Cell*, bool); 
		void Make_Dormant(Cell*, Cell*, std::string, std::string);
		double Find_Dist(Cell*, std::string, double, double, double); 
		void Compare_With_Mod_File(); 
		void Save_To_File();
};
#endif
