#ifndef Mig_Table_h
#define Mig_Table_h

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

/*
 * MIG TABLE - USED TO STORE INFORMATION FROM MIGRATION FILE AND SEARCH THE MIGRATION TABLE
 */

class Mig_Table
{
	public:
		Mig_Table(){};
		~Mig_Table(){};
		void Initialize_With_File(std::string);
		std::string Remove_Parenthesis(std::string);
		void Search_Table_Move_Coor(std::string, double&, double&, double&, bool&);
	private:
		struct migration_table 
		{
			std::string name;
			double axis_pos_AP, axis_pos_LR, axis_pos_DV, dis_axis_AP, dis_axis_LR, dis_axis_DV, total_mean_dis, 
				   std_dev_pos_AP, std_dev_pos_LR, std_dev_pos_DV, std_dev_axis_AP, std_dev_axis_LR, std_dev_axis_DV, total_dis;
		};
		std::vector<migration_table> table;

		bool Has_Nums(std::string);
};
#endif
