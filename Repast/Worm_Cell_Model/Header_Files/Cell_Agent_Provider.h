#ifndef Cell_Agent_Provider_h
#define Cell_Agent_Provider_h

#include <repast_hpc/AgentRequest.h>
#include <repast_hpc/AgentId.h>
#include <repast_hpc/SharedContext.h>
#include <vector>
#include "Cell.h"

/*
 * AGENT PROVIDER - USED TO CREATE PACKAGES TO SEND TO OTHER PROCESSORS
 */

class Cell_Agent_Provider
{
	public:
		Cell_Agent_Provider(repast::SharedContext<Cell> *agents) { cells = agents; }
		void providePackage(Cell*, std::vector<Cell_Package>&);
		void provideContent(repast::AgentRequest, std::vector<Cell_Package>&);
	
	private:
		repast::SharedContext<Cell> *cells;
};
#endif
