#ifndef Cell_h
#define Cell_h

#include <string>
#include <math.h>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"

/*
 * CELL - THIS IS THE AGENT USED FOR THE SIMULATION 
 */

class Cell
{
	public:
		Cell(){};
		~Cell(){};
		Cell(repast::AgentId new_id) { id = new_id; }
		virtual repast::AgentId& getId() { return id; } //required by the AgentId Class; DO NOT CHANGE OR CODE WILL NOT COMPILE!!!
		virtual const repast::AgentId& getId() const { return id; } //required by the AgentId Class; DO NOT CHANGE OR CODE WILL NOT COMPILE!!!
		bool Get_InDivCycle() { return in_div_cycle; }
		bool Get_CanMove() { return can_move; }
		bool Get_CanUse() { return can_use; }
		std::string Get_Name() { return name; }
		std::string Get_TwelveParent() { return twelve_parent; }
		std::string Get_Sex() { return sex; }
		int Get_DivTick() { return div_tick; }
		int Get_Age() { return age; }
		int Get_Daughter_1_ID() { return daughter_1_ID; }
		int Get_Daughter_2_ID() { return daughter_2_ID; }
		double Get_DivTime() { return div_time; }
		double Get_StdDev() { return std_dev; }
		double Get_XDivVec() { return x_div_vec; }
		double Get_YDivVec() { return y_div_vec; }
		double Get_ZDivVec() { return z_div_vec; }
		double Get_XDirVec() { return x_dir_vec; }
		double Get_YDirVec() { return y_dir_vec; }
		double Get_ZDirVec() { return z_dir_vec; }
		double Get_XCoor() { return x_coor; }
		double Get_YCoor() { return y_coor; }
		double Get_ZCoor() { return z_coor; }
		double Get_CellSize() { return cell_size; }
		double Get_CellRadius() { return cell_radius; }
		double Get_DeltaX() { return delta_x; }
		double Get_DeltaY() { return delta_y; }
		double Get_DeltaZ() { return delta_z; }
		double Get_InEllipsoid() { return in_ellipsoid; }
		double Get_DivCycleTime() { return div_cycle_time; }
		double Get_TargetXCoor() { return target_x_coor; }
		double Get_TargetYCoor() { return target_y_coor; }
		double Get_TargetZCoor() { return target_z_coor; }
		void Set_IDRank(int rank) { id.currentRank(rank); }
		void Set_InDivCycle(bool status) { in_div_cycle = status; }
		void Set_CanMove(bool status) { can_move = status; }
		void Set_CanUse(bool status) { can_use = status; }
		void Set_Name(std::string new_name) { name = new_name; }
		void Set_TwelveParent(std::string new_parent) { twelve_parent = new_parent; }
		void Set_Sex(std::string new_sex) { sex = new_sex; }
		void Set_DivTick(int tick) { div_tick = tick; }
		void Set_Age(int new_age) { age = new_age; }
		void Set_Daughter_1_ID(int id) { daughter_1_ID = id; }
		void Set_Daughter_2_ID(int id) { daughter_2_ID = id; }
		void Set_DivTime(double time) { div_time = time; }
		void Set_StdDev(double dev) { std_dev = dev; }
		void Set_XDirVec(double vector) { x_dir_vec = vector; }
		void Set_YDirVec(double vector) { y_dir_vec = vector; }
		void Set_ZDirVec(double vector) { z_dir_vec = vector; }
		void Set_XDivVec(double vector) { x_div_vec = vector; }
		void Set_YDivVec(double vector) { y_div_vec = vector; }
		void Set_ZDivVec(double vector) { z_div_vec = vector; }
		void Set_XCoor(double coor) { x_coor = coor; }
		void Set_YCoor(double coor) { y_coor = coor; }
		void Set_ZCoor(double coor) { z_coor = coor; }
		void Set_CellSize(double size) { cell_size = size; }
		void Set_InEllipsoid(double double_flag) { in_ellipsoid = double_flag; }
		void Set_CellRadius(double rad) { cell_radius = rad; }
		void Set_DeltaX(double x_dis) { delta_x = x_dis; }
		void Set_DeltaY(double y_dis) { delta_y = y_dis; }
		void Set_DeltaZ(double z_dis) { delta_z = z_dis; }
		void Set_DivCycleTime(double time) { div_cycle_time = time; }
		void Set_TargetXCoor(double coor) { target_x_coor = coor; }
		void Set_TargetYCoor(double coor) { target_y_coor = coor; }
		void Set_TargetZCoor(double coor) { target_z_coor = coor; }

	private:
		repast::AgentId id;
		bool in_div_cycle, can_move, can_use, is_tracker;
		std::string name, twelve_parent, sex;
		int  div_tick, age, daughter_1_ID, daughter_2_ID, cell_count;
		double div_time, std_dev, x_div_vec, y_div_vec, z_div_vec, x_dir_vec, y_dir_vec, z_dir_vec, x_coor, y_coor, z_coor, cell_size, cell_radius,
			  in_ellipsoid, delta_x, delta_y, delta_z, div_cycle_time, target_x_coor, target_y_coor, target_z_coor;
};


/*
	CELL PACKAGE - USED TO SEND AGENTS (CELL CLASS) BETWEEN PROCESSES 
 */

struct Cell_Package
{   
	bool in_div_cycle, can_move, can_use;
	std::string name, twelve_parent, sex;
	int  div_tick, age, daughter_1_ID, daughter_2_ID, id, rank, type, current_rank;
	double div_time, std_dev, x_div_vec, y_div_vec, z_div_vec, x_dir_vec, y_dir_vec, z_dir_vec, x_coor, y_coor, z_coor, cell_size, cell_radius,
		  in_ellipsoid, delta_x, delta_y, delta_z, div_cycle_time, target_x_coor, target_y_coor, target_z_coor;

	Cell_Package(){};
	Cell_Package(repast::AgentId, Cell*);
	
	template<class Archive> //archive packaging is handled by Boost
	void serialize(Archive& ar, const unsigned int version)
	{
	     ar &in_div_cycle;
		 ar &can_move;
		 ar &can_use;
		 ar &name;
		 ar &twelve_parent;
		 ar &sex;
		 ar &div_tick;
		 ar &age;
		 ar &daughter_1_ID;
		 ar &daughter_2_ID;
		 ar &id; 
		 ar &rank;
		 ar &type;
		 ar &current_rank;
	     ar &div_time;
		 ar &std_dev;
		 ar &x_div_vec;
		 ar &y_div_vec;
		 ar &z_div_vec;
		 ar &x_dir_vec;
		 ar &y_dir_vec;
		 ar &z_dir_vec;
		 ar &x_coor;
		 ar &y_coor;
		 ar &z_coor;
		 ar &cell_size;
		 ar &cell_radius;
		 ar &in_ellipsoid;
		 ar &delta_x;
		 ar &delta_y;
		 ar &delta_z;
		 ar &div_cycle_time;
		 ar &target_x_coor;
		 ar &target_y_coor;
		 ar &target_z_coor;
	}
};  

#endif
