#ifndef Time_Table_h
#define Time_Table_h

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

/*
 * TIME TABLE - USED TO STORE THE INFORMATION FROM THE TIME FILE AND FOR SEARCHING THE TIME TABLE 
 */

class Time_Table
{
	public:
		Time_Table(){};
		~Time_Table(){};
		void Initialize_With_File(std::string);
		std::string Remove_Parenthesis(std::string);
		void Search_Table_Division(std::string, double&, double&, bool&);
		void Search_Table_Initialize(std::string, double&, bool&);
		int Get_Num_Of_Entries() { return table.size(); }
	private:
		struct time_table
		{
			std::string name;
			double div_time, div_std_dev, cycle_time, cycle_std_dev;
		};
		std::vector<time_table> table;
};
#endif
