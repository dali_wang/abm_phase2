#ifndef Cell_Agent_Receiver_h
#define Cell_Agent_Receiver_h

#include <repast_hpc/AgentId.h>
#include <repast_hpc/SharedContext.h>
#include "Cell.h"

/*
 * AGENT RECEIVER - RECIEVES PACKAGES AND UNLOADS INFORMATION IN IT
 */

class Cell_Agent_Receiver
{
	public:
		Cell_Agent_Receiver(repast::SharedContext<Cell>* agents) { cells = agents; }
		Cell* createAgent(Cell_Package);
		void updateAgent(Cell_Package);
	
	private:
		repast::SharedContext<Cell> *cells;
};
#endif
