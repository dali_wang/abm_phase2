import sys;
import subprocess;

print "IMPORTANT: This script only functions if the entire Repository is DLd and if model.props accurately reflect current location of nuclei files"
print "\nIf no args are provided ./forVisIt is used, else it uses the path in the first argument"

with open("./Props_Files/model.props", 'r') as f:
	for line in f:
		if ("output.nuc.dir" in line):
			breakString = line.split();
			print "Reading from " + breakString[2];
			if(len(sys.argv) == 1):
				print "Writing to ./forVisIt"
				rc = subprocess.call(["python", "../../VisIt Scripts/4 Colors.py", "./Text_Files/DivisionDirWQ.txt", breakString[2], "./forVisIt/darterTest"]);
				print "Opening VisIt to view new files"
				rc2 = subprocess.call(["visit", "-s", "./visitOpenScript.py"]);
			else:
				print "Feature not currently supported!"
