#include "Time_Table.h"

/*********************************************************
 * 
 * The following function reads a file from the file path
 * and loads the entire file into the vector of structs.
 *
 * ******************************************************/

void Time_Table::Initialize_With_File(std::string file_path)
{
	std::ifstream input_file;
	time_table temp_entry;
	std::string data;

	input_file.open(file_path.c_str(), std::ifstream::in);

    if(input_file.is_open())
    {
    	while(!input_file.eof())
        {
			input_file >> data;

			if(!input_file.eof()) //Ensuring that the entire file has not been read and the "re-reading" of the last line
			{
				temp_entry.name = Remove_Parenthesis(data);
			
				input_file >> temp_entry.div_time >> temp_entry.div_std_dev >> temp_entry.cycle_time >> temp_entry.cycle_std_dev;
            	table.push_back(temp_entry);
			}
        }
    }
	else
		std::cout << "ERROR! TIME FILE COULD NOT BE OPENED!\n" << file_path << " DID NOT WORK!\n";

    input_file.close();
}

/*****************************************************
 *
 * The following function removes parenthesis ("") 
 * from a string. 
 *
 * NOTE: USED MAINLY SINCE NETLOGO REQUIRED 
 * 		 PARENTHESIS TO DENOTE A STRING.
 *
 * ***************************************************/

std::string Time_Table::Remove_Parenthesis(std::string name)
{
	int length = name.length(), index = 0;
	std::string new_name = "";

	while(index < length)
	{
		if(name[index] != '"')
			new_name += name[index];
		index++;
	}

	return new_name;
}


/********************************************************
 *
 * The following function checks the table looking for a 
 * match between names. If it finds one, it will change
 * the other variables (passed in) with the information
 * in the entry.
 *
 * NOTE: SPECIFIC TO DIVISION FUNCTION IN CELL MODEL CLASS
 *
 * *****************************************************/

void Time_Table::Search_Table_Division(std::string name, double &cycle_time, double &cycle_std_dev, bool &time_found)
{
	int table_size = table.size(), entry = 0;
	
	while(!time_found && entry < table_size)
	{
		if(table[entry].name == name)
		{
			cycle_time = table[entry].cycle_time;
			cycle_std_dev = table[entry].cycle_std_dev;
			time_found = true;
		}
		else
			entry++;
	}
}


/*********************************************************
 *
 * The following function checks the table for a match
 * between names. If it finds one, it will change the 
 * div time.
 *
 * NOTE: SPECIFIC TO INITIALIZE CONTEXT IN CELL MODEL CLASS
 *
 * ******************************************************/

void Time_Table::Search_Table_Initialize(std::string name, double &div_time, bool &time_found)
{
	int table_size = table.size(), entry = 0;
	time_found = false;
	
	while(!time_found && entry < table_size)
	{
		if(table[entry].name == name)
		{
			div_time = table[entry].div_time;
			time_found = true;
		}
		else
			entry++;
	}
}
