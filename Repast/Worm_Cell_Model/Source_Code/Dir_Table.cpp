#include "Dir_Table.h"


/*********************************************************
 * 
 * The following function reads a file from the file path
 * and loads the entire file into the vector of structs.
 *
 * ******************************************************/

void Dir_Table::Initialize_With_File(std::string file_path)
{
	std::ifstream input_file;
	direction_table temp_entry;
	std::string dummy_line, data;

	input_file.open(file_path.c_str(), std::ifstream::in);
	
	if(input_file.is_open())
	{
		std::getline(input_file, dummy_line); //The first line of the file has no useful information for the program; So it is skipped here
		
		while(!input_file.eof())
		{
			input_file >> data;
			
			if(!input_file.eof()) //Ensuring that the last line of file is not read twice, this is due to how files are read in the ifstream
			{
				temp_entry.parent = Remove_Parenthesis(data);
				
				input_file >> temp_entry.daughter_1 >> temp_entry.daughter_2 >> temp_entry.x_dir_vec >> temp_entry.y_dir_vec >> temp_entry.z_dir_vec;
				
				temp_entry.daughter_1 = Remove_Parenthesis(temp_entry.daughter_1);
				temp_entry.daughter_2 = Remove_Parenthesis(temp_entry.daughter_2);
				
				table.push_back(temp_entry);
			}
		}
	}
	else
		std::cout << "ERROR! DIRECTION FILE COULD NOT BE OPENED!\n" << file_path << " DID NOT WORK!\n";

	input_file.close();
}


/*****************************************************
 *
 * The following function removes parenthesis ("") 
 * from a string. 
 *
 * NOTE: USED MAINLY SINCE NETLOGO REQUIRED 
 * 		 PARENTHESIS TO DENOTE A STRING.
 *
 * ***************************************************/

std::string Dir_Table::Remove_Parenthesis(std::string name)
{
	int length = name.length(), index = 0;
	std::string new_name = "";

	while(index < length)
	{
		if(name[index] != '"')
			new_name += name[index];
		index++;
	}

	return new_name;
}


/***********************************************************
 *
 * The following function will search the table for a match
 * between the name in the table and the parent (passed in).
 * If they match, both daughters (passed in) are assigned to
 * the value in that entry of the table. In addition, so are
 * the coordinates (passed in).
 *
 * NOTE: SPECIFIC TO DIVISION FUNCTION IN CELL MODEL CLASS
 *
 * ********************************************************/

void Dir_Table::Search_Table_Division(std::string parent, std::string &daughter_1_name, std::string &daughter_2_name, double &x_dir_vec, double &y_dir_vec, double &z_dir_vec, bool &found_cell)
{
	int table_size = table.size(), entry = 0;	
	found_cell = false;
	
	while(!found_cell && entry < table_size) //search table until a match is found or the whole table has been scanned with no matches found
	{
		if (table[entry].parent == parent) //if found, change passed in variables to the variables in the table
		{
			daughter_1_name = table[entry].daughter_1;
			daughter_2_name = table[entry].daughter_2;
			x_dir_vec = table[entry].x_dir_vec;
			y_dir_vec = table[entry].y_dir_vec;
			z_dir_vec = table[entry].z_dir_vec;
			found_cell = true;
		}
		else
			entry++;
	}
}


/***********************************************************
 * 
 * The following function will search the table for a match
 * between the passed in name and one of the daughters.
 * Depending on the number (which_daughter), it will either
 * be daughter 1 or 2 being matched against. If it finds a 
 * match, the name (passed in) becomes the paren (passed in).
 *
 * NOTE: SPECIFIC TO DAUGHTER OF FUNCTION IN CELL MODEL
 *
 * *******************************************************/

void Dir_Table::Search_Table_Daughter_Of(std::string parent, int which_daughter, std::string &name, bool &cell_found)
{
	int table_size = table.size(), entry = 0;
	std::string daughter = "";
	
	if(which_daughter == 1 || which_daughter == 2) //GUARDING - Only 1 and 2 to be used
	{
		while(!cell_found && entry < table_size)
		{
			if(which_daughter == 1) //checks which daughter needs to be scanned against
				daughter = table[entry].daughter_1;
			else
				daughter = table[entry].daughter_2;
			
			if(name == daughter && !cell_found)
			{
				name = parent;
				cell_found = true;
			}
			else
				entry++;
		}
	}
	else
		std::cout << "ERROR! UNNEXPECTED DAUGHTER!";
}
