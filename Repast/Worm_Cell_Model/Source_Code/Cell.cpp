#include "Cell.h"

/**********************************************************
 *
 * The following function is a constructor to intialize a
 * agent package for a cell. This is used in order to pass
 * non local agents between processes.
 *
 *********************************************************/

Cell_Package::Cell_Package(repast::AgentId ID, Cell* agent)
{
	id = ID.id();
	rank = ID.startingRank();
	type = ID.agentType();
	current_rank = ID.currentRank();
	in_div_cycle = agent->Get_InDivCycle();
	name = agent->Get_Name();
	twelve_parent = agent->Get_TwelveParent();
	age = agent->Get_Age();
	sex = agent->Get_Sex();
	daughter_1_ID = agent->Get_Daughter_1_ID();
	daughter_2_ID = agent->Get_Daughter_2_ID();
	div_time = agent->Get_DivTime();
	std_dev = agent->Get_StdDev();
	x_dir_vec = agent->Get_XDirVec();
	y_dir_vec = agent->Get_YDirVec();
	z_dir_vec = agent->Get_ZDirVec();
	x_div_vec = agent->Get_XDivVec();
	y_div_vec = agent->Get_YDivVec();
	z_div_vec = agent->Get_ZDivVec();
	x_coor = agent->Get_XCoor();
	y_coor = agent->Get_YCoor();
	z_coor = agent->Get_ZCoor();
	target_x_coor = agent->Get_TargetXCoor();
	target_y_coor = agent->Get_TargetYCoor();
	target_z_coor = agent->Get_TargetZCoor();
	cell_size = agent->Get_CellSize();
	cell_radius = agent->Get_CellRadius();
	delta_x = agent->Get_DeltaX();
	delta_y = agent->Get_DeltaY();
	delta_z = agent->Get_DeltaZ();
	in_ellipsoid = agent->Get_InEllipsoid();
	div_cycle_time = agent->Get_DivCycleTime();
	can_move = agent->Get_CanMove();
	can_use = agent->Get_CanUse();
	div_tick = agent->Get_DivTick();
}
