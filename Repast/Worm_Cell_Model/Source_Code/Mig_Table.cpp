#include "Mig_Table.h"

/*********************************************************
 * 
 * The following function reads a file from the file path
 * and loads the entire file into the vector of structs.
 *
 * ******************************************************/

void Mig_Table::Initialize_With_File(std::string file_path)
{
	std::ifstream input_file;
	std::string poss_num = "", name;
	migration_table temp_entry;
	double junk_data;
	
	input_file.open(file_path.c_str(), std::ifstream::in);

	if(input_file.is_open())
	{
		while(!input_file.eof())
		{
			if(poss_num == "") //Since every line is not gaurenteed to have data and a second var is read
				input_file >> temp_entry.name; //It is not every time that we need to read a name

			if(!input_file.eof()) //This and the other end of file check is to ensure that data is not rea twice into the table
			{					  //This is a issue with the way ifstream reads files
				input_file >> poss_num;

				if( !input_file.eof() && Has_Nums(poss_num) ) //If a number was read, the rest of the line is good
				{						 					  //read it and load into table
					temp_entry.axis_pos_AP = stod(poss_num);
					input_file >> temp_entry.axis_pos_LR >> temp_entry.axis_pos_DV >> temp_entry.dis_axis_AP >> temp_entry.dis_axis_LR >> temp_entry.dis_axis_DV 
							   >> temp_entry.total_mean_dis >> temp_entry.std_dev_pos_AP >> temp_entry.std_dev_pos_LR >> temp_entry.std_dev_pos_DV 
							   >> temp_entry.std_dev_axis_AP >> temp_entry.std_dev_axis_LR >> temp_entry.std_dev_axis_DV >> temp_entry.total_dis;
			
					temp_entry.name = Remove_Parenthesis(temp_entry.name);
					temp_entry.axis_pos_AP /= 2;
					temp_entry.axis_pos_LR /= -2;
					temp_entry.axis_pos_DV /= -2;
					temp_entry.dis_axis_AP /= 2;
					temp_entry.dis_axis_LR /= -2;
					temp_entry.dis_axis_DV /= -2;
					
					table.push_back(temp_entry);
					poss_num = ""; //Finished reading the line, ready to accept another name into temp_entry
				}
				else //Otherwise, "believe" that the variable read is the name of the line underneeth it and don't read another name in
					temp_entry.name = poss_num;
			}
		}
		input_file.close();
	}
	else
		std::cout << "ERROR! MIGRATION FILE COULD NOT BE OPENED!\n" << file_path << " DID NOT WORK!\n";
}


/*****************************************************
 *
 * The following function removes parenthesis ("") 
 * from a string. 
 *
 * NOTE: USED MAINLY SINCE NETLOGO REQUIRED 
 * 		 PARENTHESIS TO DENOTE A STRING.
 *
 * ***************************************************/

std::string Mig_Table::Remove_Parenthesis(std::string name)
{
	int length = name.length(), index = 0;
	std::string new_name = "";

	while(index < length)
	{
		if(name[index] != '"')
			new_name += name[index];
		index++;
	}

	return new_name;
}


/***********************************************************
 *
 * This function checks whether a string has a number in it
 *
 * ********************************************************/

bool Mig_Table::Has_Nums(std::string poss_num_str)
{
	int length = poss_num_str.length();

	for(int index = 0; index < length; index++)
	{
		if(poss_num_str[index] >= '0' && poss_num_str[index] <= '9')
			return true;
	}

	return false;
}


/************************************************************
 *
 * This function searchs the table for a mtach between names.
 * If one is found, then the vectors coordinates thrown in 
 * are changed to reflect where the agent should be moving 
 * towards
 *
 * NOTE: CALLED BY BOTH IN INITIALIZATION AND WANDER FUNCTIONS
 *
 * *********************************************************/

void Mig_Table::Search_Table_Move_Coor(std::string name, double &x_coor, double &y_coor, double &z_coor, bool &found_name)
{
	int table_size = table.size(), entry = 0;
	found_name = false;

	while(entry < table_size && !found_name)
	{
		if(table[entry].name == name)
		{
			x_coor = table[entry].dis_axis_AP + table[entry].axis_pos_AP;
			y_coor = table[entry].dis_axis_LR + table[entry].axis_pos_LR;
			z_coor = table[entry].dis_axis_DV + table[entry].axis_pos_DV;
			found_name = true;
		}
		else
			entry++;
	}
	
}
