#include "Cell_Agent_Receiver.h"

/****************************************************
 *
 * The following function creates an agent out of the
 * information provided by the package. New instance
 * to be used by a process.
 *
 * *************************************************/

Cell* Cell_Agent_Receiver::createAgent(Cell_Package package)
{
	repast::AgentId id(package.id, package.rank, package.type, package.current_rank);

	Cell* new_cell = new Cell(id);
	
	new_cell->Set_Name(package.name);
	new_cell->Set_StdDev(package.std_dev);
	new_cell->Set_InEllipsoid(package.in_ellipsoid);
	new_cell->Set_DivTime(package.div_time);
	new_cell->Set_DivTick(package.div_tick);
	new_cell->Set_XCoor(package.x_coor);
	new_cell->Set_YCoor(package.y_coor);
	new_cell->Set_ZCoor(package.z_coor);
	new_cell->Set_Age(package.age);
	new_cell->Set_CellSize(package.cell_size);
	new_cell->Set_XDivVec(package.x_div_vec);
	new_cell->Set_YDivVec(package.y_div_vec);
	new_cell->Set_ZDivVec(package.z_div_vec);
	new_cell->Set_XDirVec(package.x_dir_vec);
	new_cell->Set_YDirVec(package.y_dir_vec);
	new_cell->Set_ZDirVec(package.z_dir_vec);
	new_cell->Set_InDivCycle(package.in_div_cycle);
	new_cell->Set_TwelveParent(package.twelve_parent);
	new_cell->Set_Daughter_1_ID(package.daughter_1_ID);
	new_cell->Set_Daughter_2_ID(package.daughter_2_ID);
	new_cell->Set_CellRadius(package.cell_radius);
	new_cell->Set_DeltaX(package.delta_x);
	new_cell->Set_DeltaY(package.delta_y);
	new_cell->Set_DeltaZ(package.delta_z);
	new_cell->Set_DivCycleTime(package.div_cycle_time);
	new_cell->Set_CanMove(package.can_move);
	new_cell->Set_TargetXCoor(package.target_x_coor);
	new_cell->Set_TargetYCoor(package.target_y_coor);
	new_cell->Set_TargetZCoor(package.target_z_coor);
	new_cell->Set_CanUse(package.can_use);

	return new_cell;
}


/* **********************************************************
 *
 * The following function updates an agent based on the 
 * information in the package. This is to sync an agent
 * when traveling between processes since info done to
 * an agent is overwritten between processes.
 *
 * *********************************************************/

void Cell_Agent_Receiver::updateAgent(Cell_Package package)
{
	repast::AgentId id(package.id, package.rank, package.type);
	
	Cell *new_cell = cells->getAgent(id); //AGENT MUST ALREADY EXIST OTHERWISE THIS WILL CRASH
				    
	new_cell->Set_IDRank(package.current_rank);
	new_cell->Set_Name(package.name);
	new_cell->Set_StdDev(package.std_dev);
	new_cell->Set_InEllipsoid(package.in_ellipsoid);
	new_cell->Set_DivTime(package.div_time);
	new_cell->Set_DivTick(package.div_tick);
	new_cell->Set_XCoor(package.x_coor);
	new_cell->Set_YCoor(package.y_coor);
	new_cell->Set_ZCoor(package.z_coor);
	new_cell->Set_Age(package.age);
	new_cell->Set_CellSize(package.cell_size);
	new_cell->Set_XDivVec(package.x_div_vec);
	new_cell->Set_YDivVec(package.y_div_vec);
	new_cell->Set_ZDivVec(package.z_div_vec);
	new_cell->Set_XDirVec(package.x_dir_vec);
	new_cell->Set_YDirVec(package.y_dir_vec);
	new_cell->Set_ZDirVec(package.z_dir_vec);
	new_cell->Set_InDivCycle(package.in_div_cycle);
	new_cell->Set_TwelveParent(package.twelve_parent);
	new_cell->Set_Daughter_1_ID(package.daughter_1_ID);
	new_cell->Set_Daughter_2_ID(package.daughter_2_ID);
	new_cell->Set_CellRadius(package.cell_radius);
	new_cell->Set_DeltaX(package.delta_x);
	new_cell->Set_DeltaY(package.delta_y);
	new_cell->Set_DeltaZ(package.delta_z);
	new_cell->Set_DivCycleTime(package.div_cycle_time);
	new_cell->Set_CanMove(package.can_move);
	new_cell->Set_TargetXCoor(package.target_x_coor);
	new_cell->Set_TargetYCoor(package.target_y_coor);
	new_cell->Set_TargetZCoor(package.target_z_coor);
	new_cell->Set_CanUse(package.can_use);
}
