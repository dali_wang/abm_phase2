#include "Cell_Agent_Provider.h"

/*****************************************************
 *
 * The following functions prepares packages an agent
 * and places it in a vector to contain it. Used to 
 * send non local agents to different processes.
 *
 *
 * **************************************************/

void Cell_Agent_Provider::providePackage(Cell *agent, std::vector<Cell_Package>& sender)
{
    repast::AgentId id = agent->getId();
	Cell_Package package(id, agent);
	sender.push_back(package);
}


/***************************************************
 *
 * The following function prepares a list of agent 
 * packages for a requesting process. The amount is
 * determined outside the function.
 *
 * *************************************************/

void Cell_Agent_Provider::provideContent(repast::AgentRequest requester, std::vector<Cell_Package>& sender)
{
	std::vector<repast::AgentId> ids = requester.requestedAgents();
	
	for(size_t i = 0; i < ids.size(); i++)
	{
		providePackage( cells->getAgent(ids[i]), sender );
	}
}
