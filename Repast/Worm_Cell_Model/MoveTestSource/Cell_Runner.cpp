#include <string>
#include <boost/mpi.hpp>
#include "Cell_Model.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Schedule.h"

int main(int argc, char** argv)
{

	if(argc!=3){
        printf("USAGE: ./Simulation configFile propsFile\n");
    }
    std::string configFile = argv[1], propsFile = argv[2];
	
	//Setting up Boost
	boost::mpi::environment env(argc, argv); 
	boost::mpi::communicator world;

	//Settuing up Repast
	repast::RepastProcess::init(configFile);

	//Setting up simulation
	Cell_Model *cells = new Cell_Model(propsFile, argc, argv, &world);

	//Creating the schedule for the simulation and running it
	repast::ScheduleRunner &simulation = repast::RepastProcess::instance()->getScheduleRunner();
	cells->Initialize_Simulation(simulation);
	simulation.run();

	//Clean up
	delete cells;
	repast::RepastProcess::instance()->done();
}
