#include "Cell_Model.h"

//NOTE: NORMALLY (WITH THE EXCEPTION OF WANDER) THE ONLY CELLS NEEDED ARE LOCAL ONES.
//		SINCE WANDER REQUIRES KNOWLEDGE OF WHERE ALL CELLS ARE, EVERY CELL INCLUDING
//		NONLOCAL CELLS ARE NEEDED
//
//NOTE: ALL NEW CELLS ARE COPIES OF THEIR PARENTS. THEIR VALUES ARE LATER CHANGED TO
//		BE MORE SPECIFIC TO THAT CELL
//
//NOTE: ANY WARNINGS INVOLVING CELLS NOT BEING FOUND IN A TABLE (WITH EXCEPTION TO 
//		THOSE NOT FOUND IN THE TIME TABLE DURING THE DIVISION FUNCTION--THIS HAS
//		ALREADY BEEN ACCOUNTED FOR AND AN IMPLEMENT CREATED TO HANDLE IT) WILL CAUSE
//		THE CELL TO NOT BE INTIALIZED WITH ITS CORRECT VALUES AND ASSUME THAT ALL OUTPUT
//		PAST THAT MESSAGE IS WRONG. ONCE THIS MESSAGE POPS UP, CHECK THE TABLES FOR
//		DISCREPANCIES.


/****************************************
 *
 * The following function is the
 * constructor for the Cell Model Class.
 * It extracts all info from the
 * model.props file (including file paths
 * of other files needed to create tables)
 * and initializes variables.
 *
 * *************************************/

Cell_Model::Cell_Model(std::string props_file, int argc, char** argv, boost::mpi::communicator* comm):cell_context(comm)
{
	int total_num_of_cells, world_size = repast::RepastProcess::instance()->worldSize(), rank = repast::RepastProcess::instance()->rank();
	repast::Properties *props = new repast::Properties(props_file, argc, argv, comm);

	//Setting up process variables
	time_resolution = repast::strToInt(props->getProperty("time.resolution"));
	cell_focus_switch = (1 == repast::strToInt(props->getProperty("cell.focus.switch")));
	time_limit = repast::strToInt(props->getProperty("time.limit"));
	init_file_number = repast::strToInt(props->getProperty("init.file.number"));

	id = 1;
	num_of_cells = 0;
	times_accessed_file = 0;

	//Setting up files and directories for reading
	mod_file_dir = props->getProperty("mod.files.dir");
	displacement_file = props->getProperty("output.dis.dir");
	nuclei_file_dir = props->getProperty("output.nuc.dir");
	initial_cell_file = props->getProperty("cell.intialize.file");
	

	//Setting a time table variable

	//Seeding the random number generator
	initializeRandom(*props, comm);

	//Set the moveStdDev
	migStdDev = repast::strToDouble(props->getProperty("mig.stdDev"));
	delete props;
	
	//Creating provider and receiver objects
	cell_provider = new Cell_Agent_Provider(&cell_context);
	cell_receiver = new Cell_Agent_Receiver(&cell_context);

	
	//Creating grid for agents
	repast::Point<double> origin(ORIGIN_X, ORIGIN_Y, ORIGIN_Z);
	repast::Point<double> ext_from_origin(GRID_LENGTH, GRID_HEIGHT, GRID_DEPTH);
			    
	repast::GridDimensions grid_dim(origin, ext_from_origin);
						    
	std::vector<int> process_dim;
	process_dim.push_back(DIS_BUFFER_AREA);
	process_dim.push_back(DIS_BUFFER_AREA);
	process_dim.push_back(DIS_BUFFER_AREA);
													    
	cont_ellipsoid = new repast::SharedContinuousSpace<Cell, repast::WrapAroundBorders, repast::SimpleAdder<Cell> >("AgentContinuousSpace", grid_dim, process_dim, CONT_BUFFER_AREA, comm);
	dis_ellipsoid = new repast::SharedDiscreteSpace<Cell, repast::WrapAroundBorders, repast::SimpleAdder<Cell> >("AgentDiscreteSpace", grid_dim, process_dim, DIS_BUFFER_AREA, comm);
																	
	cell_context.addProjection(cont_ellipsoid);
	cell_context.addProjection(dis_ellipsoid);
}


/*****************************************
 *
 * This destructor destroys
 * the cell_provider and cell_receiver
 * pointers for freeing memory
 *
 * ***************************************/

Cell_Model::~Cell_Model()
{
	delete cell_provider;
	delete cell_receiver;
}


/********************************************
 *
 * This function creates agents and returns
 * them.
 *
 * *****************************************/

Cell* Cell_Model::Create_Cell()
{
	int rank = repast::RepastProcess::instance()->rank();
	repast::AgentId id(num_of_cells, rank, CELL_TYPE, rank);
	
	//Create a new agent
	Cell *agent = new Cell(id);
	
	cell_context.addAgent(agent);
	agent->Set_CanUse(CAN_USE);
	num_of_cells++;

	return agent;
}

/*********************************************
 *
 * This function generates one dummy cell so
 * that the simulation can run the every 
 * process is able to call at least one local
 * cell. Segmentation faults occur when
 * attempting to make a vector of local agents
 * without any local agents being in the context
 *
 * *******************************************/

void Cell_Model::Generate_Cells()
{
	int rank = repast::RepastProcess::instance()->rank(), area_buffer = CONT_BUFFER_AREA + 1;
	double x_coor, y_coor, z_coor;

	//First get the full range of coordinates for this grid
	x_coor = (double)cont_ellipsoid->dimensions().origin().getX() + (double)cont_ellipsoid->dimensions().extents().getX();
	y_coor = (double)cont_ellipsoid->dimensions().origin().getY() + (double)cont_ellipsoid->dimensions().extents().getY();
	z_coor = (double)cont_ellipsoid->dimensions().origin().getZ() + (double)cont_ellipsoid->dimensions().extents().getZ();

	//Create a random number generator that will arbitraily place the cells within the confines of this process's grid
	//Since the origin is always the bottom left corner of the grid, we can always add the buffer to the origin value for our coordinate buffer pushing the value...
	//...to the right. However, for the extents, sometimes we must need to always push the boundary to the left. Thus we need to chedk if the extents are negative
	repast::DoubleUniformGenerator genX = repast::Random::instance()->createUniDoubleGenerator( ( (double)cont_ellipsoid->dimensions().origin().getX() + area_buffer),
																					  		  ( x_coor < 0 ? x_coor + area_buffer : x_coor - area_buffer) ); 
	repast::DoubleUniformGenerator genY = repast::Random::instance()->createUniDoubleGenerator( ( (double)cont_ellipsoid->dimensions().origin().getY() + area_buffer),
																							  ( y_coor < 0 ? y_coor + area_buffer : y_coor - area_buffer) ); 
	repast::DoubleUniformGenerator genZ = repast::Random::instance()->createUniDoubleGenerator( ( (double)cont_ellipsoid->dimensions().origin().getZ() + area_buffer),
																							  ( z_coor < 0 ? z_coor + area_buffer : z_coor - area_buffer) ); 

	repast::AgentId id(0, rank, DUMMY_TYPE, rank);
	Cell *agent = new Cell(id);
	cell_context.addAgent(agent);
	agent->Set_CanUse(CANNOT_USE);
	agent->Set_InDivCycle(CANNOT_DIVIDE);
	agent->Set_CanMove(CANNOT_MOVE);

	//Place cell somewhere on the grid
	x_coor = (double)genX.next();
	y_coor = (double)genY.next();
	z_coor = (double)genZ.next();

	repast::Point<double> cont_location(x_coor, y_coor, z_coor);
	repast::Point<int> dis_location((int)x_coor, (int)y_coor, (int)z_coor);
	dis_ellipsoid->moveTo(agent->getId(), dis_location);
	cont_ellipsoid->moveTo(agent->getId(), cont_location);
}


/*********************************************
 *
 * This function initializes the cell_context with
 * the cells described in the initial cell 
 * file. Think of the cell_context as a bucket to
 * store agents. It also initializes the new
 * cells (agents).
 *
 * ******************************************/

void Cell_Model::Initialize_Context()
{
	std::string dummy_vars, curr_name;
	double div_time = -1.0, x_coor, y_coor, z_coor, size;
	bool mig_table_flag = false, time_table_flag = false;
	int process_iterations = 0, file_iterations = 0, world_size = repast::RepastProcess::instance()->worldSize(), rank = repast::RepastProcess::instance()->rank(); 

	std::ifstream input_file;
	input_file.open(initial_cell_file.c_str(), std::ifstream::in);

	if(input_file.is_open())
	{
		while(!input_file.eof()) //Several variables in this file are not needed for this simulation, but they still need to be read
		{
			input_file >> dummy_vars;

			if(!input_file.eof()) //Ensuring that the end of the file has not been read
			{					  //Ensuring that the last line of the file is not added to the cell_context twice
				input_file >> dummy_vars >> dummy_vars >> dummy_vars >> x_coor >> y_coor >> z_coor >> size >> curr_name;
				
				if(rank == (file_iterations - (world_size * process_iterations)))
				{ //Every process starts with one cell, in the case that there are more intial cells that process, they may have to take more
					//Creating cell
					Cell *agent = Create_Cell();
					mig_table_flag = time_table_flag = false;

					agent->Set_Daughter_1_ID(id);
					id++;
					agent->Set_Daughter_2_ID(-1);

					agent->Set_XCoor(x_coor / 2 * X_SCALE);
					agent->Set_YCoor(y_coor / 2 * Y_SCALE);
					agent->Set_ZCoor(z_coor / 2 * Z_SCALE);
					agent->Set_CellSize(size / 2 * SIZE_SCALE);
					agent->Set_Name( Remove_Parenthesis(curr_name) );

					time_info_table.Search_Table_Initialize( agent->Get_Name(), div_time, time_table_flag );

					if(time_table_flag) //Successful search in time table
					{
						agent->Set_DivTime(div_time * 60);
						agent->Set_InDivCycle(CAN_DIVIDE);
					}
					else
					{
						agent->Set_InDivCycle(CANNOT_DIVIDE);
						agent->Set_DivCycleTime(NO_DIV_CYCLE);
						std::cout << agent->Get_Name() << " WAS NOT FOUND IN TIME TABLE [INITIALIZATION].\n";
					}
					
					agent->Set_DivTick(-1000000);
					agent->Set_XDivVec(0);
					agent->Set_YDivVec(0);
					agent->Set_ZDivVec(0);
					x_coor = y_coor = z_coor = 0.0;
						
					migration_info_table.Search_Table_Move_Coor( agent->Get_Name(), x_coor, y_coor, z_coor, mig_table_flag );

					if(mig_table_flag && time_table_flag) //Successful search in both time table and migration table
					{
						agent->Set_DeltaX( ( x_coor - agent->Get_XCoor() ) / (agent->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
						agent->Set_DeltaY( ( y_coor - agent->Get_YCoor() ) / (agent->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
						agent->Set_DeltaZ( ( z_coor - agent->Get_ZCoor() ) / (agent->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
						agent->Set_CanMove(CAN_MOVE);
					}
					else if(!mig_table_flag) //Unsuccessful search migration table, but succesful time table search
					{
							agent->Set_CanMove(CANNOT_MOVE);
							std::cout << agent->Get_Name() << " WAS NOT FOUND IN MIGRATION TABLE [INITIALIZATION].\n";
					}
					
					/*
					repast::Point<double> cont_location(agent->Get_XCoor(), agent->Get_YCoor(), agent->Get_ZCoor());
					repast::Point<int> dis_location((int)agent->Get_XCoor(), (int)agent->Get_YCoor(), (int)agent->Get_ZCoor());
					dis_ellipsoid->moveTo(agent->getId(), dis_location);
					cont_ellipsoid->moveTo(agent->getId(), cont_location);
					*/
				}
				file_iterations++;
				
				if(file_iterations % world_size == 0) //Increase the number of iterations if all processs have been given a cell
					process_iterations++;
			}
		}
		input_file.close();
	}
	else
		std::cout << "CELL INITIALIZATION FILE COULD NOT BE OPENED!\n" << initial_cell_file << " DID NOT WORK!\n";
}


/*****************************************************
 *
 * The following function removes parenthesis ("") 
 * from a string. 
 *
 * NOTE: USED MAINLY SINCE NETLOGO REQUIRED 
 * 		 PARENTHESIS TO DENOTE A STRING.
 *
 * ***************************************************/

std::string Cell_Model::Remove_Parenthesis(std::string name)
{
	int length = name.length(), index = 0;
	std::string new_name = "";

	while(index < length)
	{
		if(name[index] != '"')
			new_name += name[index];
		index++;
	}

	return new_name;
}


/**********************************************************
 *
 * This function initializes the schedule runner.
 * When it is ran, the functions described will run
 * until the end of the simulation.
 *
 * NOTE: The scheduleEvent function works like this
 * 		 (starting tick, number of ticks before action is
 * 		 repeated, and the function to be done). If the 
 * 		 action does not need to be repeated, just leave
 * 		 it blank.
 *
 * NOTE: In order to ensure that each event starts after
 * 		 another event has been completed, events have been
 * 		 split by .1 of a tick.
 *
 * *******************************************************/

void Cell_Model::Initialize_Simulation(repast::ScheduleRunner &tasks)
{
	//Create a dummy cell
	//tasks.scheduleEvent(0, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Cell_Model> (this, &Cell_Model::Generate_Cells)));
	//Initialize the context with the starting cells
	tasks.scheduleEvent(((init_file_number * 90) + 1170 + 0.0), repast::Schedule::FunctorPtr(new repast::MethodFunctor<Cell_Model> (this, &Cell_Model::Initialize_Context)));
	//Begin to move the cells
	tasks.scheduleEvent(((init_file_number * 90) + 1170 + 0.1), 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Cell_Model> (this, &Cell_Model::Wander)));
	//Compare the current data with another file
	//tasks.scheduleEvent(0, 90, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Cell_Model> (this, &Cell_Model::Compare_With_Mod_File)));
	//All cells divide
	//Save to file
	tasks.scheduleEvent(((init_file_number * 90) + 1170 + 0.4), 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Cell_Model> (this, &Cell_Model::Save_To_File)));
	//Stop the simulation
	tasks.scheduleStop(time_limit + 0.4);
}


/*******************************************************************
 *
 * This function handles all cell movement. If the cell gets too
 * close or outside of the boundary, it redirects it to the center.
 * It also causes the cell to move by the corse of its division
 * vector and modifies the direction vector through info in the 
 * migration table. Finally, it adjusts the cell based on whichever
 * cell is closest to it.
 *
 * NOTE: THE AREA OF MOVEMENT IS AN ELLIPSOID.
 *
 * ****************************************************************/

void Cell_Model::Wander()
{
	double ellipsoid_check, cycle_time, x_dir_vec, y_dir_vec, z_dir_vec, distance;
	
	//Make copies of cells in buffer zones (copies them as non local agents in the appropriate process)
	//repast::RepastProcess::instance()->synchronizeProjectionInfo<Cell, Cell_Package, Cell_Agent_Provider, Cell_Agent_Receiver, Cell_Agent_Receiver>(cell_context, *cell_provider, *cell_receiver, *cell_receiver);
	//Update all non local agents with their actual information
	//repast::RepastProcess::instance()->synchronizeAgentStates<Cell_Package, Cell_Agent_Provider, Cell_Agent_Receiver>(*cell_provider, *cell_receiver);

	//Get all local agents
	std::vector<Cell*> all_cells;
	cell_context.selectAgents(repast::SharedContext<Cell>::LOCAL, all_cells);
	std::vector<Cell*>::iterator single_cell = all_cells.begin();
	std::vector<Cell*>::iterator end_cell = all_cells.end();	

	if(all_cells.size() != 0) //Only wander as long as there are cells to wander around
	{
		while(single_cell != end_cell) //Go through every cell in the process
		{
			/*
			ellipsoid_check = ( pow( (*single_cell)->Get_XCoor(), 2.0) / (pow((340 / 4 * 0.15), 2.0)) +
								pow( (*single_cell)->Get_YCoor(), 2.0) / (pow((203.9 / 4 * 0.15), 2.0)) +
								pow( (*single_cell)->Get_ZCoor(), 2.0) / (pow((203.9 / 4 * 0.15), 2.0)) );
			*/
			//(*single_cell)->Set_InEllipsoid(ellipsoid_check);
			if( (*single_cell)->Get_CanUse() && (*single_cell)->Get_CanMove() )
			{
				(*single_cell)->Set_InEllipsoid(0);
				
				if( (*single_cell)->Get_InEllipsoid() < 1.0 )
				{
					if( (*single_cell)->Get_DivTick() < div_cycle_time ) //This occurs while the cell is still within a division cycle
					{
						std::cout << "Cell: " << (*single_cell)->Get_Name() << std::endl;
						(*single_cell)->Set_XCoor( (*single_cell)->Get_XCoor() + ( ( (*single_cell)->Get_XDivVec() * (*single_cell)->Get_CellSize() / 2 ) / div_cycle_time ) ); 
						std::cout << "XDivVec: " << (*single_cell)->Get_XDivVec()  << "\nCell_Size: " << (*single_cell)->Get_CellSize() / 2 << std::endl;
						(*single_cell)->Set_YCoor( (*single_cell)->Get_YCoor() + ( ( (*single_cell)->Get_YDivVec() * (*single_cell)->Get_CellSize() / 2 ) / div_cycle_time ) ); 
						std::cout << "YDivVec: " << (*single_cell)->Get_YDivVec() * (*single_cell)->Get_CellSize() / 2 << std::endl;
						(*single_cell)->Set_ZCoor( (*single_cell)->Get_ZCoor() + ( ( (*single_cell)->Get_ZDivVec() * (*single_cell)->Get_CellSize() / 2 ) / div_cycle_time ) ); 
						std::cout << "ZDivVec: " << (*single_cell)->Get_ZDivVec() * (*single_cell)->Get_CellSize() / 2 << std::endl << std::endl;
						(*single_cell)->Set_DivTick( (*single_cell)->Get_DivTick() + 1 );
						
						if( (*single_cell)->Get_DivTick() == div_cycle_time )
						{
							if( (*single_cell)->Get_InDivCycle() )
							{
								(*single_cell)->Set_DeltaX( ( (*single_cell)->Get_TargetXCoor() - (*single_cell)->Get_XCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
								(*single_cell)->Set_DeltaY( ( (*single_cell)->Get_TargetYCoor() - (*single_cell)->Get_YCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
								(*single_cell)->Set_DeltaZ( ( (*single_cell)->Get_TargetZCoor() - (*single_cell)->Get_ZCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
							}
							else
							{
								(*single_cell)->Set_DeltaX( ( (*single_cell)->Get_TargetXCoor() - (*single_cell)->Get_XCoor() ) / ( NO_DIV_CYCLE * 60 ) );
								(*single_cell)->Set_DeltaY( ( (*single_cell)->Get_TargetYCoor() - (*single_cell)->Get_YCoor() ) / ( NO_DIV_CYCLE * 60 ) );
								(*single_cell)->Set_DeltaZ( ( (*single_cell)->Get_TargetZCoor() - (*single_cell)->Get_ZCoor() ) / ( NO_DIV_CYCLE * 60 ) );
							}
						}
					}
					else
					{
						if( (*single_cell)->Get_InEllipsoid() > 0.95 ) //Move away from the boundary and go back to the origin
							Move_To_Origin(*single_cell);
						else
						{
							/*
							NOTE/TODO: COMPLEX_WANDER IS NOT YET IMPLEMENTED. CURRENTLY BEYOND THE NETLOGO THERE IS NO DEMAND CURRENT
									   FOR NOW, ALWAYS KEEP CELL_FOCUS_SWITCH FALSE
							if(cell_focus_switch) 
								TODO Complex_Wander( (*single_cell) );
							*/
							
							//THE FOLLOWING CODE WORKS BUT CURRENTLY IS NOT NEEDED
							//MOORE2DQUERY IS NOT READY FOR 3D. WILL CRASH PROGRAM!

							/*
							//Find the closest agent to this cell and move away from it
							
							Cell *closest_cell = NULL;
							double shortest_distance = 1000000.0;				
							
							std::vector<Cell*> close_cells;
							repast::Point<int> this_cell_loc((int)(*single_cell)->Get_XCoor(), (int)(*single_cell)->Get_YCoor(), (int)(*single_cell)->Get_ZCoor());
		    				repast::Moore2DGridQuery<Cell> moore2DQuery(space);
			    			moore2DQuery.query(this_cell_loc, SCAN_DISTANCE, false, close_cells);
							std::vector<Cell*> cell = close_cells.begin();
							std::vector<Cell*> cell_end = close_cells.end();

							repast::Point<double> cont_cell_location((*single_cell)->Get_XCoor(), (*single_cell)->Get_YCoor(), (*single_cell)->Get_ZCoor());

							while(cell != cell_end)
							{
								if( (*cell)->Get_CanUse() )//Do not use the current cell or trackers
								{
									
									repast::Point<double> dif_cell_loc((*cell)->Get_XCoor(), (*cell)->Get_YCoor(), (*cell)->Get_ZCoor());
									double distance = cont_ellipsoid->getDistance(cont_cell_location, dif_cell_loc); //Calculate the distance between the two cells

									if(distance < (double)SCAN_DISTANCE && distance < shortest_distance)
									{
										closest_cell = (*cell);
										shortest_distance = distance;
									}
								}

								cell++;
							}
							
							//Since the goal is to move away from the closest cell, the delta is subtracted instead of added
							if( closest_cell != NULL)
							{
								if( (*single_cell)->Get_InDivCycle())
								{
									(*single_cell)->Set_XCoor( (*single_cell)->Get_XCoor() - ( ( closest_cell->Get_XCoor() - (*single_cell)->Get_XCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) ) );
									(*single_cell)->Set_YCoor( (*single_cell)->Get_YCoor() - ( ( closest_cell->Get_YCoor() - (*single_cell)->Get_YCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) ) );
									(*single_cell)->Set_ZCoor( (*single_cell)->Get_ZCoor() - ( ( closest_cell->Get_ZCoor() - (*single_cell)->Get_ZCoor() ) / ( (*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) ) );
								}
								else
								{
									(*single_cell)->Set_XCoor( (*single_cell)->Get_XCoor() - ( ( closest_cell->Get_XCoor() - (*single_cell)->Get_XCoor() ) / ( NO_DIV_CYCLE * 60 ) ) );
									(*single_cell)->Set_YCoor( (*single_cell)->Get_YCoor() - ( ( closest_cell->Get_YCoor() - (*single_cell)->Get_YCoor() ) / ( NO_DIV_CYCLE * 60 ) ) );
									(*single_cell)->Set_ZCoor( (*single_cell)->Get_ZCoor() - ( ( closest_cell->Get_ZCoor() - (*single_cell)->Get_ZCoor() ) / ( NO_DIV_CYCLE * 60 ) ) );
								}
							}
							*/
							Calc_DeltaXYZ(*single_cell);
							soapBubble(*single_cell);

							(*single_cell)->Set_XCoor( (*single_cell)->Get_XCoor() + (*single_cell)->Get_DeltaX() );
							(*single_cell)->Set_YCoor( (*single_cell)->Get_YCoor() + (*single_cell)->Get_DeltaY() );
							(*single_cell)->Set_ZCoor( (*single_cell)->Get_ZCoor() + (*single_cell)->Get_DeltaZ() );
							//(*single_cell)->Set_XCoor( (*single_cell)->Get_XCoor() + (*single_cell)->Calc_DeltaX() );
							//(*single_cell)->Set_YCoor( (*single_cell)->Get_YCoor() + (*single_cell)->Calc_DeltaY() );
							//(*single_cell)->Set_ZCoor( (*single_cell)->Get_ZCoor() + (*single_cell)->Calc_DeltaZ() );
						}	
					}
				}
				else
					Move_To_Origin(*single_cell);
				
				//Set the cell on the grid after movement
				repast::Point<double> cont_location((*single_cell)->Get_XCoor(), (*single_cell)->Get_YCoor(), (*single_cell)->Get_ZCoor());
				repast::Point<int> dis_location((int)(*single_cell)->Get_XCoor(), (int)(*single_cell)->Get_YCoor(), (int)(*single_cell)->Get_ZCoor());
				dis_ellipsoid->moveTo((*single_cell)->getId(), dis_location);
				cont_ellipsoid->moveTo((*single_cell)->getId(), cont_location);
			}

			single_cell++;
		}
	}

	//Mark all cells that need to be moved to different processes
	//dis_ellipsoid->balance();
	//Move all cells that have been marked to the appropriate process
	//repast::RepastProcess::instance()->synchronizeAgentStatus<Cell, Cell_Package, Cell_Agent_Provider, Cell_Agent_Receiver, Cell_Agent_Receiver>(cell_context, *cell_provider, *cell_receiver, *cell_receiver);
}


/****************************************************
 *
 * The following function moves a cell in the
 * direction of the origin. Like the movement in the
 * Wander function, this movement is time stepped. It
 * also moves the agent on the grid.
 *
 * **************************************************/

void Cell_Model::Move_To_Origin(Cell *single_cell)
{//TODO: Test to see if this actually works right
	if( single_cell->Get_InDivCycle() )
	{
		single_cell->Set_XCoor( ( ORIGIN_X  - single_cell->Get_XCoor() ) / ( single_cell->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
		single_cell->Set_YCoor( ( ORIGIN_Y  - single_cell->Get_YCoor() ) / ( single_cell->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
		single_cell->Set_ZCoor( ( ORIGIN_Z  - single_cell->Get_ZCoor() ) / ( single_cell->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() ) );
	}
	else
	{
		single_cell->Set_XCoor( ( ORIGIN_X - single_cell->Get_XCoor() ) / ( NO_DIV_CYCLE * 60 ) );
		single_cell->Set_YCoor( ( ORIGIN_Y - single_cell->Get_YCoor() ) / ( NO_DIV_CYCLE * 60 ) );
		single_cell->Set_ZCoor( ( ORIGIN_Z - single_cell->Get_ZCoor() ) / ( NO_DIV_CYCLE * 60 ) );
	}
}


/************************************************
 *
 * This function determines whether or not a cell
 * is a daughter of the parent based on a name 
 * comparison. It does check information with the
 * direction table to find whether or not this is
 * true or false if one of the exit conditions are
 * not met.
 *
 * *********************************************/

bool Cell_Model::Daughter_Of(std::string parent_name, std::string curr_name, Cell *single_cell) 
{
   	bool daughter_found = false;

    if(parent_name == single_cell->Get_TwelveParent())
       	return true;
    else
    {	
       	while(daughter_found || !daughter_found) //NOTE: THIS INFITE LOOP IS ON PURPOSE
        {
           	for(int daughter = 1; daughter <= 2; daughter++)
            {	
               	daughter_found = false;

                if(curr_name == parent_name) //Daughter has been found
                   	return true;
                if(curr_name == "AB" || curr_name == "P1") //These cells have no daughter (?) TODO: What is this condition for?
                    return false;

				direction_info_table.Search_Table_Daughter_Of(parent_name, daughter, curr_name, daughter_found);
            }	
		}
	}
}


/*************************************************************
 *
 * The following function divides cells and creates new ones
 * should they not need to be made dormant. This function will
 * cross reference information with the time table and direction
 * table when initializing a new cell or preparing a cell for 
 * dormancy.
 *
 * **********************************************************/

void Cell_Model::Divide()
{
	bool time_table_flag = false, dir_table_flag = false, mig_table_flag = false;
	std::string curr_name, daughter_1 = "", daughter_2 = "";
	double cycle_time, cycle_std_dev, x_dir_vec, y_dir_vec, z_dir_vec, x_coor = 0.0, y_coor = 0.0, z_coor = 0.0;
	int running_counter = 0, local_cells = 0, max_rank_cells = 0;
	Cell *new_cell;
	
	//Get the number of cells created by other processes
	boost::mpi::all_reduce( (*(repast::RepastProcess::instance()->communicator())), times_accessed_file, running_counter, std::plus<int>() );

	//NOTE: THIS IS FOR SERIAL USE ONLY
	//running_counter = times_accessed_file;

	//Get all local cells
	//NOTE: USING A VECTOR INSTEAD OF CONSTANT STATE AWARE ITERATOR; THIS FUNCTION ADDS CELLS TO THE CONTEXT
	//		THIS IS TO ENSURE THAT THE FUNCTION DOES NOT WASTE TIME DIVIDING NEW CELLS	
	std::vector<Cell*> all_cells;
	cell_context.selectAgents(repast::SharedContext<Cell>::LOCAL, all_cells);
	std::vector<Cell*>::iterator single_cell = all_cells.begin();
	std::vector<Cell*>::iterator end_cell = all_cells.end(); //Since this function does add cells to the cell_context, the ending of the loop is defined here
															 //There is no need to include recently hatched cells in the current run of divisions. Next
															 //time the function is called, they will be included
	local_cells = all_cells.size();

	//Find the largest number of cells from all processes
	boost::mpi::all_reduce( (*(repast::RepastProcess::instance()->communicator())), local_cells, max_rank_cells, boost::mpi::maximum<int>() );
	
	if(all_cells.size() != 0) //Only divide if there are cells to divide
	{
		if( running_counter >= max_time_file_reads ) //If the file is finished, make all cells dormant (they can no longer divide)
		{
			while(single_cell != end_cell)
			{
				if( (*single_cell)->Get_CanUse() )
				{
					curr_name = (*single_cell)->Get_Name();
						
					if(((*single_cell)->Get_InDivCycle()) && repast::RepastProcess::instance()->getScheduleRunner().currentTick() > (*single_cell)->Get_DivTime())
					{//ERROR may be here
						direction_info_table.Search_Table_Division(curr_name, daughter_1, daughter_2, x_dir_vec, y_dir_vec, z_dir_vec, dir_table_flag);

						if(dir_table_flag)
						{
							(*single_cell)->Set_XDirVec(x_dir_vec);
							std::cout << "Cell in Div: " << (*single_cell)->Get_Name() << "\n\tx_dir_vec: " << x_dir_vec << std::endl;
							(*single_cell)->Set_YDirVec(y_dir_vec);
							(*single_cell)->Set_ZDirVec(z_dir_vec);
							
							new_cell = Create_Cell();
							Make_Dormant((*single_cell), new_cell, daughter_1, daughter_2);

							//Put the new cell on the grid
							repast::Point<double> cont_location(new_cell->Get_XCoor(), new_cell->Get_YCoor(), new_cell->Get_ZCoor());
							repast::Point<int> dis_location((int)new_cell->Get_XCoor(), (int)new_cell->Get_YCoor(), (int)new_cell->Get_ZCoor());
							dis_ellipsoid->moveTo(new_cell->getId(), dis_location);
							cont_ellipsoid->moveTo(new_cell->getId(), cont_location);
						}
						else
							std::cout << curr_name << " WAS NOT FOUND IN DIRECTION TABLE [DIVISION - END ALL CELLS STEP].\n";
					}
				}
				single_cell++;
			}
		}
		else
		{
			for(int cell = 0; cell < max_rank_cells; cell++) //Loop through "all" cells to better safe guard that the time file is not over read
			{
				//Get the number of cells created by other processes
				boost::mpi::all_reduce( (*(repast::RepastProcess::instance()->communicator())), times_accessed_file, running_counter, std::plus<int>() );
			
				if(single_cell != end_cell)
				{
					if( (*single_cell)->Get_CanUse() && (*single_cell)->Get_InDivCycle() )
					{
						if( running_counter >= max_time_file_reads ) 
						{
							if(((*single_cell)->Get_InDivCycle()) && repast::RepastProcess::instance()->getScheduleRunner().currentTick() > (*single_cell)->Get_DivTime())
							{
								new_cell = Create_Cell();
								Make_Dormant((*single_cell), new_cell, daughter_1, daughter_2);

								//Put the new cell on the grid
								repast::Point<double> cont_location(new_cell->Get_XCoor(), new_cell->Get_YCoor(), new_cell->Get_ZCoor());
								repast::Point<int> dis_location((int)new_cell->Get_XCoor(), (int)new_cell->Get_YCoor(), (int)new_cell->Get_ZCoor());
								dis_ellipsoid->moveTo(new_cell->getId(), dis_location);
								cont_ellipsoid->moveTo(new_cell->getId(), cont_location);
							}
						}
						else
						{
							if(((*single_cell)->Get_InDivCycle()) && repast::RepastProcess::instance()->getScheduleRunner().currentTick() > (*single_cell)->Get_DivTime())
							{
								//Create a new cell
								new_cell = Create_Cell(); 
								Copy_Cell( (*single_cell), new_cell );
									
								curr_name = new_cell->Get_Name();

								dir_table_flag = time_table_flag = mig_table_flag = false;
								
								direction_info_table.Search_Table_Division(curr_name, daughter_1, daughter_2, x_dir_vec, y_dir_vec, z_dir_vec, dir_table_flag);

								if(dir_table_flag)
								{
									new_cell->Set_XDirVec(x_dir_vec);
									std::cout << "Cell in Div: " << (*single_cell)->Get_Name() << "\n\tx_dir_vec: " << x_dir_vec << std::endl;
									new_cell->Set_YDirVec(y_dir_vec);
									new_cell->Set_ZDirVec(z_dir_vec);

									new_cell->Set_DivTick(0);
									new_cell->Set_Daughter_1_ID( (*single_cell)->Get_Daughter_2_ID() );
									new_cell->Set_Daughter_2_ID(-1);
									Set_Coords(new_cell, NEW_CELL);
									new_cell->Set_Name(daughter_1);
									new_cell->Set_CellSize( new_cell->Get_CellSize() * SIZE_MULTI );
									
									x_coor = y_coor = z_coor = 0.0;
									curr_name = new_cell->Get_Name();

									times_accessed_file++;
									time_info_table.Search_Table_Division(curr_name, cycle_time, cycle_std_dev, time_table_flag);
									migration_info_table.Search_Table_Move_Coor(curr_name, x_coor, y_coor, z_coor, mig_table_flag);

									if(time_table_flag) //If a cell is found in the time table, set its time to divide and standard deviation
									{
										repast::DoubleUniformGenerator gen = repast::Random::instance()->createUniDoubleGenerator(0.0, ((cycle_std_dev * 60) * 2)); 
										
										new_cell->Set_DivCycleTime(cycle_time * 60);
										new_cell->Set_DivTime( new_cell->Get_DivCycleTime() + repast::RepastProcess::instance()->getScheduleRunner().currentTick() );
										new_cell->Set_StdDev(cycle_std_dev * 60);
										new_cell->Set_DivTime( new_cell->Get_DivTime() + ((double)gen.next() - new_cell->Get_StdDev()) );
										new_cell->Set_InDivCycle(CAN_DIVIDE);
										time_table_flag = false;
									}
									else //If the cell cannot be found in the time table, set its target coordinates to the ones in the migration table
									{	 //Its movement will be handled in the Wander function
										std::cout << curr_name << " (NEW CELL) COULD NOT BE FOUND IN TIME TABLE [DIVISION].\n";
										new_cell->Set_DivTime(-1);
										new_cell->Set_DivCycleTime(NO_DIV_CYCLE * 60);
										new_cell->Set_InDivCycle(CANNOT_DIVIDE);
									}
								
									if(mig_table_flag)
									{
										new_cell->Set_CanMove(CAN_MOVE);
										mig_table_flag = false;
									}
									else
									{
										new_cell->Set_CanMove(CANNOT_MOVE);
										std::cout << curr_name << " (NEW CELL) COULD NOT BE FOUND IN MIGRATION TABLE [DIVISION].\n";
									}
									
									new_cell->Set_TargetXCoor(x_coor);
									new_cell->Set_TargetYCoor(y_coor);
									new_cell->Set_TargetZCoor(z_coor);
				
									//Put the new cell on the grid
									repast::Point<double> cont_location(new_cell->Get_XCoor(), new_cell->Get_YCoor(), new_cell->Get_ZCoor());
									repast::Point<int> dis_location((int)new_cell->Get_XCoor(), (int)new_cell->Get_YCoor(), (int)new_cell->Get_ZCoor());
									dis_ellipsoid->moveTo(new_cell->getId(), dis_location);
									cont_ellipsoid->moveTo(new_cell->getId(), cont_location);

									//Parent cell becomes the daughter 2 cell
									(*single_cell)->Set_XDirVec(x_dir_vec);//next 3 lines are new changes in an effort to fix bugs
									//std::cout << "Cell in Div: " << (*single_cell)->Get_Name() << "\n\tx_dir_vec: " << x_dir_vec << std::endl;
									(*single_cell)->Set_YDirVec(y_dir_vec);
									(*single_cell)->Set_ZDirVec(z_dir_vec);
									Set_Coords((*single_cell), OLD_CELL);
									(*single_cell)->Set_Name(daughter_2);
									(*single_cell)->Set_DivTick(0);
									(*single_cell)->Set_CellRadius( (*single_cell)->Get_CellRadius() / 2 );
									(*single_cell)->Set_CellSize( (*single_cell)->Get_CellSize() * SIZE_MULTI );
									(*single_cell)->Set_Daughter_2_ID(-1);
									
									if( running_counter >= max_time_file_reads ) //NOTE: THIS SHOULD NEVER HAPPEN
										std::cout << "ERROR! DAUGHTER TOOK LAST DIVISION TIME!\n";
									else
									{
										curr_name = (*single_cell)->Get_Name();
										x_coor = y_coor = z_coor = 0.0;
										
										times_accessed_file++;
										time_info_table.Search_Table_Division(curr_name, cycle_time, cycle_std_dev, time_table_flag);
										migration_info_table.Search_Table_Move_Coor(curr_name, x_coor, y_coor, z_coor, mig_table_flag);

										if(time_table_flag) //If cell is found within time table, set its division time and standard deviation
										{
											repast::DoubleUniformGenerator gen = repast::Random::instance()->createUniDoubleGenerator(0.0, ((cycle_std_dev * 60) * 2)); 
										
											(*single_cell)->Set_DivCycleTime(cycle_time * 60);
											(*single_cell)->Set_DivTime( (*single_cell)->Get_DivCycleTime() + repast::RepastProcess::instance()->getScheduleRunner().currentTick() );
											(*single_cell)->Set_StdDev(cycle_std_dev * 60);
											(*single_cell)->Set_DivTime( (*single_cell)->Get_DivTime() + ((double)gen.next() - (*single_cell)->Get_StdDev()) );
											(*single_cell)->Set_InDivCycle(CAN_DIVIDE);
											time_table_flag = false;
										}
											else //If cell is not found within time table, the cell must not be allowed to divide ever again
										{	 //It can still move, so it's movement must be handled within Wander Function
											std::cout << curr_name << " (PARENT/DAUGHTER 2 CELL) COULD NOT BE FOUND IN TIME TABLE [DIVISION].\n";
											(*single_cell)->Set_DivTime(-1);
											(*single_cell)->Set_InDivCycle(CANNOT_DIVIDE);
											(*single_cell)->Set_DivCycleTime(NO_DIV_CYCLE * 60);
										}

										if(mig_table_flag)
										{
											(*single_cell)->Set_CanMove(CAN_MOVE);
											mig_table_flag = false;
										}
										else
										{
											std::cout << curr_name << " (PARENT/DAUGHTER 2 CELL) COULD NOT BE FOUND IN MIGRATION TABLE [DIVISION].\n";
											(*single_cell)->Set_CanMove(CANNOT_MOVE);
										}

										(*single_cell)->Set_TargetXCoor(x_coor);
										(*single_cell)->Set_TargetYCoor(y_coor);
										(*single_cell)->Set_TargetZCoor(z_coor);
									}
								}
								else
									std::cout << curr_name << " COULD NOT BE FOUND IN DIRECTION TABLE. [DIVISION - CREATE NEW CELL STEP]\n";
							}
						}
					}
					single_cell++;
				}
			}
		}
	}
}


/**************************************************************
 *
 * This function copies almost all information of one cell to 
 * another cell.
 *
 * NOTE: The AgentId is not copied since every agent must have
 * 		 a unique Id. If ever two agents have the same exact id,
 * 		 crashes will occur.
 *
 * ************************************************************/

void Cell_Model::Copy_Cell(Cell *copy_from, Cell *copy_to)
{
	copy_to->Set_Name( copy_from->Get_Name() );
	copy_to->Set_TwelveParent( copy_from->Get_TwelveParent() );
	copy_to->Set_DivTime( copy_from->Get_DivTime() );
	copy_to->Set_DivTick( copy_from->Get_DivTick() );
	copy_to->Set_Age( copy_from->Get_Age() );
	copy_to->Set_StdDev( copy_from->Get_StdDev() );
	copy_to->Set_XDivVec( copy_from->Get_XDivVec() );
	copy_to->Set_YDivVec( copy_from->Get_YDivVec() );
	copy_to->Set_ZDivVec( copy_from->Get_ZDivVec() );
	copy_to->Set_XDirVec( copy_from->Get_XDirVec() );
	copy_to->Set_YDirVec( copy_from->Get_YDirVec() );
	copy_to->Set_ZDirVec( copy_from->Get_ZDirVec() );
	copy_to->Set_XCoor( copy_from->Get_XCoor() );
	copy_to->Set_YCoor( copy_from->Get_YCoor() );
	copy_to->Set_ZCoor( copy_from->Get_ZCoor() );
	copy_to->Set_CellSize( copy_from->Get_CellSize() );
	copy_to->Set_InEllipsoid( copy_from->Get_InEllipsoid() );
	copy_to->Set_CellRadius( copy_from->Get_CellRadius() );
	copy_to->Set_DeltaX( copy_from->Get_DeltaX() );
	copy_to->Set_DeltaY( copy_from->Get_DeltaY() );
	copy_to->Set_DeltaZ( copy_from->Get_DeltaZ() );
	copy_to->Set_Daughter_1_ID( copy_from->Get_Daughter_1_ID() );
	copy_to->Set_Daughter_2_ID( copy_from->Get_Daughter_2_ID() );
	copy_to->Set_DivCycleTime( copy_from->Get_DivCycleTime() );
	copy_to->Set_TargetXCoor( copy_from->Get_TargetXCoor() );
	copy_to->Set_TargetYCoor( copy_from->Get_TargetYCoor() );
	copy_to->Set_TargetZCoor( copy_from->Get_TargetZCoor() );
	copy_to->Set_Sex( copy_from->Get_Sex() );
}


/**************************************************************
 *
 * This function sets the division vector to that of the
 * direction vector. However, depending on whether or not the
 * cell is new or not, this can either be the negative of the 
 * vector or the positive. New cells' direction vectors are also
 * set during this time.
 *
 * NEW CELL (true) - negative
 * OLD CELL (false) - positive
 *
 * **********************************************************/

void Cell_Model::Set_Coords(Cell *single_cell, bool is_new_cell)
{
	int multiplier = (is_new_cell ? -1 : 1);
		
	if(is_new_cell)//potential issue: do we account for Cell_Size twice??
	{
		//std::cout << "Is New Cell: " << (single_cell->Get_Name()) << "\nXDirVec: " << (single_cell->Get_XDirVec()) << std::endl << "Cell_Size: " << (single_cell->Get_CellSize()) << std::endl;
		single_cell->Set_XDirVec( ( single_cell->Get_XDirVec() * single_cell->Get_CellSize() ) / 2 ); 
		single_cell->Set_YDirVec( ( single_cell->Get_YDirVec() * single_cell->Get_CellSize() ) / 2 ); 
		single_cell->Set_ZDirVec( ( single_cell->Get_ZDirVec() * single_cell->Get_CellSize() ) / 2 ); 
	}

	single_cell->Set_XDivVec( single_cell->Get_XDirVec() * multiplier );
	//std::cout << "Cell in Set: " << (single_cell->Get_Name()) << "\nXDivVec: " << (single_cell->Get_XDivVec()) << std::endl << "multiplier: " << multiplier << std::endl;
	single_cell->Set_YDivVec( single_cell->Get_YDirVec() * multiplier );
	single_cell->Set_ZDivVec( single_cell->Get_ZDirVec() * multiplier );
}

/**************************************************************
 *
 * This function decides to direction in which the cell will
 * move when it is wandering i.e. not in performing devision,
 * and will be iterated upon as we make cell movement
 * more complex.
 *
 * Currently looks at target location in each direction (X,Y,Z)
 * and randomly assigns a unit vector with some randomness
 * towards the target
 *
 * ***********************************************************/

void Cell_Model::Calc_DeltaXYZ(Cell *single_cell)
{
	double newDeltaX = single_cell->Get_TargetXCoor() - single_cell->Get_XCoor() + moveStdDev();
	double newDeltaY = single_cell->Get_TargetYCoor() - single_cell->Get_YCoor() + moveStdDev();
	double newDeltaZ = single_cell->Get_TargetZCoor() - single_cell->Get_ZCoor() + moveStdDev();

	double mag = sqrt(pow(newDeltaX,2) + pow(newDeltaY,2) + pow(newDeltaZ,2));
	double moveSpeed;

	if((single_cell->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick() < 1)){
		moveSpeed = 0.0;
	}
	else{
		moveSpeed = mag / (single_cell->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	}

	newDeltaX /= mag;
	newDeltaY /= mag;
	newDeltaZ /= mag;

	//std::cout << "moveSpeed: " << moveSpeed << std::endl;
	/*if(moveSpeed > .5){
		std::cout << "SPEED LIKELY TOO HIGH!\n";
	}
	//std::cout << "newDeltaX: " << newDeltaX << std::endl;
	if(newDeltaX > 1){
		std::cout << "DeltaX incorrect\n";
	}
	std::cout << "newDeltaY: " << newDeltaY << std::endl;
	if(newDeltaY > 1){
		std::cout << "DeltaY incorrect\n";
	}
	std::cout << "newDeltaZ: " << newDeltaZ << std::endl;
	if(newDeltaZ > 1){
		std::cout << "DeltaZ incorrect\n";
	}*/

	single_cell->Set_DeltaX(newDeltaX * moveSpeed);
	single_cell->Set_DeltaY(newDeltaY * moveSpeed);
	single_cell->Set_DeltaZ(newDeltaZ * moveSpeed);
	/*This is where code for interacting with other cells will go*/
	//single_cell->Set_UnitWanderX()
}

void Cell_Model::soapBubble(Cell* curr_cell){
	std::vector<Cell*> all_cells;
	cell_context.selectAgents(repast::SharedContext<Cell>::LOCAL, all_cells);
	std::vector<Cell*>::iterator single_cell = all_cells.begin();
	std::vector<Cell*>::iterator end_cell = all_cells.end();
	while(single_cell != end_cell){
		if((*single_cell)->Get_CanUse() && (*single_cell)!=curr_cell){
			if(distanceCell((*single_cell), curr_cell) < 1.0){
				std::cout << "Close to cell!\n" << std::endl;
			}
		}
	}
}

double distanceCell(Cell* cellA, Cell* cellB){
	return sqrt(pow(cellA->Get_XCoor() - cellB->Get_XCoor(), 2) + pow(cellA->Get_YCoor() - cellB->Get_YCoor(), 2) + pow(cellA->Get_ZCoor() - cellB->Get_ZCoor(), 2));
}

double Cell_Model::moveStdDev(){
	//srand((unsigned)time(NULL));
	//double stdDevVal = 0;
	//double stdDevVal = props
	double numDev = repast::Random::instance()->nextDouble() * 100;
	if(numDev < 68.2){//cout << a value between -1 stdDev and 1 stdDev
		double ret = repast::Random::instance()->nextDouble() * migStdDev;
		//return repast::Random::instance()->nextDouble() * migStdDev;
		return ret;
	}
	else if(numDev < 68.2+ 27.2){
		double ret = repast::Random::instance()->nextDouble() * migStdDev;
		//return repast::Random::instance()->nextDouble() * migStdDev + migStdDev;
		return ret;
	}
	else{
		double ret = repast::Random::instance()->nextDouble() * migStdDev;
		//return repast::Random::instance()->nextDouble() * migStdDev + migStdDev*2;
		return ret;
	}
}

/**************************************************************
 *
 * The following function makes a new cell and a old cell. It
 * causes both cells to be dormant. They can still move but
 * won't divide since the DivTime is set to -1.
 *
 * ***********************************************************/

void Cell_Model::Make_Dormant(Cell *single_cell, Cell *new_cell, std::string daughter_1, std::string daughter_2)
{
	double x_coor = 0, y_coor = 0, z_coor = 0;
	bool mig_table_flag = false;

	Copy_Cell(single_cell, new_cell);

	//The new cell becomes the daughter 1
	Set_Coords(new_cell, NEW_CELL);
	new_cell->Set_Name(daughter_1);
	new_cell->Set_DivTime(-1);
	new_cell->Set_DivTick(0);
	new_cell->Set_CellSize( new_cell->Get_CellSize() * SIZE_MULTI );
	new_cell->Set_Daughter_2_ID(-1);
	new_cell->Set_Daughter_1_ID( single_cell->Get_Daughter_2_ID() );
	new_cell->Set_InDivCycle(CANNOT_DIVIDE);

	//Find target coordinates for new cell
	migration_info_table.Search_Table_Move_Coor( new_cell->Get_Name(), x_coor, y_coor, z_coor, mig_table_flag );
	
	if(mig_table_flag)
	{
		new_cell->Set_CanMove(CAN_MOVE);
		mig_table_flag = false;
	}
	else
	{
		new_cell->Set_CanMove(CANNOT_MOVE);
		std::cout << new_cell->Get_Name() << " (NEW CELL) COULD NOT BE FOUND IN MIGRATION TABLE [DORMANCY].\n";
	}
	
	new_cell->Set_TargetXCoor(x_coor);
	new_cell->Set_TargetYCoor(y_coor);
	new_cell->Set_TargetZCoor(z_coor);

	//The old cell (parent) becomes daughter 2
	Set_Coords(single_cell, OLD_CELL);
	single_cell->Set_Name(daughter_2);
	single_cell->Set_DivTime(-1);
	single_cell->Set_DivTick(0);
	single_cell->Set_CellSize( single_cell->Get_CellSize() * SIZE_MULTI );
	single_cell->Set_Daughter_2_ID(-1);
	single_cell->Set_InDivCycle(CANNOT_DIVIDE);
}


/*******************************************************
 *
 * This function finds the distance between a cell and
 * a coordinate if the names are a match. Otherwise it
 * sends -1.0 as a signal that a match was not present.
 * Distance formula.
 *
 * NOTE: SPECIFICALLY USED FOR COMPARE WITH MOD FILE
 *
 * ****************************************************/

double Cell_Model::Find_Dist(Cell *single_cell, std::string curr_name, double x_coor, double y_coor, double z_coor) 
{
  	if(single_cell->Get_Name() == curr_name)
		return sqrt( pow((single_cell->Get_XCoor() - x_coor), 2.0) + 
					 pow((single_cell->Get_YCoor() - y_coor), 2.0) + 
					 pow((single_cell->Get_ZCoor() - z_coor), 2.0) );
	return -1.0;
}


/****************************************************************
 *
 * This function compares the current cells with the cells
 * in a modification file and stores the displacement 
 * (distance) within a text file. The number of hits are
 * stored and used to modify the distance. If there are not
 * any hits, a sentence is stored in the output. There is not
 * a hit if every cell reports a distance of -1.0, meaning that
 * no corresponding cell was in the file.
 *
 * **************************************************************/

void Cell_Model::Compare_With_Mod_File()
{
	int curr_iteration, num_of_reads = 0, num_of_hits = 0, add_to_dist;
	double x_coor, y_coor, z_coor, temp, junk_data, avg_dist = 0.0;
	std::string name, file_name;
	std::ifstream input_file;

	curr_iteration = (repast::RepastProcess::instance()->getScheduleRunner().currentTick() / 90) - 14;

	std::vector<Cell*>all_cells;
	cell_context.selectAgents(repast::SharedContext<Cell>::LOCAL, all_cells);
	std::vector<Cell*>::iterator starting_cell = all_cells.begin(), single_cell;
	std::vector<Cell*>::iterator end_cell = all_cells.end();
	
	if(curr_iteration > 0)
	{
		file_name = (mod_file_dir + std::to_string(curr_iteration) + "mod.txt");
		input_file.open(file_name.c_str(), std::ifstream::in);

		if(input_file.is_open())
		{
			while(!input_file.eof())
			{
				add_to_dist = -1;
				num_of_reads++;

				input_file >> junk_data >> junk_data >> junk_data >> junk_data >> x_coor >> y_coor >> z_coor >> junk_data >> name;

				x_coor = (x_coor / 2 * X_SCALE);
				y_coor = (y_coor / 2 * Y_SCALE);
				z_coor = (z_coor / 2 * Z_SCALE);
				
				single_cell = starting_cell;

				while(single_cell != end_cell)
				{
					temp = Find_Dist( (*single_cell), name, x_coor, y_coor, z_coor ); //Find distance between a cell and this current data

					if(temp != -1.0) //This whill occur if the file was opened and this particular cell has not been created yet
						add_to_dist = temp;

					single_cell++;
				}	

				if(add_to_dist != -1.0) //If recordable data was found, add it to the total average distance
				{
					avg_dist += add_to_dist;
					num_of_hits++; //Increase the number of successful finds
				}
			}
			
			input_file.close();
													
			//Write the average distance divided by the number of successful finds to the file
			std::ofstream output_file;
			output_file.open((displacement_file + "displacement.txt").c_str(), std::ofstream::app);

			if(output_file.is_open())
			{
				if(num_of_hits != 0) //Safeguarding against ERROR: DIVIDE BY ZERO; Note: This is -nan (Not A Number) when written to a file
					output_file << (avg_dist / num_of_hits) << "\n";
				else
					output_file << "NO MATCHES FOUND!\n";
			
				output_file.close();
			}
			else
				std::cout << "DISPLACEMENT FILE COULD NOT BE OPENED!\n" << displacement_file << " DID NOT WORK!\n";
		}
		else
			std::cout << "MODIFICATION FILE COULD NOT BE OPENED!\n" << file_name << " DID NOT WORK!\n";
	}
}


/***********************************************************
 *
 * This function writes all the IDs, coordinate, name, and 
 * size of the cell within a file. It creates a series of
 * files.
 *
 * *******************************************************/

void Cell_Model::Save_To_File()
{
	if((int)repast::RepastProcess::instance()->getScheduleRunner().currentTick() % time_resolution == 0)
	{
		std::string file_name_addition = "", file_name;
		std::ofstream output_file;
		int num_of_ticks = (repast::RepastProcess::instance()->getScheduleRunner().currentTick() / time_resolution) - 20;
		
		//Collect all cells local to this process and write to file
		std::vector<Cell*>all_cells;
		cell_context.selectAgents(repast::SharedContext<Cell>::LOCAL, all_cells);
		std::vector<Cell*>::iterator single_cell = all_cells.begin();
		std::vector<Cell*>::iterator end_cell = all_cells.end();
		
		if(all_cells.size() != 0)
		{
				//This is formatting the correct file name
				if(num_of_ticks >= 100)
					file_name_addition = "t";
				else if(num_of_ticks >= 10)
					file_name_addition = "t0";
				else
					file_name_addition = "t00";

				file_name = (nuclei_file_dir + file_name_addition + std::to_string(num_of_ticks) + "-nuclei.txt");
				output_file.open(file_name.c_str(), std::ofstream::app); //Open in append mode to avoid processs overwriting the file


				while(single_cell != end_cell)
				{
					if( (*single_cell)->Get_CanUse() )
					{
						if(((*single_cell)->Get_DivTime() - repast::RepastProcess::instance()->getScheduleRunner().currentTick()) < time_resolution)
						{
							(*single_cell)->Set_Daughter_2_ID(id);
							id++;
						}

						if(output_file.is_open())
						{
							output_file << (*single_cell)->Get_Daughter_1_ID() << ", 1, " << (*single_cell)->Get_Daughter_1_ID() 
										<< ", " << (*single_cell)->Get_Daughter_1_ID() << ", " << (*single_cell)->Get_Daughter_2_ID() 
										<< ", " << (*single_cell)->Get_XCoor() << ", " << (*single_cell)->Get_YCoor() << ", " 
										<< (*single_cell)->Get_ZCoor() << ", " << (*single_cell)->Get_CellSize() << ", "  
									<< (*single_cell)->Get_Name() << ", 0, 0, 0, 0 , , 0, 0, 0, , 0, , 0, \n";
						}
						else
						{
							std::cout << "NUCLEI FILE COULD NOT BE OPENED!\n" << file_name << " DID NOT WORK!\n";
						}

					}
					single_cell++;
				}

				output_file.close();
		}
	}
}
