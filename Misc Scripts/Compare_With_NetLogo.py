net_cells = {}
rep_cells = {}
net_dir_name = "/Users/Gerard/Desktop/CSURE 2015 Work Fdr/NetLogo - Ben/nuclei/"
rep_dir_name = "/Users/Gerard/Desktop/RepastHPC Txt Files/"
file_name = ""
file_number_rep = ""
file_number_net = ""
cell_name = ""
START = 1
END = 160

output_file = open("/Users/Gerard/Desktop/displacement.txt", 'w')

for x in range(START, END + 1): #Range currently set manually
	if(x < 10):
		file_number_net = "00" + str(x)
	elif(x < 100):
		file_number_net = "0" + str(x)
	else:
		file_number_net = str(x)

	if( (x) < 10):
		file_number_rep = "00" + str(x)
	elif( (x) < 100):
		file_number_rep = "0" + str(x)
	else:
		file_number_rep = str(x)
	
	net_input_file = open(net_dir_name + "t" + file_number_rep + "-nuclei.txt", 'r') #Open file
	rep_input_file = open(rep_dir_name + "t" + file_number_net + "-nuclei.txt", 'r') #Open file

	for file_line in net_input_file:
		line = file_line.split(", ") #Split each entry into an array
		net_cells[ line[9] ] = [ float(line[5]), float(line[6]), float(line[7]) ]
	
	for file_line in rep_input_file:
		line = file_line.split(", ") #Split each entry into an array
		rep_cells[ line[9] ] = [ float(line[5]), float(line[6]), float(line[7]) ]
	

	for entry in net_cells:
		if( entry in rep_cells and not(net_cells[entry] == "Deleted")) : #Find displacement between the two files
			x_dis = ( abs(net_cells[entry][0]) / 2 * 0.087) - abs(rep_cells[entry][0])
			y_dis = ( abs(net_cells[entry][1]) / 2 * 0.087) - abs(rep_cells[entry][1])
			z_dis = ( abs(net_cells[entry][2]) / 2 * 0.504) - abs(rep_cells[entry][2])
 
			if( not(x_dis == 0 and y_dis == 0 and z_dis == 0) ):
				output_file.write( entry + " - File " + str(x))
				output_file.write( " - X Dis: " + str(x_dis) )
				output_file.write( " - Y Dis: " + str(y_dis) )
				output_file.write( " - Z Dis: " + str(z_dis) + "\n" )
			
		net_cells[entry] = "Deleted"

	net_input_file.close()
	rep_input_file.close()

output_file.close()

sec_input = open("/Users/Gerard/Desktop/displacement.txt", 'r')
sec_output = open("/Users/Gerard/Desktop/new_displacement.txt", 'w')
organized_cells = {}

#The second procedure is used to reorganized the displacement file. 
#Doing so, each cell's displacement is grouped together for easier readability.
#Writes to file start at a cell's first appearance to last appearance.

for line in sec_input:
	new_line = line.split(" ")
	
	if( new_line[0] not in organized_cells ):
		organized_cells[new_line[0]] = [line]
	else:
		organized_cells[new_line[0]].append(line)

sec_input.close()

#Important to note that cells will not be ordered since dictionaries are not ordered.
#They will still be in groups, but the groups won't be ordered in terms of at what time they were added to the dictionary.

for entry in organized_cells:
	for index in organized_cells[entry]:
		sec_output.write(index)
	sec_output.write("\n")

sec_output.close()
