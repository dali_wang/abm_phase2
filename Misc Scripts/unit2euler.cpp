#include <cmath>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;

class turtle{
public:
    double X, Y, Z;
    string name;
    vector<string> closest;
};

struct nameAndDistance{
    float distance;
    string name;
};

bool compByDist(const nameAndDistance &a, const nameAndDistance &b){
    return a.distance < b.distance;
}

float myDistance(turtle c1, turtle c2)
{
    //(Dx*Dx+Dy*Dy+Dz*Dz)^.5 
    float dx = c2.X - c1.X;
    float dy = c2.Y - c1.Y;
    float dz = c2.Z - c1.Z;

    return sqrt((float)(dx * dx + dy * dy + dz * dz));
}

void calcEuler(turtle to, turtle from){
    float unitX, unitY, unitZ;
    unitX = (to.X - from.X) / myDistance(to, from);
    unitY = (to.Y - from.Y) / myDistance(to, from);
    unitZ = (to.Z - from.Z) / myDistance(to, from);

    float tmpH = 180.0 * atan2(unitY, unitX) / M_PI;
    float tmpP = 180.0 * asin(unitZ) / M_PI;
    //if (tmpH < 0){
      //  tmpH = 360.0 + tmpH;}
    //if (tmpP < 0){
      //  tmpP = 360.0 + tmpP;}

    cout << "if daughterOf[set heading " << tmpH << " set pitch " << tmpP << " fd .01]\n";
    //cout << "\t" << tmpP << endl;
}

int main(){
    vector<turtle> turtleVec;
    string tmp;
    ifstream fin("17mod.txt");
    fin >> tmp;
    while(!fin.eof()){
        double inF;
        turtle tmpTurtle;
        fin >> tmp >> tmp >> tmp >> tmp;
        inF = atof(tmp.c_str()) / 2 * .087;
        tmpTurtle.X = inF;
        fin >> tmp;
        inF = atof(tmp.c_str()) / 2 * .087;
        tmpTurtle.Y = inF;
        fin >> tmp;
        inF = atof(tmp.c_str()) / 2 * .504;
        tmpTurtle.Z = inF;
        fin >> tmp >> tmp;
        tmpTurtle.name = tmp;
        turtleVec.push_back(tmpTurtle);
        fin >> tmp;
    }

    for(int i=0; i<turtleVec.size(); i++){
        vector<nameAndDistance> tmpVec;
        cout << "set parentName " << turtleVec[i].name << endl << "if daughterOf [\nask cells in-radius (cellSize * 1.5)[\nset currName name\n";
        for(int j=0; j<turtleVec.size(); j++){
            if(i!=j){
                nameAndDistance nad;
                nad.distance = myDistance(turtleVec[i], turtleVec[j]);
                nad.name = turtleVec[j].name;
                tmpVec.push_back(nad);
            }
        }
        sort(tmpVec.begin(), tmpVec.end(), compByDist);
        //cout << turtleVec[i].name << "\n\t" << tmpVec[0].name << "\n\t" << tmpVec[1].name << "\n\t" << tmpVec[2].name << endl;
        for(int j=0; j<3; j++){
            for(int k=0; k< turtleVec.size(); k++){
                if(tmpVec[j].name == turtleVec[k].name){
                    cout << "set parentName " << turtleVec[k].name << "\n";
                    calcEuler(turtleVec[i], turtleVec[k]);
                }
            }
        }
        cout << "]\n]\n";
    }
}

