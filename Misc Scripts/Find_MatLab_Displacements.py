cells = {}
#dir_name = "/Users/Gerard/abmcodeanddoc/NetLogo/matlabOutputTest/nuclei/nuclei/"
dir_name = "/Users/Gerard/Desktop/RepastHPC Txt Files/"
file_name = ""
file_number = ""
cell_name = ""
searching_lists = False

output_file = open("/Users/Gerard/Desktop/displacement.txt", 'w')

for x in range(1, 201): #Range currently set manually
	if(x < 10):
		file_number = "00" + str(x)
	elif(x < 100):
		file_number = "0" + str(x)
	else:
		file_number = str(x)

	input_file = open(dir_name + "t" + file_number + "-nuclei.txt", 'r') #Open file

	for file_line in input_file:
		line = file_line.split(", ") #Split each entry into an array

		#Add the cell to the dictionary if it is not in the dictionary and, in the case of searching, it is the cell being searched for 
		if( (not searching_lists and line[9] not in cells) or (searching_lists and line[9] not in cells and line[9] == cell_name) ): 
			cells[ line[9] ] = [ float(line[5]), float(line[6]), float(line[7]) ]
		elif( not searching_lists or (searching_lists and line[9] == cell_name) ) : #If the cell is present or the cell I'm searching for
			x_dis = cells[line[9]][0] - float(line[5])			    #Get the displacement between the two files
			y_dis = cells[line[9]][1] - float(line[6])
			z_dis = cells[line[9]][2] - float(line[7])

			cells[line[9]][0] = float(line[5])
			cells[line[9]][1] = float(line[6])
			cells[line[9]][2] = float(line[7])

			if( not(x_dis == 0 and y_dis == 0 and z_dis == 0) ):
				output_file.write( line[9] + " - File " + str(x-1) + " and File " + str(x))
				output_file.write( " - X Dis: " + str(x_dis) )
				output_file.write( " - Y Dis: " + str(y_dis) )
				output_file.write( " - Z Dis: " + str(z_dis) + "\n" )
	input_file.close()
output_file.close()

sec_input = open("/Users/Gerard/Desktop/displacement.txt", 'r')
sec_output = open("/Users/Gerard/Desktop/new_displacement.txt", 'w')
organized_cells = {}

#The second procedure is used to reorganized the displacement file. 
#Doing so, each cell's displacement is grouped together for easier readability.
#Writes to file start at a cell's first appearance to last appearance.

for line in sec_input:
	new_line = line.split(" ")
	
	if( new_line[0] not in organized_cells ):
		organized_cells[new_line[0]] = [line]
	else:
		organized_cells[new_line[0]].append(line)

sec_input.close()

#Important to note that cells will not be ordered since dictionaries are not ordered.
#They will still be in groups, but the groups won't be ordered in terms of at what time they were added to the dictionary.

for entry in organized_cells:
	for index in organized_cells[entry]:
		sec_output.write(index)
	sec_output.write("\n")

sec_output.close()
