import os
#with open(rootdir + '/' + file) as fp:
import sys
if len(sys.argv) != 3:
	print "USAGE: ./script inputDirectory outputDirectory\n"
	sys.exit()
rootdir = sys.argv[1]
counter = 1
folder = sys.argv[2]

for files in os.walk(rootdir):
    for nfiles in files:
        if nfiles != rootdir and nfiles != []:
            for file in nfiles:
                if file[0] != '.':
	                sctr = str(counter)
	                #print 'writing to file: ' + folder + 'data' + sctr + '.3d' + '...'
                	fileNum = file[1:4]
                 	#print fileNum
	                with open(folder + 'data' + fileNum + '.3d', 'w') as outputFile:
	                    #infile.seek(0)
	                    #infile.truncate()
	                    outputFile.write("x y z var\n")
	                    with open(rootdir + file) as fp:
	                        #print 'reading from file: ' + rootdir + file + '...'
	                        #print rootdir + file
	                        for line in fp:
	                            #print line
	                            data = line.split(', ')
	                            #cID = data[0]
	                            valid = data[1]
	                            #pID = data[2]
	                            #nID = data[3]
	                            #dID = data[4]
	                            xCOR = data[5]
	                            yCOR = data[6]
	                            zCOR = data[7]
	                            size = data[8]
	                            name = data[9]
	                            if "Nuc" in name:
	                                valid = 0
	                            if valid == "1":
	                                outputFile.write(xCOR + " ")
	                                outputFile.write(yCOR + " ")
	                                outputFile.write(zCOR + " ")
	                                outputFile.write(size + "\n")
	                counter = counter + 1
