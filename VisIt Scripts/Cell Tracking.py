import os, shutil

def ParseDivTable():
    DivTable = []
    with open('DivisionDirWQ.txt') as inDiv:
        for line in inDiv:
            data = line.split('" ')
            parent = data[0]
            parent = parent[1:]
            daughter1 = data[1]
            daughter1 = daughter1[1:]
            daughter2 = data[2]
            daughter2 = daughter2[1:]
            listofdata = [parent, daughter1, daughter2]
            DivTable.append(listofdata)
        DivTable = DivTable[1:]
        return DivTable
           
def GetUserCell():
    User_Cell = raw_input('Enter the name of the cell you want to track: ')
    return User_Cell

def Find_Ancestors(User_Cell, DivTable, Ancestors):
    found = False
    search_cell = User_Cell
    while search_cell != 'ABp' and search_cell != 'EMS' and search_cell != 'P2' and search_cell != 'ABa':
        found = False
        for cells in DivTable:
            if not found:
                if search_cell == cells[1] or search_cell == cells[2]:
                    found = True
                    search_cell = cells[0]
                    Ancestors.append(search_cell)

def Find_Children(User_Cell, DivTable, Children):
    search_cells = []
    search_cells.append(User_Cell)
    while search_cells != []:
        search_cell = search_cells[0]
        for cells in DivTable:
            if search_cell == cells[0]:
                search_cells.append(cells[1])
                search_cells.append(cells[2])
                Children.append(cells[1])
                Children.append(cells[2])
        search_cells.remove(search_cell)
    
def Highlight_Cells(User_Cell, Ancestors, Children):
    rootdir = 'C:/Python27/nuclei'
    counter = 1
    folder = 'C:/Python27/data files/'
    
    for files in os.walk(rootdir):
        for nfiles in files:
            if nfiles != rootdir and nfiles != []:
                for file in nfiles:
                    sctr = str(counter)
                    with open(folder + 'data' + sctr + '.3d', 'a') as infile:
                        infile.seek(0)
                        infile.truncate()
                        infile.write("x y z var\n")
                        with open(rootdir + '/' + file) as fp:
                            for line in fp:
                                data = line.split(', ')
                                #cID = data[0]
                                valid = data[1]
                                #pID = data[2]
                                #nID = data[3]
                                #dID = data[4]
                                xCOR = data[5]
                                yCOR = data[6]
                                zCOR = data[7]
                                color_v = data[8]
                                name = data[9]
                                if '"' in name:
                                    name = name[1:len(name) - 1]
                                if "Nuc" in name:
                                    valid = 0
                                if valid == "1":
                                    if name in Ancestors:
                                        color_v = 100
                                    elif name == User_Cell:
                                        color_v = 50
                                    elif name in Children:
                                        color_v = 50
                                    else:
                                        color_v = 0
                                    infile.write(xCOR + " ")
                                    infile.write(yCOR + " ")
                                    infile.write(zCOR + " ")
                                    infile.write(str(color_v) + "\n")
                    counter = counter + 1

DivTable = ParseDivTable()
User_Cell = GetUserCell()
Ancestors = []
Children = []

Find_Ancestors(User_Cell, DivTable, Ancestors)

Find_Children(User_Cell, DivTable, Children)

Highlight_Cells(User_Cell, Ancestors, Children)
