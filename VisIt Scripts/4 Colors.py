import os, shutil, sys, re

def ParseDivTable(pathDivTable):
	DivTable = []
	with open(pathDivTable) as inDiv:
		for line in inDiv:
			data = line.split('" ')
			parent = data[0]
			parent = parent[1:]
			daughter1 = data[1]
			daughter1 = daughter1[1:]
			daughter2 = data[2]
			daughter2 = daughter2[1:]
			listofdata = [parent, daughter1, daughter2]
			DivTable.append(listofdata)
		DivTable = DivTable[1:]
		return DivTable

def Find_Children(User_Cell, DivTable, Children):
	search_cells = []
	search_cells.append(User_Cell)
	Children.append(User_Cell)
	while search_cells != []:
		search_cell = search_cells[0]
		for cells in DivTable:
			if search_cell == cells[0]:
				search_cells.append(cells[1])
				search_cells.append(cells[2])
				Children.append(cells[1])
				Children.append(cells[2])
		search_cells.remove(search_cell)
	
def Highlight_Cells(inputDir, outputDir, DivTable, blue, yellow, green, red):
	#rootdir = 'C:/Python27/nuclei'
	rootdir = inputDir
	counter = 1
	#folder = 'C:/Python27/data files/'
	folder = outputDir
	
	for files in os.walk(rootdir):
		for nfiles in files:
			if nfiles != rootdir and nfiles != []:
				for file in nfiles:
					#print file[0]
					if file[0] != '.':
						sctr = str(counter)
						matchObj = re.match(r't([0-9]*)-nuclei.txt', file)
						print file
						print matchObj.group(1)
						print matchObj.group()
						fileNum = file[1:4]
						with open(folder + 'data' + matchObj.group(1) + '.3d', 'w') as infile: #used to be fileNum instead of matchObj
							infile.write("x y z var\n")
							with open(rootdir + '/' + file) as fp:
								for line in fp:
									data = line.split(', ')
									#cID = data[0]
									valid = data[1]
									#pID = data[2]
									#nID = data[3]
									#dID = data[4]
									xCOR = data[5]
									yCOR = data[6]
									zCOR = data[7]
									#size = data[8]
									name = data[9]
									if '"' in name:
										name = name[1:len(name) - 1]
									if "Nuc" in name:
										valid = 0
									if valid == "1":
										if name in blue:
											color = 0
										elif name in yellow:
											color = 75
										elif name in green:
											color = 50
										elif name in red:
											color = 100
										infile.write(xCOR + " ")
										infile.write(yCOR + " ")
										infile.write(zCOR + " ")
										infile.write(str(color) + "\n")

if len(sys.argv) != 4:
	print "USAGE: ./script DivisionTable nucleiDirectory outputDirectory\n"
	sys.exit()

DivTable = ParseDivTable(sys.argv[1])
Starting_Cells = ['P2', 'EMS', 'ABa', 'ABp']
blue = []
yellow = []
green = []
red = []

Children = []
Find_Children('P2', DivTable, Children)
blue = Children
Children = []
Find_Children('EMS', DivTable, Children)
yellow = Children
Children = []
Find_Children('ABa', DivTable, Children)
green = Children
Children = []
Find_Children('ABp', DivTable, Children)
red = Children
Highlight_Cells(sys.argv[2], sys.argv[3], DivTable, blue, yellow, green, red)
